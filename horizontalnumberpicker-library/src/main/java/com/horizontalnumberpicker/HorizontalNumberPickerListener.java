package com.horizontalnumberpicker;


public interface HorizontalNumberPickerListener {
    void onHorizontalNumberPickerChanged(HorizontalNumberPicker horizontalNumberPicker, int id, int value);
}
