package com.sar.user.livfastRewards.adapter;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sar.user.livfastRewards.Interface.onItemClickListner;
import com.sar.user.livfastRewards.Model.SegmentModel;
import com.sar.user.livfastRewards.R;

import java.util.List;

public class SegmentAdapter extends RecyclerView.Adapter<SegmentAdapter.MyViewHolder> {

    Context context;
    private onItemClickListner onItemClickListner;
    private List<SegmentModel> segmentModels;
    private int selectedPos;


    public SegmentAdapter(List<SegmentModel> segmentModels, onItemClickListner onItemClickListner) {
        this.onItemClickListner = onItemClickListner;
        this.segmentModels = segmentModels;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_segment, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final String val = segmentModels.get(position).getCar_segment();
        holder.segmentText.setText(val);
        holder.itemImage.setImageResource(checkImage(val));


        holder.itemImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notifyItemChanged(selectedPos);
                selectedPos = position;
                notifyItemChanged(selectedPos);
                onItemClickListner.onItemClick(segmentModels.get(position).getId(), segmentModels.get(position).getCar_segment());
            }
        });

        if (position == selectedPos) {
            Log.d("AdapterPosition", "" + selectedPos);
            setSelectorImage(val, holder.itemImage);
        } else {
            holder.itemImage.setImageResource(checkImage(val));
        }

    }

    private int checkImage(String segmentText) {
        int res = R.drawable.car_image;
        if (segmentText.equalsIgnoreCase("CAR")) {
            res = R.drawable.car_image;
        } else if (segmentText.equalsIgnoreCase("SUV/ MUV")) {
            res = R.drawable.suv_image;
        } else if (segmentText.equalsIgnoreCase("3 WHEELER")) {
            res = R.drawable.auto_image;
        } else if (segmentText.equalsIgnoreCase("TRACTOR")) {
            res = R.drawable.truck_image;
        } else if (segmentText.equalsIgnoreCase("MOTORCYCLE")) {
            res = R.drawable.bike_image;
        } else if (segmentText.equalsIgnoreCase("SCOOTER")) {
            res = R.drawable.scooty_image;
        } else if (segmentText.equalsIgnoreCase("CV")) {
            res = R.drawable.cv_image;
        } else if (segmentText.equalsIgnoreCase("IB")) {
            res = R.drawable.home_image;
        }
        return res;
    }

    private void setSelectorImage(String segmentText, AppCompatImageView appCompatImageView) {
        if (segmentText.equalsIgnoreCase("CAR")) {
            appCompatImageView.setImageResource(R.drawable.car_image_on);
        } else if (segmentText.equalsIgnoreCase("SUV/ MUV")) {
            appCompatImageView.setImageResource(R.drawable.suv_image_on);
        } else if (segmentText.equalsIgnoreCase("3 WHEELER")) {
            appCompatImageView.setImageResource(R.drawable.auto_image_on);
        } else if (segmentText.equalsIgnoreCase("TRACTOR")) {
            appCompatImageView.setImageResource(R.drawable.truck_image_on);
        } else if (segmentText.equalsIgnoreCase("MOTORCYCLE")) {
            appCompatImageView.setImageResource(R.drawable.bike_image_on);
        } else if (segmentText.equalsIgnoreCase("SCOOTER")) {
            appCompatImageView.setImageResource(R.drawable.scooty_image_on);
        } else if (segmentText.equalsIgnoreCase("CV")) {
            appCompatImageView.setImageResource(R.drawable.cv_image_on);
        } else if (segmentText.equalsIgnoreCase("IB")) {
            appCompatImageView.setImageResource(R.drawable.home_image_on);
        } else {
            appCompatImageView.setImageResource(R.drawable.car_image);
        }

    }

    @Override
    public int getItemCount() {
        return segmentModels.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        AppCompatTextView segmentText;
        AppCompatImageView itemImage;

        MyViewHolder(View view) {
            super(view);
            segmentText = (AppCompatTextView) view.findViewById(R.id.segment_text);
            itemImage = (AppCompatImageView) view.findViewById(R.id.car_image);
        }
    }
}
