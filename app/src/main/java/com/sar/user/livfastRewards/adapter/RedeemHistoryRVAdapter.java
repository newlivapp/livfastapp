package com.sar.user.livfastRewards.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sar.user.livfastRewards.Model.GetRedeemHistoryModel;
import com.sar.user.livfastRewards.R;
import com.sar.user.livfastRewards.helper.MyUtils;

import java.util.List;

/**
 * Created by Nav on 03-Aug-17.
 */

public class RedeemHistoryRVAdapter extends RecyclerView.Adapter<RedeemHistoryRVAdapter.MyViewHolder> {
    Context context;
    List<GetRedeemHistoryModel> redeemhistoryList;

    public RedeemHistoryRVAdapter(Context context1, List<GetRedeemHistoryModel> redeemhistoryList1) {
        context = context1;
        redeemhistoryList = redeemhistoryList1;
    }

    @Override
    public RedeemHistoryRVAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.redeemhistory_rv_item, parent, false);
        MyViewHolder mvh = new MyViewHolder(v);
        return mvh;
    }

    @Override
    public void onBindViewHolder(RedeemHistoryRVAdapter.MyViewHolder holder, int position) {
        holder.date.setText(MyUtils.getValidDateFormat(redeemhistoryList.get(position).getCreated_timestamp()));
        holder.loyalty_points.setText("" + redeemhistoryList.get(position).getLoyalty_point());
//        holder.value.setText(""+redeemhistoryList.get(position).getValue());
        holder.value.setText("0");
        String status = (redeemhistoryList.get(position).getRedeem_state()).toLowerCase();
        holder.status.setText(redeemhistoryList.get(position).getRedeem_state());

        if (status.equals("settled")) {
            holder.status.setTextColor(Color.parseColor("#6FCF97"));
        } else if (status.equals("pending")) {
            holder.status.setTextColor(Color.parseColor("#F2994A"));
        } else if (status.equals("cancelled")) {
            holder.status.setTextColor(Color.parseColor("#EB5757"));
        }
    }

    @Override
    public int getItemCount() {
        return redeemhistoryList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView date, loyalty_points, value, status;

        public MyViewHolder(View itemView) {
            super(itemView);
            date = (TextView) itemView.findViewById(R.id.date);
            loyalty_points = (TextView) itemView.findViewById(R.id.loyalty_points);
            value = (TextView) itemView.findViewById(R.id.value);
            status = (TextView) itemView.findViewById(R.id.status);
        }
    }
}
