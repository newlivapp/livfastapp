package com.sar.user.livfastRewards.Model;

/**
 * Created by Nav on 31-Jul-17.
 */

public class GetSerialNoModel {
    public String unique_code;

    public String getUnique_code() {

        return unique_code;
    }

    public void setUnique_code(String unique_code) {
        this.unique_code = unique_code;
    }
}
