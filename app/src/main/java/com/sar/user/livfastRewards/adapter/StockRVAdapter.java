package com.sar.user.livfastRewards.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sar.user.livfastRewards.Model.GetStockModel;
import com.sar.user.livfastRewards.R;

import java.util.List;

/**
 * Created by Nav on 01-Aug-17.
 */

public class StockRVAdapter extends RecyclerView.Adapter<StockRVAdapter.MyViewHolder> {
    Context context;
    List<GetStockModel> stocklist;

    public StockRVAdapter(Context context1, List<GetStockModel> stocklist1) {
        context = context1;
        stocklist = stocklist1;
    }

    @Override
    public StockRVAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.stock_rv_items, parent, false);
        MyViewHolder mvh = new MyViewHolder(v);
        return mvh;
    }

    @Override
    public void onBindViewHolder(StockRVAdapter.MyViewHolder holder, int position) {
        holder.product_name.setText(stocklist.get(position).getModel_number().toUpperCase());
        holder.quantity.setText("" + stocklist.get(position).getCount());
    }

    @Override
    public int getItemCount() {
        return stocklist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView product_name, quantity;

        public MyViewHolder(View itemView) {
            super(itemView);
            product_name = (TextView) itemView.findViewById(R.id.product_name);
            quantity = (TextView) itemView.findViewById(R.id.quantity);
        }
    }
}
