package com.sar.user.livfastRewards.Model;

/**
 * Created by user on 31-03-2017.
 */

public class LoginResponseModel {


    public String token;
    public DataModel data;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public DataModel getData() {
        return data;
    }

    public void setData(DataModel data) {
        this.data = data;
    }
}
