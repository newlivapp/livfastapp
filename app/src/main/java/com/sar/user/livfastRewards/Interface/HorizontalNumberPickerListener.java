package com.sar.user.livfastRewards.Interface;

import com.horizontalnumberpicker.HorizontalNumberPicker;

public interface HorizontalNumberPickerListener {
    void onHorizontalNumberPickerChanged(HorizontalNumberPicker horizontalNumberPicker, String id, int value);
}