package com.sar.user.livfastRewards.helper;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

/**
 * Created by user on 30-03-2017.
 */

public class JsonUtils {
    private static final String Tag = JsonUtils.class.getSimpleName();
    private static Gson gson = new Gson();

    public static String jsonify(Object object) {
        return gson.toJson(object);
    }

    public static <T> T objectify(String pJson, Class<T> pType) {
        try {
            return gson.fromJson(pJson, pType);
        } catch (JsonSyntaxException e) {
            Log.d("catch", e.toString());
        } catch (Exception e) {
            Log.d("catch", e.toString());
        }
        return null;
    }
}
