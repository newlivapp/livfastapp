package com.sar.user.livfastRewards.Model;

public class RedeemPointsAPIModel {
    String id, created_timestamp, updated_timestamp, user_id, verification_otp, verification_status, points_redeem, category;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreated_timestamp() {
        return created_timestamp;
    }

    public void setCreated_timestamp(String created_timestamp) {
        this.created_timestamp = created_timestamp;
    }

    public String getUpdated_timestamp() {
        return updated_timestamp;
    }

    public void setUpdated_timestamp(String updated_timestamp) {
        this.updated_timestamp = updated_timestamp;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getVerification_otp() {
        return verification_otp;
    }

    public void setVerification_otp(String verification_otp) {
        this.verification_otp = verification_otp;
    }

    public String getVerification_status() {
        return verification_status;
    }

    public void setVerification_status(String verification_status) {
        this.verification_status = verification_status;
    }

    public String getPoints_redeem() {
        return points_redeem;
    }

    public void setPoints_redeem(String points_redeem) {
        this.points_redeem = points_redeem;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
