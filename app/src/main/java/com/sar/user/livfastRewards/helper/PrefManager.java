package com.sar.user.livfastRewards.helper;


import android.content.Context;
import android.content.SharedPreferences;

import com.sar.user.livfastRewards.Model.DataModel;
import com.sar.user.livfastRewards.Model.LoginResponseModel;

/**
 * Created by user on 30-03-2017.
 */

public class PrefManager {

    private static final String PREF_NAME = "LIVEGUARD_APP";
    private static final String KEY_USER_ID = "key_user_id";
    private static final String KEY_BATCODE = "key_batcode";
    private static final String KEY_TOKEN = "key_token";
    private static final String KEY_IS_Login = "key_is_login";
    private static final String KEY_PHONE_NUMBER = "key_phone_no";
    private static final String KEY_DISTRIBUTOR_NAME = "key_distributor_name";
    private static final String KEY_DISTRIBUTOR_CODE = "key_distributor_code";
    private static final String KEY_DEALERSHIP_NAME = "key_dealership_name";
    private static final String KEY_EMAIL = "key_email";
    private static final String KEY_LARGE_IMAGE = "key_large_image";
    private static final String KEY_SMALL_IMAGE = "key_small_image";
    private static final String KEY_FIRST_NAME = "key_first_name";
    private static final String KEY_LAST_NAME = "key_last_name";
    private static final String KEY_ADDRESS = "key_address";
    private static final String KEY_CITY = "key_city";
    private static final String KEY_STATE = "key_state";
    private static final String LOGIN_MODEL = "login_model";

    private static final String STATUS = "status";
    private static final String KEY_FLAG = "key_flag";
    private static final String KEY_FLAG_WB = "key_flag_wb";

    private static PrefManager prefManager;
    private static SharedPreferences pref;
    private static SharedPreferences.Editor editor;


    public PrefManager(Context ctx) {
        pref = ctx.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = pref.edit();
        editor.apply();
    }

    public static PrefManager getInstance(Context context) {
        if (prefManager == null)
            prefManager = new PrefManager(context);

        return prefManager;
    }

    public void clearSession() {
        editor.clear();
        editor.commit();
    }

    public void setUserId(String userId) {
        editor.putString(KEY_USER_ID, userId);
        editor.apply();
    }

    public String getÜserId() {
        return pref.getString(KEY_USER_ID, "");
    }

    public String getBatCode() {
        return pref.getString(KEY_BATCODE, "");
    }

    public void setBatCode(String batCode) {
        editor.putString(KEY_BATCODE, batCode);
        editor.apply();
    }

    public String getToken() {
        return pref.getString(KEY_TOKEN, "");
    }

    public void setToken(String token) {
        editor.putString(KEY_TOKEN, token);
        editor.apply();
    }

    public String getPhoneNo() {
        return pref.getString(KEY_PHONE_NUMBER, "");
    }

    public void setPhoneNo(String phoneNo) {
        editor.putString(KEY_PHONE_NUMBER, phoneNo);
        editor.apply();
    }

    public String getDistributorName() {
        return pref.getString(KEY_DISTRIBUTOR_NAME, "");
    }

    public void setDistributorName(String distributorName) {
        editor.putString(KEY_DISTRIBUTOR_NAME, distributorName);
        editor.apply();
    }

    public String getDistributorCode() {
        return pref.getString(KEY_DISTRIBUTOR_CODE, "");
    }

    public void setDistributorCode(String distributorCode) {
        editor.putString(KEY_DISTRIBUTOR_CODE, distributorCode);
        editor.apply();
    }

    public String getDealershipName() {
        return pref.getString(KEY_DEALERSHIP_NAME, "");
    }

    public void setDealershipName(String dealershipName) {
        editor.putString(KEY_DEALERSHIP_NAME, dealershipName);
        editor.apply();
    }

    public String getEmail() {
        return pref.getString(KEY_EMAIL, "");
    }

    public void setEmail(String email) {
        editor.putString(KEY_EMAIL, email);
        editor.apply();
    }

    public String getSmallImage() {
        return pref.getString(KEY_LARGE_IMAGE, "");
    }

    public void setSmallImage(String largeImage) {
        editor.putString(KEY_SMALL_IMAGE, largeImage);
        editor.apply();
    }

    public String getLargeImage() {
        return pref.getString(KEY_LARGE_IMAGE, "");
    }

    public void setLargeImage(String largeImage) {
        editor.putString(KEY_LARGE_IMAGE, largeImage);
        editor.apply();
    }

    public String getFirstName() {
        return pref.getString(KEY_FIRST_NAME, "");
    }

    public void setFirstName(String firstName) {
        editor.putString(KEY_FIRST_NAME, firstName);
        editor.apply();
    }

    public String getLastName() {
        return pref.getString(KEY_LAST_NAME, "");
    }

    public void setLastName(String lastName) {
        editor.putString(KEY_LAST_NAME, lastName);
        editor.apply();
    }

    public String getAddress() {
        return pref.getString(KEY_ADDRESS, "");
    }

    public void setAddress(String address) {
        editor.putString(KEY_ADDRESS, address);
        editor.apply();
    }

    public String getCity() {
        return pref.getString(KEY_CITY, "");
    }

    public void setCity(String city) {
        editor.putString(KEY_CITY, city);
        editor.apply();
    }

    public String getState() {
        return pref.getString(KEY_STATE, "");
    }

    public void setState(String state) {
        editor.putString(KEY_STATE, state);
        editor.apply();
    }

    public void setIsLogin(boolean isLogin) {
        editor.putBoolean(KEY_IS_Login, isLogin);
        editor.apply();
    }

    public boolean isLogin() {
        return pref.getBoolean(KEY_IS_Login, false);
    }

    public void saveUserInfo(LoginResponseModel loginResponseModel) {

        if (loginResponseModel == null)
            return;

        setToken(loginResponseModel.getToken());

        setUserData(loginResponseModel.getData());
    }

    public void setUserData(DataModel dataModel) {
        if (dataModel == null)
            return;
        //We have data save the user Details
        setUserId(dataModel.getUser_id());
        setBatCode(dataModel.getUsername());
        setPhoneNo(String.valueOf(dataModel.getPhone()));
        setDistributorName(dataModel.getDistributor_name());
        setDistributorCode(dataModel.getDistributor_code());
        setDealershipName(dataModel.getDealership_name());
        setEmail(dataModel.getEmail());
        setLargeImage(dataModel.getLarge_image());
        setSmallImage(dataModel.getSmall_image());
        setFirstName(dataModel.getFirst_name());
        setLastName(dataModel.getLast_name());
        setAddress(dataModel.getAddress());
        setCity(dataModel.getCity());
        setState(dataModel.getState());
    }

    public LoginResponseModel getLoginModel() {
        String userJson = pref.getString(LOGIN_MODEL, null);
        LoginResponseModel locationModel = JsonUtils.objectify(userJson, LoginResponseModel.class);
        return locationModel;
    }

    //please change key for delhischeme data to KEY_FLAG
    public boolean getUserViewFlag() {
        return pref.getBoolean(KEY_FLAG, false);
    }

    public void setUserViewFlag(boolean flag) {
        editor.putBoolean(KEY_FLAG, flag);
        editor.apply();
    }

    //please change key for delhischeme data to KEY_FLAG
    public boolean getUserViewFlagWB() {
        return pref.getBoolean(KEY_FLAG_WB, false);
    }

    public void setUserViewFlagWB(boolean flag) {
        editor.putBoolean(KEY_FLAG_WB, flag);
        editor.apply();
    }

}
