package com.sar.user.livfastRewards.Model;

/**
 * Created by user on 05-04-2017.
 */

public class TargetDetailsModel {
    int id, loyalty_point, warranty_year, warranty_month;
    String brand, product, warranty;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLoyalty_point() {
        return loyalty_point;
    }

    public void setLoyalty_point(int loyalty_point) {
        this.loyalty_point = loyalty_point;
    }

    public int getWarranty_year() {
        return warranty_year;
    }

    public void setWarranty_year(int warranty_year) {
        this.warranty_year = warranty_year;
    }

    public int getWarranty_month() {
        return warranty_month;
    }

    public void setWarranty_month(int warranty_month) {
        this.warranty_month = warranty_month;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getWarranty() {
        return warranty;
    }

    public void setWarranty(String warranty) {
        this.warranty = warranty;
    }
}
