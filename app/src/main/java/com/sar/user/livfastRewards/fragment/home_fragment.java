package com.sar.user.livfastRewards.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sar.user.livfastRewards.AutoScrollViewPager;
import com.sar.user.livfastRewards.Database.DatabaseBackend;
import com.sar.user.livfastRewards.Interface.RequestListener;
import com.sar.user.livfastRewards.Model.BannerModel;
import com.sar.user.livfastRewards.Model.GetViewFlag;
import com.sar.user.livfastRewards.Model.HomeCountModel;
import com.sar.user.livfastRewards.Model.LoginModel;
import com.sar.user.livfastRewards.Model.LoginResponseModel;
import com.sar.user.livfastRewards.Model.LoyalityModel;
import com.sar.user.livfastRewards.Model.PointsModel;
import com.sar.user.livfastRewards.R;
import com.sar.user.livfastRewards.activity.Activity_sec_ibups_redeem;
import com.sar.user.livfastRewards.activity.EditProfileActivity;
import com.sar.user.livfastRewards.activity.RedeemActivity;
import com.sar.user.livfastRewards.helper.Controller;
import com.sar.user.livfastRewards.helper.JsonUtils;
import com.sar.user.livfastRewards.helper.PrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class home_fragment extends BaseFragment {

    ProgressBar progressBar;
    TextView partnerNameTv, dealershipNameTv, dealer_name_tv, phone_no, total_purchase_count, total_warranty_points, total_warranty_points_pr, total_ib_counts, total_ups_points, total_hw_counts, total_hw_counts_pr, total_lw_counts, total_lw_counts_pr, total_e_loyality, total_e_loyality_pr, total_ericksha_count, total_ericksha_count_pr, total_sec_ibups_points,
            total_ib_sec_points, total_ups_points_sec,points_heading_tertiary,points_heading_purchase,points_heading_sec_ibups_dh,total_sec_ibups_points_dh;
    CircleImageView profile_pic;
    Context context;
    RecyclerView battery_model_rv;
    //Spinner manufacturer_spinner, car_model_spinner, fuel_spinner;
    RelativeLayout points_RL;
    RelativeLayout points_wrranty, points_wrranty_pr, points_RL_tertiary, points_RL_ericksha, points_RL_ericksha_pr, points_Ib, points_ups, points_hw, points_hw_pr, points_lw, points_lw_pr, loyalty_ricksha, loyalty_ricksha_pr, points_sec_ibups, points__sec_Ib, points_ups_sec;
    RelativeLayout points_RL_purchase,points_sec_ibups_dh;
    List<String> banner;
    SwipeRefreshLayout mySwiprRefreshLayout;
    RelativeLayout profilePicRL;
    DatabaseBackend databaseBackend;
    String text;
    String points, pointstwo, pointthree, poitnsfour;
    Double pointsnew;
    int curval1 = 0;
    int curval2 = 0;
    int count1, count2, count3, count4, count4new, count5, count6, count7, count1new, count2new, countERick_pr, countLW, countHW,count8,count9,count10,count11;
    double Loyality_points1;
    double Loyality_points2;
    double Loyality_points3;
    double Loyality_points4;
    double Loyality_points4new;
    int Loyality_points5;
    double Loyality_points6;
    double Loyality_points7;
    double Loyality_points_erick_pr;
    double IBRedeemPoint;
    double FourWRedeemPoint;
    double ERICKRedeemPoint;
    double Loyality_points1new;
    double Loyality_points2new;
    double Loyality_pointsLW;
    double Loyality_pointsHW;

    Double Loyality_points_dh6,ibups_fourwpoints;
    double Loyality_points_dh7;
    double Loyality_points_dh8;
    double Loyality_points_dh9;
    double Loyality_points_dh10;
    double Loyality_points_dh11;

    LinearLayout points_hw_li,points_sec_li;

    PrefManager pref;

    RequestListener loginlistener = new RequestListener() {
        @Override
        public void onRequestStarted() {
        }

        @Override
        public void onRequestCompleted(final Object responseObject) {
            Log.d("response manualLogin", responseObject.toString());
            final LoginResponseModel responsemodel = JsonUtils.objectify(responseObject.toString(), LoginResponseModel.class);
            pref = PrefManager.getInstance(getContext());

            if (responsemodel == null || responsemodel.getData() == null || responsemodel.getData().getUser_id().isEmpty()) {
                getActivity().runOnUiThread(new Thread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                        //Toast.makeText(getContext(), responseObject.toString(), Toast.LENGTH_SHORT).show();
                    }
                }));

                return;
            }
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                    pref.saveUserInfo(responsemodel);

                    /*ImageLoader.getInstance().loadImage(responsemodel.getData().getSmall_image(), new ImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String imageUri, View view) {
                        }

                        @Override
                        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        }

                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                            profile_pic.setImageBitmap(loadedImage);
                        }

                        @Override
                        public void onLoadingCancelled(String imageUri, View view) {
                        }
                    });*/

                    Glide
                            .with(getContext())
                            .load(pref.getSmallImage())
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .centerCrop()
                            .into(profile_pic);
                }
            });

        }

        @Override
        public void onRequestError(int errorCode, String message) {
            handelError(errorCode, message);
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);

                }
            });
        }
    };
    RequestListener newpointslistner=new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @Override
        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
            Log.d("response getPoints", responseObject.toString());
            try {
                JSONObject json = new JSONObject(responseObject.toString());    // create JSON obj from string
                pointsnew= Double.parseDouble(json.getString("total_loyality"));
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        total_purchase_count.setText(pointsnew + "");
                        points = String.valueOf(pointsnew);
                    }
                });

            }catch (Exception e){
                return ;
            }
        }

        @Override
        public void onRequestError(int errorCode, String message) {

        }
    };
//    RequestListener newsecondarylistner=new RequestListener() {
//        @Override
//        public void onRequestStarted() {
//
//        }
//
//        @Override
//        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
//            Log.d("response getPoints", responseObject.toString());
//            try {
//                JSONObject json = new JSONObject(responseObject.toString());    // create JSON obj from string
//                pointsnew= Integer.parseInt(json.getString("total_loyality"));
//                getActivity().runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
////                        Toast.makeText(context, ""+pointsnew, Toast.LENGTH_SHORT).show();
////                        total_sec_ibups_points.setText(String.valueOf(pointsnew));
////                        total_sec_ibups_points_dh.setText("" + total);
//
//                    }
//                });
//
//            }catch (Exception e){
//                return ;
//            }
//
//        }
//
//        @Override
//        public void onRequestError(int errorCode, String message) {
//
//        }
//    };
    RequestListener pointsListner = new RequestListener() {
        @Override
        public void onRequestStarted() {
        }

        @Override
        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
            Log.d("response getPoints", responseObject.toString());
            JSONArray jsonArray = new JSONArray(responseObject.toString());
            getneschemepoints();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jObj = jsonArray.getJSONObject(i);
                String cat = jObj.getString("cat");
                try {

                    if (cat.equals("First")) {
                        count1 = Integer.parseInt(jObj.getString("count"));
                        Loyality_points1 = Double.parseDouble(jObj.getString("loyalty_points"));
                    } else if (cat.equals("Second")) {
                        count2 = Integer.parseInt(jObj.getString("count"));
                        Loyality_points2 = Double.parseDouble(jObj.getString("loyalty_points"));
                    } /*else if (cat.equals("Third")) {
                        count3 = Integer.parseInt(jObj.getString("count"));
                        Loyality_points3 = Double.parseDouble(jObj.getString("loyalty_points"));
                    } else if (cat.equals("Fourth")) {
                        count4 = Integer.parseInt(jObj.getString("count"));
                        Loyality_points4 = Double.parseDouble(jObj.getString("loyalty_points"));
                    }*/ else if (cat.equals("Fifth")) {
                        count5 = Integer.parseInt(jObj.getString("count"));
                        Loyality_points5 = Integer.parseInt(jObj.getString("loyalty_points"));
                    } else if (cat.equals("IB(Tertiary)")) {
                        IBRedeemPoint = Double.parseDouble(jObj.getString("loyalty_points"));
                    } /*else if (cat.equals("4W(Tertiary)")) {
                        FourWRedeemPoint = Double.parseDouble(jObj.getString("loyalty_points"));
                    }*/ else if (cat.equals("ERICK(Tertiary)")) {
                        ERICKRedeemPoint = Double.parseDouble(jObj.getString("loyalty_points"));
                    } else if (cat.equals("First1")) {
                        count1new = Integer.parseInt(jObj.getString("count"));
                        Loyality_points1new = Double.parseDouble(jObj.getString("loyalty_points"));
                    } else if (cat.equals("Second1")) {
                        count2new = Integer.parseInt(jObj.getString("count"));
                        Loyality_points2new = Double.parseDouble(jObj.getString("loyalty_points"));
                    } /*else if (cat.equals("Fourth1")) {
                        count4new = Integer.parseInt(jObj.getString("count"));
                        Loyality_points4new = Double.parseDouble(jObj.getString("loyalty_points"));
                    }*/

                } catch (NumberFormatException ex) {
                    ex.printStackTrace();
                }
            }


            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //--For Ib and ups
                    double total = Math.round((count1 * Loyality_points1) + (count2 * Loyality_points2) + (count1new * Loyality_points1new) + (count2new * Loyality_points2new));

                    //Reduce the redeem point
                    total = total - IBRedeemPoint;
//                    if (total >= 1100 && total < 2200) {
//                        points = String.valueOf(1100);
//                    } else if (total >= 2200 && total < 3300) {
//                        points = String.valueOf(2200);
//                    } else if (total >= 3300 && total < 4400) {
//                        points = String.valueOf(3300);
//                    } else if (total >= 4400 && total < 5500) {
//                        points = String.valueOf(4400);
//                    } else if (total >= 5500 && total < 6600) {
//                        points = String.valueOf(6875);
//                    } else if (total >= 6600 && total < 7700) {
//                        points = String.valueOf(7975);
//                    } else if (total >= 7700 && total < 8800) {
//                        points = String.valueOf(9075);
//                    } else if (total >= 8800 && total < 9900) {
//                        points = String.valueOf(10175);
//                    } else if (total >= 9900 && total < 11000) {
//                        points = String.valueOf(11275);
//                    } else if (total >= 11000 && total < 12100) {
//                        points = String.valueOf(13750);
//                    } else if (total >= 12100 && total < 13200) {
//                        points = String.valueOf(14850);
//                    } else if (total >= 13200 && total < 14300) {
//                        points = String.valueOf(15950);
//                    } else if (total >= 14300 && total < 15400) {
//                        points = String.valueOf(17050);
//                    } else if (total >= 15400 && total < 16500) {
//                        points = String.valueOf(18150);
//                    } else if (total >= 16500 && total < 22000) {
//                        points = String.valueOf(20625);
//                    } else if (total >= 22000 && total < 27500) {
//                        points = String.valueOf(27500);
//                    } else if (total >= 27500 && total < 33000) {
//                        points = String.valueOf(34375);
//                    } else if (total >= 33000 && total < 38500) {
//                        points = String.valueOf(41250);
//                    } else if (total >= 38500 && total < 44000) {
//                        points = String.valueOf(48125);
//                    } else if (total >= 44000 && total < 49500) {
//                        points = String.valueOf(55000);
//                    } else if (total >= 49500 && total < 55000) {
//                        points = String.valueOf(61875);
//                    } else if (total >= 55000 && total < 60500) {
//                        points = String.valueOf(68750);
//                    } else if (total >= 60500 && total < 66000) {
//                        points = String.valueOf(75625);
//                    } else if (total >= 66000 && total < 71500) {
//                        points = String.valueOf(82500);
//                    } else if (total >= 71500 && total < 77000) {
//                        points = String.valueOf(89375);
//                    } else if (total >= 77000 && total < 82500) {
//                        points = String.valueOf(96250);
//                    } else if (total >= 82500) {
//                        points = String.valueOf(103125);
//                    } else {
//                        points = String.valueOf(0);
//                    }
//
//                    curval1 = count1 + count1new;
//                    curval2 = count2 + count2new;
//                    total_purchase_count.setText(points);
                    // total_ib_counts.setText("" + curval1);
                    //       total_ups_points.setText("" + curval2);

                   /* //--for 4w---\\
                    double proLoyality = (count3 * Loyality_points3) + (count4 * Loyality_points4) + (count4new * Loyality_points4new);

                    //Reduce the Redeem Points
                    proLoyality = proLoyality - FourWRedeemPoint;

                    if (proLoyality >= 866 && proLoyality < 1732) {
                        pointstwo = String.valueOf(866);
                    } else if (proLoyality >= 1733 && proLoyality < 2599) {
                        pointstwo = String.valueOf(1733);
                    } else if (proLoyality >= 2599 && proLoyality < 3465) {
                        pointstwo = String.valueOf(2599);
                    } else if (proLoyality >= 3465 && proLoyality < 5414) {
                        pointstwo = String.valueOf(3465);
                    } else if (proLoyality >= 5414 && proLoyality < 6280) {
                        pointstwo = String.valueOf(5414);
                    } else if (proLoyality >= 6280 && proLoyality < 7146) {
                        pointstwo = String.valueOf(6280);
                    } else if (proLoyality >= 7146 && proLoyality < 8013) {
                        pointstwo = String.valueOf(7146);
                    } else if (proLoyality >= 8013 && proLoyality < 8879) {
                        pointstwo = String.valueOf(8013);
                    } else if (proLoyality >= 8879) {
                        pointstwo = String.valueOf(8879);
                    } else {
                        pointstwo = String.valueOf(0);
                    }

                    curval1 = count3;
                    curval2 = count4;
                    total_hw_counts.setText("" + count3);
                    total_lw_counts.setText("" + (count4 + count4new));
                    total_warranty_points.setText(pointstwo);*/

                    //---for E-rick---\\
                    int etotal = count5;
                    if (etotal < 240) {
                        total_e_loyality.setText("" + 0);
                        total_ericksha_count.setText("" + count5);
                        pointthree = String.valueOf(0);
                    } else if (etotal >= 240) {
                        int NewPoint = count5 * Loyality_points5;
                        NewPoint = (int) (NewPoint - ERICKRedeemPoint);
                        pointthree = String.valueOf(NewPoint);
                        total_e_loyality.setText("" + count5);
                        total_ericksha_count.setText("" + NewPoint);
                    } else if (etotal >= 480) {
                        int secondstage = count5 * Loyality_points5;
                        secondstage = (int) (secondstage - ERICKRedeemPoint);
                        pointthree = String.valueOf(secondstage);
                        total_e_loyality.setText("" + count5);
                        total_ericksha_count.setText("" + secondstage);
                    } else if (etotal >= 960) {
                        int thirdStage = count5 * Loyality_points5;
                        thirdStage = (int) (thirdStage - ERICKRedeemPoint);
                        pointthree = String.valueOf(thirdStage);
                        total_e_loyality.setText("" + count5);
                        total_ericksha_count.setText("" + thirdStage);
                    } else if (etotal >= 1920) {
                        int fourthStage = count5 * Loyality_points5;
                        fourthStage = (int) (fourthStage - ERICKRedeemPoint);
                        pointthree = String.valueOf(fourthStage);
                        total_e_loyality.setText("" + count5);
                        total_ericksha_count.setText("" + fourthStage);
                    } else if (etotal >= 3600) {
                        int fifthStage = count5 * Loyality_points5;
                        fifthStage = (int) (fifthStage - ERICKRedeemPoint);
                        pointthree = String.valueOf(fifthStage);
                        total_e_loyality.setText("" + count5);
                        total_ericksha_count.setText("" + fifthStage);
                    } else if (etotal >= 4800) {
                        int sixthstage = count5 * Loyality_points5;
                        sixthstage = (int) (sixthstage - ERICKRedeemPoint);
                        pointthree = String.valueOf(sixthstage);
                        total_e_loyality.setText("" + count5);
                        total_ericksha_count.setText("" + sixthstage);
                    } else {
                        total_e_loyality.setText("" + 0);
                        total_ericksha_count.setText("" + 0);
                        pointthree = String.valueOf(0);
                    }
                }
            });
        }

        @Override
        public void onRequestError(int errorCode, String message) {

        }
    };
    RequestListener onApiViewFlagAndData =new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @Override
        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
            Log.d("response State Flag", responseObject.toString());
            try {
                JSONObject jsonObject = new JSONObject(responseObject.toString());
                boolean flag = jsonObject.getBoolean("flag");
                if (flag) {
                    String state = jsonObject.getString("state");
                    if (state.toLowerCase().equals("delhi")) {
                        Log.e("state", state);
                        pref.setUserViewFlag(flag);
                        pref.setUserViewFlagWB(false);
                    }
                } else {
                    pref.setUserViewFlagWB(false);
                    pref.setUserViewFlag(false);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
//                    progressBar.setVisibility(View.GONE);

                }
            });
        }

        @Override
        public void onRequestError(int errorCode, String message) {

        }
    };
    RequestListener popupListner=new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @Override
        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
            Log.e("response homecountL", responseObject.toString());
            JSONObject jObj = new JSONObject(responseObject.toString());

            boolean isShow = false;
            if (jObj.has("is_show"))
                isShow = jObj.getBoolean("is_show");
            if (!isShow)
                return;
//            if (jObj.has("counts")){
//                JSONArray countsArray = jObj.getJSONArray("counts");
//                for(int i = 0; i < countsArray.length(); i++){
//                    JSONObject countObject = countsArray.getJSONObject(i);
//                    String name = countObject.getString("name");
//                    int count = countObject.getInt("count");
//                    if(name.equalsIgnoreCase("i2-verter") || name.equalsIgnoreCase("i-verter") || name.equalsIgnoreCase("iH-verter")){
//                        ivCount += count;
//                    } else {
//                        ibCount += count;
//                    }
//                }
//            }
            try{
                final String msg = (jObj.getString("msg"));
                final String title = (jObj.getString("title"));
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Handler mHandler = new Handler();
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                // int point = 1500 - Integer.parseInt(String.valueOf(loyality));
                                if (getActivity() != null) {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                    builder.setTitle(title);
                                    builder.setIcon(R.mipmap.ic_mainlauncher_round);
                                    builder.setCancelable(false);
                                    builder.setMessage(msg)
                                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {

                                                }
                                            });
                                    final AlertDialog alert = builder.create();
                                    if (getActivity() != null)
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                if (!getActivity().isFinishing()) {
                                                    try {
                                                        alert.show();
                                                    } catch (WindowManager.BadTokenException e) {
                                                        Log.e("WindowManagerBad ", e.toString());
                                                    } catch (Exception e) {
                                                        Log.e("Exception ", e.toString());
                                                    }
                                                }
                                            }
                                        });
                                }
                            }
                        });
                    }
                });
            }catch (Exception e){
                return;
            }
        }

        @Override
        public void onRequestError(int errorCode, String message) {

        }
    };
    RequestListener loyalityListner = new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onRequestCompleted(final Object responseObject) throws JSONException, ParseException {
            Log.d("response getloyality", responseObject.toString());
//            JSONArray jsonArray = new JSONArray(responseObject.toString());
//            for (int i = 0; i < jsonArray.length(); i++) {
//                JSONObject jObj = jsonArray.getJSONObject(i);
//                String cat = jObj.getString("cat");
//                if(pref.getUserViewFlag())
//                {
//                    if (cat.equals("IB")) {
//                        count6 = Integer.parseInt(jObj.getString("count"));
//                        Loyality_points_dh6 = Double.parseDouble(jObj.getString("loyalty_point"));
//                        Log.e("loyalty_point",jObj.getString("loyalty_point"));
//                    } else if (cat.equals("INV")) {
//                        count7 = Integer.parseInt(jObj.getString("count"));
//                        Loyality_points_dh7 = Double.parseDouble(jObj.getString("loyalty_point"));
//                    } else if(cat.equals("4W")){
//                        count8 = Integer.parseInt(jObj.getString("count"));
//                        Loyality_points_dh8 = Double.parseDouble(jObj.getString("loyalty_point"));
//                    }else if (cat.equals("E-RICKSHAW")) {
//                        countERick_pr = Integer.parseInt(jObj.getString("count"));
//                        Loyality_points_erick_pr = Double.parseDouble(jObj.getString("loyalty_point"));
//                    }
//
//                }else {
//                    if (cat.equals("First")) {
//                        count6 = Integer.parseInt(jObj.getString("count"));
//                        Loyality_points6 = Integer.parseInt(jObj.getString("loyalty_point"));
//                    } else if (cat.equals("Second")) {
//                        count7 = Integer.parseInt(jObj.getString("count"));
//                        Loyality_points7 = Integer.parseInt(jObj.getString("loyalty_point"));
//                    } else if (cat.equals("E-rick")) {
//                        countERick_pr = Integer.parseInt(jObj.getString("count"));
//                        Loyality_points_erick_pr = Integer.parseInt(jObj.getString("loyalty_point"));
//                    } else if (cat.equals("HW")) {
//                        countHW = Integer.parseInt(jObj.getString("count"));
//                        Loyality_pointsHW = Double.parseDouble(jObj.getString("loyalty_point"));
//                    } else if (cat.equals("LW")) {
//                        countLW = Integer.parseInt(jObj.getString("count"));
//                        Loyality_pointsLW = Double.parseDouble(jObj.getString("loyalty_point"));
//                    }
//                }
//            }
            try{
                JSONObject jsonObject=new JSONObject(responseObject.toString());
                 ibups_fourwpoints= Double.parseDouble(jsonObject.getString("ib_ups_fourwsecpoints"));
                 countERick_pr= Integer.parseInt(jsonObject.getString("ericksecpoints"));

            }catch (Exception ignored){

            }

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    total_ericksha_count_pr.setText(countERick_pr + "");
                    if (countERick_pr >= 180 && countERick_pr < 300) {
                        total_e_loyality_pr.setText("180");
                    } else if (countERick_pr >= 300 && countERick_pr < 600) {
                        total_e_loyality_pr.setText("300");
                    } else if (countERick_pr >= 600 && countERick_pr < 1200) {
                        total_e_loyality_pr.setText("600");
                    } else if (countERick_pr >= 1200 && countERick_pr < 1800) {
                        total_e_loyality_pr.setText("1200");
                    } else if (countERick_pr >= 1800 && countERick_pr < 2400) {
                        total_e_loyality_pr.setText("1800");
                    } else if (countERick_pr >= 2400) {
                        total_e_loyality_pr.setText("2400");
                    } else {
                        total_e_loyality_pr.setText("0");
                    }

                }
            });

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if(pref.getUserViewFlag())
                    {
                        Log.e("total", String.valueOf(ibups_fourwpoints));
                        total_sec_ibups_points.setText("" + ibups_fourwpoints);
                        poitnsfour = String.valueOf(ibups_fourwpoints);
                    }
                    else {
                        total_sec_ibups_points.setText(String.valueOf(ibups_fourwpoints));
                        poitnsfour= String.valueOf(ibups_fourwpoints);
//                        double total = count6 * (Loyality_points6) + count7 * (Loyality_points7);
//                        if (total == 50 && total < 99) {
//                            text = "1 Coupon";
//                            total_sec_ibups_points.setText("" + text);
//                            poitnsfour = String.valueOf(text);
//                            if (total == 50) {
//                                curval1 = count1;
//                                curval2 = count2;
//                               // total_ib_sec_points.setText("" + 0);
//                              //  total_ups_points_sec.setText("" + 0);
//                            } else if (total > 50 && total <= 99) {
//                                double points1 = count1 - curval1;
//                                double points2 = count2 - curval2;
//                               // total_ib_sec_points.setText("" + (int) points1);
//                              //  total_ups_points_sec.setText("" + (int) points2);
//                            }
//                        } else if (total >= 100 && total < 149) {
//                            text = "2 Coupon";
//                            total_sec_ibups_points.setText("" + text);
//                            poitnsfour = String.valueOf(text);
//                            if (total == 100) {
//                                curval1 = count1;
//                                curval2 = count2;
//                               // total_ib_sec_points.setText("" + 0);
//                               // total_ups_points_sec.setText("" + 0);
//                            } else if (total > 100 && total <= 149) {
//                                double points1 = count1 - curval1;
//                                double points2 = count2 - curval2;
//                               // total_ib_sec_points.setText((int) points1 + "");
//                              //  total_ups_points_sec.setText((int) points1 + "");
//                            }
//                        } else if (total >= 150 && total < 199) {
//                            text = "3 Coupon";
//                            total_sec_ibups_points.setText("" + text);
//                            poitnsfour = String.valueOf(text);
//                            if (total == 150) {
//                                curval1 = count1;
//                                curval2 = count2;
//                               // total_ib_sec_points.setText("" + 0);
//                                total_ups_points_sec.setText("" + 0);
//                            } else if (total > 150 && total <= 199) {
//                                double points1 = count1 - curval1;
//                                double points2 = count2 - curval2;
//                               // total_ib_sec_points.setText("" + (int) points1);
//                                //total_ups_points_sec.setText("" + (int) points2);
//                            }
//                        } else if (total >= 200 && total < 249) {
//                            text = "4 Coupon";
//                            total_sec_ibups_points.setText("" + text);
//                            poitnsfour = String.valueOf(text);
//                            if (total == 200) {
//                                curval1 = count1;
//                                curval2 = count2;
//                               // total_ib_sec_points.setText("" + 0);
//                               // total_ups_points_sec.setText("" + 0);
//                            } else if (total > 200 && total <= 249) {
//                                double points1 = count1 - curval1;
//                                double points2 = count2 - curval2;
//                              //  total_ib_sec_points.setText("" + (int) points1);
//                              //  total_ups_points_sec.setText("" + (int) points2);
//                            }
//                        } else if (total >= 250) {
//                            text = "5 Coupon";
//                            total_purchase_count.setText("" + text);
//                            poitnsfour = String.valueOf(text);
//                            if (total == 250) {
//                                curval1 = count1;
//                                curval2 = count2;
//                              //  total_ib_sec_points.setText("" + 0);
//                              //  total_ups_points_sec.setText("" + 0);
//                            } else {
//                                double points1 = count1 - curval1;
//                                double points2 = count2 - curval2;
//                              //  total_ib_sec_points.setText("" + (int) points1);
//                               // total_ups_points_sec.setText("" + (int) points2);
//                            }
//                        } else {
//                            text = "0";
//                           // total_ib_sec_points.setText("" + count7);
//                          //  total_ups_points_sec.setText("" + count6);
//                            total_sec_ibups_points.setText("" + text);
//                            poitnsfour = String.valueOf(text);
//                        }
                    }
                }
            });

//            getActivity().runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    //--for 4w---\\
//                    double proLoyality = (countLW * Loyality_pointsLW) + (countHW * Loyality_pointsHW);
//
//                    proLoyality = Math.round(proLoyality);
//
//                    //Reduce the Redeem Points
//                    //proLoyality = proLoyality - FourWRedeemPoint;
//                    if (proLoyality >= 1025 && proLoyality < 2050) {
//                        pointstwo = String.valueOf(1025);
//                    } else if (proLoyality >= 2050 && proLoyality < 3075) {
//                        pointstwo = String.valueOf(2050);
//                    } else if (proLoyality >= 3075 && proLoyality < 4100) {
//                        pointstwo = String.valueOf(3075);
//                    } else if (proLoyality >= 4100 && proLoyality < 5125) {
//                        pointstwo = String.valueOf(4100);
//                    } else if (proLoyality >= 5125 && proLoyality < 6406) {
//                        pointstwo = String.valueOf(6406);
//                    } else if (proLoyality >= 6150 && proLoyality < 7175) {
//                        pointstwo = String.valueOf(7431);
//                    } else if (proLoyality >= 7175 && proLoyality < 8200) {
//                        pointstwo = String.valueOf(8456);
//                    } else if (proLoyality >= 8200 && proLoyality < 9225) {
//                        pointstwo = String.valueOf(9481);
//                    } else if (proLoyality >= 9225 && proLoyality < 10250) {
//                        pointstwo = String.valueOf(10506);
//                    } else if (proLoyality >= 10250 && proLoyality < 11275) {
//                        pointstwo = String.valueOf(12812);
//                    } else if (proLoyality >= 11275 && proLoyality < 12300) {
//                        pointstwo = String.valueOf(13837);
//                    } else if (proLoyality >= 12300 && proLoyality < 13325) {
//                        pointstwo = String.valueOf(14862);
//                    } else if (proLoyality >= 13325 && proLoyality < 14350) {
//                        pointstwo = String.valueOf(15887);
//                    } else if (proLoyality >= 14350 && proLoyality < 15375) {
//                        pointstwo = String.valueOf(16912);
//                    } else if (proLoyality >= 15375 && proLoyality < 16400) {
//                        pointstwo = String.valueOf(19218);
//                    } else if (proLoyality >= 16400 && proLoyality < 17425) {
//                        pointstwo = String.valueOf(20243);
//                    } else if (proLoyality >= 17425 && proLoyality < 18450) {
//                        pointstwo = String.valueOf(21268);
//                    } else if (proLoyality >= 18450 && proLoyality < 19475) {
//                        pointstwo = String.valueOf(22293);
//                    } else if (proLoyality >= 19475 && proLoyality < 20500) {
//                        pointstwo = String.valueOf(23318);
//                    } else if (proLoyality >= 20500 && proLoyality < 21525) {
//                        pointstwo = String.valueOf(25624);
//                    } else if (proLoyality >= 21525 && proLoyality < 22550) {
//                        pointstwo = String.valueOf(26649);
//                    } else if (proLoyality >= 22550 && proLoyality < 23575) {
//                        pointstwo = String.valueOf(27674);
//                    } else if (proLoyality >= 23575 && proLoyality < 24600) {
//                        pointstwo = String.valueOf(28699);
//                    } else if (proLoyality >= 24600 && proLoyality < 25625) {
//                        pointstwo = String.valueOf(29724);
//                    } else if (proLoyality >= 25625 && proLoyality <26650) {
//                        pointstwo = String.valueOf(32030);
//                    }  else if( proLoyality >=26650 && proLoyality<27675){
//                        pointstwo = String.valueOf(33055);
//                    } else if( proLoyality >=27675 && proLoyality<28700){
//                        pointstwo = String.valueOf(34080);
//                    }else if( proLoyality >=28700 && proLoyality<29725){
//                        pointstwo = String.valueOf(35105);
//                    }else if( proLoyality >=29725 && proLoyality<30750){
//                        pointstwo = String.valueOf(36130);
//                    }else if( proLoyality >=30750 && proLoyality<31775){
//                        pointstwo = String.valueOf(38436);
//                    }else if( proLoyality >=31775 && proLoyality<32800){
//                        pointstwo = String.valueOf(44842);
//                    }else if( proLoyality >=32800){
//                        pointstwo = String.valueOf(51248);
//                    }
//                    else {
//                        pointstwo = String.valueOf(0);
//                    }
//                    /*curval1 = count3;
//                    curval2 = count4;*/
//                  //  total_hw_counts_pr.setText("" + countHW);
//                  //  total_lw_counts_pr.setText("" + countLW);
//                    //total_warranty_points_pr.setText(pointstwo);
//                }
//            });
        }

        @Override
        public void onRequestError(int errorCode, String message) {

        }
    };


    private AutoScrollViewPager viewPager;
    RequestListener bannerListener = new RequestListener() {
        @Override
        public void onRequestStarted() {
        }

        @Override
        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
            Log.d("response GetBanner", responseObject.toString());
            banner.clear();
            Type collectionType = new TypeToken<List<BannerModel>>() {
            }.getType();
            final List<BannerModel> ca = (List<BannerModel>) new Gson().fromJson(responseObject.toString(), collectionType);
            for (int i = 0; i < ca.size(); i++) {
                banner.add(ca.get(i).getLarge_image());
            }
            if (((Activity) getContext()) != null) {
                ((Activity) getContext()).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mySwiprRefreshLayout.setRefreshing(false);
                        viewPager.setplaceImage(R.drawable.empty_graphical_banner);
                        viewPager.setImageUrls(banner);
                        viewPager.start();
                        viewPager.setAutoPagerListener(new AutoScrollViewPager.AutoScrollerViewPagerListener() {
                            @Override
                            public void onPageSelected(int position) {
                            }

                            @Override
                            public void onPageClicked(int position) {
                            }
                        });
                    }
                });
            }
        }

        @Override
        public void onRequestError(int errorCode, String message) {
            if ((Activity) getContext() != null) {
                ((Activity) getContext()).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mySwiprRefreshLayout.setRefreshing(false);

                    }
                });
            }
            handelError(errorCode, message);
        }
    };

    @SuppressLint("ValidFragment")
    public home_fragment(Context context1) {
        context = context1;
    }


    public home_fragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.home_fragment, container, false);
    }

    @SuppressLint({"SetTextI18n", "NewApi"})
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        progressBar = getView().findViewById(R.id.progressbar);
        profile_pic = getView().findViewById(R.id.profilePic);
        dealer_name_tv = getView().findViewById(R.id.distributorNameTv);
        dealershipNameTv = getView().findViewById(R.id.dealershipNameTv);
        partnerNameTv = getView().findViewById(R.id.partnerNameTv);
        phone_no = getView().findViewById(R.id.dealer_no);
        viewPager = getView().findViewById(R.id.viewpager);
        total_purchase_count = getView().findViewById(R.id.total_purchase_points);
        total_warranty_points = getView().findViewById(R.id.total_warranty_points);
        //  total_warranty_points_pr = getView().findViewById(R.id.total_warranty_points_pr);
        /*manufacturer_spinner = getView().findViewById(R.id.manufacturer_spinner);
        car_model_spinner = getView().findViewById(R.id.car_model_spinner);
        fuel_spinner = getView().findViewById(R.id.fuel_spinner);*/
        points_RL = getView().findViewById(R.id.points_RL);
        points_wrranty = getView().findViewById(R.id.points_RL1);
        //points_wrranty_pr = getView().findViewById(R.id.points_RL1_pr);
        profilePicRL = getView().findViewById(R.id.profilePicRL);
        battery_model_rv = getView().findViewById(R.id.battery_model);
        // total_ib_counts = getView().findViewById(R.id.total_ib_points);
        //  total_ups_points = getView().findViewById(R.id.total_ups_points);
        total_hw_counts = getView().findViewById(R.id.total_hw_points);
        //  total_hw_counts_pr = getView().findViewById(R.id.total_hw_points_pr);
        total_lw_counts = getView().findViewById(R.id.total_lw_points);
        //  total_lw_counts_pr = getView().findViewById(R.id.total_lw_points_pr);
        points_RL_purchase = getView().findViewById(R.id.points_RL_purchase);
        mySwiprRefreshLayout = getView().findViewById(R.id.swiperefresh);
        mySwiprRefreshLayout.setEnabled(false);
        points_RL_tertiary = getView().findViewById(R.id.points_RL_tertiary);
        points_RL_ericksha = getView().findViewById(R.id.points_RL_ericksha);
        points_RL_ericksha_pr = getView().findViewById(R.id.points_RL_ericksha_pr);
        // points_Ib = getView().findViewById(R.id.points_Ib);
        //  points_ups = getView().findViewById(R.id.points_ups);
        points_hw = getView().findViewById(R.id.points_hw);
        // points_hw_pr = getView().findViewById(R.id.points_hw_pr);
        points_lw = getView().findViewById(R.id.points_lw);
        // points_lw_pr = getView().findViewById(R.id.points_lw_pr);
        loyalty_ricksha = getView().findViewById(R.id.loyalty_ricksha);
        loyalty_ricksha_pr = getView().findViewById(R.id.loyalty_ricksha_pr);
        total_ericksha_count = getView().findViewById(R.id.total_ricksha_count);
        total_ericksha_count_pr = getView().findViewById(R.id.total_ricksha_count_pr);
        total_e_loyality = getView().findViewById(R.id.total_ericksha_points);
        total_e_loyality_pr = getView().findViewById(R.id.total_ericksha_points_pr);
        points_sec_ibups = getView().findViewById(R.id.points_sec_ibups);
        // points__sec_Ib = getView().findViewById(R.id.points__sec_Ib);
        // points_ups_sec = getView().findViewById(R.id.points_ups_sec);
        total_sec_ibups_points = getView().findViewById(R.id.total_sec_ibups_points);
        //  total_ib_sec_points = getView().findViewById(R.id.total_ib_sec_points);
        //total_ups_points_sec = getView().findViewById(R.id.total_ups_points_sec);
        // points_hw_li = getView().findViewById(R.id.points_hw_li);
        points_sec_li = getView().findViewById(R.id.points_sec_li);
        points_heading_tertiary = getView().findViewById(R.id.points_heading_tertiary);
        points_heading_purchase = getView().findViewById(R.id.points_heading_purchase);

        total_sec_ibups_points_dh = getView().findViewById(R.id.total_sec_ibups_points_dh);
        points_heading_sec_ibups_dh = getView().findViewById(R.id.points_heading_sec_ibups_dh);
        points_sec_ibups_dh = getView().findViewById(R.id.points_sec_ibups_dh);

        databaseBackend = DatabaseBackend.getInstance(getContext());

        banner = new ArrayList<>();

        Controller.GetBanner(getContext(), bannerListener);


        // getUserData();

        pref = PrefManager.getInstance(getActivity());

        if (pref != null) {

            dealer_name_tv.setText(pref.getDistributorName());
            phone_no.setText("" + pref.getPhoneNo());
            dealershipNameTv.setText(pref.getDealershipName());

            getpopup();
            delhiwd();
            Glide
                    .with(this)
                    .load(pref.getSmallImage())
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .centerCrop()
                    .into(profile_pic);
            //profile_pic.setImageURI(Uri.parse(pref.getSmallImage()));

         /*   ImageLoader.getInstance().loadImage(pref.getSmallImage(), new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    profile_pic.setImageBitmap(loadedImage);
                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {
                }
            });*/
        }

        final LoyalityModel loyalityModel=new LoyalityModel();

        points_RL_purchase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                getsecondarypoints();
                loyalityModel.setJobtype("secondary");
                getData();


                points_heading_tertiary.setTextColor(Color.parseColor("#ea5e20"));
                points_heading_purchase.setTextColor(Color.parseColor("#FFFFFF"));

                points_RL_purchase.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                points_RL_tertiary.setBackground(getResources().getDrawable(R.drawable.white_rounded_background));

                points_RL.setVisibility(View.GONE);
                points_wrranty.setVisibility(View.GONE);
                // points_wrranty_pr.setVisibility(View.VISIBLE);
                points_RL_ericksha.setVisibility(View.GONE);
                points_RL_ericksha_pr.setVisibility(View.VISIBLE);
                //  points_Ib.setVisibility(View.GONE);
                //  points_ups.setVisibility(View.GONE);
                points_hw.setVisibility(View.GONE);
                points_lw.setVisibility(View.GONE);
                loyalty_ricksha.setVisibility(View.GONE);
                loyalty_ricksha_pr.setVisibility(View.VISIBLE);


                if(pref.getUserViewFlag())
                {
                    //  points_wrranty_pr.setVisibility(View.GONE);
                    //  points__sec_Ib.setVisibility(View.GONE);
                    //  points_ups_sec.setVisibility(View.GONE);
                    /*points_hw_pr.setVisibility(View.GONE);
                    points_lw_pr.setVisibility(View.GONE);*/
                    //  points_hw_li.setVisibility(View.GONE);
                    points_sec_ibups.setVisibility(View.VISIBLE);
                    points_sec_ibups_dh.setVisibility(View.GONE);
                }else {
                    //  points__sec_Ib.setVisibility(View.VISIBLE);
                    //  points_ups_sec.setVisibility(View.VISIBLE);
                    //  points_wrranty_pr.setVisibility(View.VISIBLE);
                    //   points_hw_li.setVisibility(View.VISIBLE);
//                    points_sec_ibups_dh.setVisibility(View.VISIBLE);
//                    points_sec_ibups.setVisibility(View.GONE);
                    points_sec_ibups.setVisibility(View.VISIBLE);
                    points_sec_ibups_dh.setVisibility(View.GONE);
                    //  points_hw_pr.setVisibility(View.VISIBLE);
                    //  points_lw_pr.setVisibility(View.VISIBLE);
                }

                /*points__sec_Ib.setVisibility(View.VISIBLE);
                points_ups_sec.setVisibility(View.VISIBLE);*/

            }
        });
        points_RL_tertiary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PointsModel pointsModel = new PointsModel();
                pointsModel.setUser_id(PrefManager.getInstance(getActivity()).getÜserId());
                Controller.getPoints(getContext(), pointsModel, pointsListner);

                points_heading_purchase.setTextColor(Color.parseColor("#ea5e20"));
                points_heading_tertiary.setTextColor(Color.parseColor("#FFFFFF"));

                points_RL_tertiary.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                points_RL_purchase.setBackground(getResources().getDrawable(R.drawable.white_rounded_background));

                points_sec_ibups.setVisibility(View.GONE);
                //  points__sec_Ib.setVisibility(View.GONE);
                //  points_ups_sec.setVisibility(View.GONE);
                points_wrranty.setVisibility(View.VISIBLE);
                //   points_wrranty_pr.setVisibility(View.GONE);
                points_RL_ericksha.setVisibility(View.VISIBLE);
                points_RL_ericksha_pr.setVisibility(View.GONE);
                points_hw.setVisibility(View.VISIBLE);
                // points_hw_pr.setVisibility(View.GONE);
                points_lw.setVisibility(View.VISIBLE);
                //  points_lw_pr.setVisibility(View.GONE);
                loyalty_ricksha.setVisibility(View.VISIBLE);
                loyalty_ricksha_pr.setVisibility(View.GONE);
                points_sec_ibups_dh.setVisibility(View.GONE);

                if(pref.getUserViewFlag())
                {
                    points_sec_li.setVisibility(View.VISIBLE);
                    points_RL.setVisibility(View.VISIBLE);
//                    points_sec_li.setVisibility(View.GONE);
                }else {
                    points_sec_li.setVisibility(View.VISIBLE);
                    points_RL.setVisibility(View.VISIBLE);
                    //    points_Ib.setVisibility(View.VISIBLE);
                    //   points_ups.setVisibility(View.VISIBLE);
                }

            }
        });

        points_RL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), RedeemActivity.class);
                intent.putExtra("screen", 1);
                intent.putExtra("points", points);
                startActivity(intent);


            }
        });
        points_RL_ericksha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), RedeemActivity.class);
                intent.putExtra("screen", 3);
                intent.putExtra("points", pointthree);
                startActivity(intent);

                /*
                By Sanjay
                Intent intent = new Intent(getContext(), ErickshaRedeemActivity.class);
                intent.putExtra("point3",pointthree);
                startActivity(intent);*/
            }
        });
        points_RL_ericksha_pr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), RedeemActivity.class);
                intent.putExtra("screen", 4);
                intent.putExtra("points", total_e_loyality_pr.getText().toString().trim());
                startActivity(intent);
            }
        });
        points_wrranty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), RedeemActivity.class);
                intent.putExtra("screen", 2);
                intent.putExtra("points", pointstwo);
                startActivity(intent);
                /*
                Sanjay
                Intent intent = new Intent(getContext(), FourWheelerRedeemActivity.class);
                intent.putExtra("points1",pointstwo);
                startActivity(intent);*/
            }
        });
//        points_wrranty_pr.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(getContext(), RedeemActivity.class);
//                intent.putExtra("screen", 2);
//                intent.putExtra("points", pointstwo);
//                startActivity(intent);
//                /*
//                Sanjay
//                Intent intent = new Intent(getContext(), FourWheelerRedeemActivity.class);
//                intent.putExtra("points1",pointstwo);
//                startActivity(intent);*/
//            }
//        });
        points_sec_ibups.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), Activity_sec_ibups_redeem.class);
                intent.putExtra("points4", poitnsfour);
                startActivity(intent);
            }
        });

        points_sec_ibups_dh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), Activity_sec_ibups_redeem.class);
                intent.putExtra("points4", poitnsfour);
                startActivity(intent);
            }
        });

        profilePicRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), EditProfileActivity.class);
                startActivity(intent);
            }
        });
    }



    @Override
    public void onResume() {
        super.onResume();

        //getUserData();

       /* ImageLoader.getInstance().loadImage(PrefManager.getInstance(getContext()).getSmallImage(), new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                profile_pic.setImageBitmap(loadedImage);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
            }
        });*/

        Glide
                .with(this)
                .load(pref.getSmallImage())
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .centerCrop()
                .into(profile_pic);

        if (points_heading_purchase != null)
            points_heading_purchase.setTextColor(Color.parseColor("#ea5e20"));
        if (points_heading_tertiary != null)
            points_heading_tertiary.setTextColor(Color.parseColor("#ea5e20"));
        if (points_RL_purchase != null)
            points_RL_purchase.setBackground(getResources().getDrawable(R.drawable.white_rounded_background));
        if (points_RL_tertiary != null)
            points_RL_tertiary.setBackground(getResources().getDrawable(R.drawable.white_rounded_background));

        points_RL.setVisibility(View.GONE);
        points_wrranty.setVisibility(View.GONE);
        // points_wrranty_pr.setVisibility(View.GONE);
        points_RL_ericksha.setVisibility(View.GONE);
        points_RL_ericksha_pr.setVisibility(View.GONE);
        // points_Ib.setVisibility(View.GONE);
        // points_ups.setVisibility(View.GONE);
        points_hw.setVisibility(View.GONE);
        //  points_hw_pr.setVisibility(View.GONE);
        points_lw.setVisibility(View.GONE);
        // points_lw_pr.setVisibility(View.GONE);
        loyalty_ricksha.setVisibility(View.GONE);
        loyalty_ricksha_pr.setVisibility(View.GONE);
        points_sec_ibups.setVisibility(View.GONE);
        // points__sec_Ib.setVisibility(View.GONE);
        // points_ups_sec.setVisibility(View.GONE);
        points_sec_ibups_dh.setVisibility(View.GONE);
    }
    public void delhiwd()
    {
        //this is used to show the spacial view which is set from admin panel
        GetViewFlag getViewFlag = new GetViewFlag();
        getViewFlag.setId(pref.getÜserId());
        Controller.getViewFlagAndData(getContext(), getViewFlag, onApiViewFlagAndData);
    }

    public void getData()
    {
        LoyalityModel loyalityModel = new LoyalityModel();
        loyalityModel.setUser_id(pref.getÜserId());
        Controller.getloyality(getContext(), loyalityModel, loyalityListner);
    }

    public void getneschemepoints(){
        LoyalityModel loyalityModel=new LoyalityModel();
        loyalityModel.setUser_id((pref.getÜserId()));
        Controller.gettotalloyalty(getContext(),loyalityModel,newpointslistner);
    }

//    public void  getsecondarypoints(){
//        LoyalityModel loyalityModel=new LoyalityModel();
//        loyalityModel.setUser_id(pref.getÜserId());
//        Controller.getsecondarypoints(getContext(),loyalityModel,newsecondarylistner);
//    }
//    public void getUserData()
//    {
//        LoginModel loginModel = new LoginModel();
//        loginModel.setJob_Type("Login");
//        loginModel.setBat_code(loginModel.getBat_code());
//        loginModel.setPassword(loginModel.getPassword());
//
//        //call API
//        Controller.manualLogin(getActivity(), loginModel, loginlistener);
//
//        /*ImageLoader.getInstance().loadImage(pref.getSmallImage(), new ImageLoadingListener() {
//            @Override
//            public void onLoadingStarted(String imageUri, View view) {
//            }
//
//            @Override
//            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
//            }
//
//            @Override
//            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
//                profile_pic.setImageBitmap(loadedImage);
//            }
//
//            @Override
//            public void onLoadingCancelled(String imageUri, View view) {
//            }
//        });*/
//
//    }
    public  void getpopup(){
        HomeCountModel homeCountModel=new HomeCountModel();
        homeCountModel.setUser_id(pref.getÜserId());
        Controller.getpopup(getContext(),homeCountModel,popupListner);
    }
}
