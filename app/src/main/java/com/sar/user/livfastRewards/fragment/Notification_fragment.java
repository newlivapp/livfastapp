package com.sar.user.livfastRewards.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sar.user.livfastRewards.Interface.RequestListener;
import com.sar.user.livfastRewards.Model.NotificationModel;
import com.sar.user.livfastRewards.R;
import com.sar.user.livfastRewards.adapter.NotificationRecyclerViewAdapter;

import org.json.JSONException;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 28-03-2017.
 */

public class Notification_fragment extends BaseFragment {
    Context context;
    List<NotificationModel> notificationModelList;
    NotificationRecyclerViewAdapter notification_adapter;
    RecyclerView notification_rv;
    //    ProgressBar progressBar;
    SwipeRefreshLayout swipeRefreshLayout;
    RelativeLayout empty_notification_image;
    RequestListener notificationListner = new RequestListener() {
        @Override
        public void onRequestStarted() {
        }

        @Override
        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
            Log.d("response", responseObject.toString());
            notificationModelList.clear();
            Type collectionType = new TypeToken<List<NotificationModel>>() {
            }.getType();
            final List<NotificationModel> notifyList = (List<NotificationModel>) new Gson().fromJson(responseObject.toString(), collectionType);
            notificationModelList.addAll(notifyList);
            if ((Activity) getContext() != null) {
                ((Activity) getContext()).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefreshLayout.setRefreshing(false);
                        if (notificationModelList.isEmpty()) {
                            empty_notification_image.setVisibility(View.VISIBLE);
                        } else {
                            empty_notification_image.setVisibility(View.GONE);
                            notification_rv.setVisibility(View.VISIBLE);
                        }
                        notification_adapter.notifyDataSetChanged();
                    }
                });
            }
        }

        @Override
        public void onRequestError(int errorCode, String message) {
            if ((Activity) getContext() != null) {
                ((Activity) getContext()).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefreshLayout.setRefreshing(false);
                    }
                });
            }

            handelError(errorCode, message);

        }
    };

    @SuppressLint("ValidFragment")
    public Notification_fragment(Context context1) {
        context = context1;
    }

    public Notification_fragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.notification_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        notification_rv = (RecyclerView) getView().findViewById(R.id.notification_rv);
//        progressBar= (ProgressBar) getView().findViewById(R.id.progressbar);
        empty_notification_image = (RelativeLayout) getView().findViewById(R.id.empty_rl);
        swipeRefreshLayout = (SwipeRefreshLayout) getView().findViewById(R.id.swiperefresh);
        notificationModelList = new ArrayList<NotificationModel>();
        //  Controller.GetNotification(getContext(),notificationListner);
//        progressBar.setVisibility(View.VISIBLE);
        notification_adapter = new NotificationRecyclerViewAdapter(getContext(), notificationModelList);
        notification_rv.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        notification_rv.setAdapter(notification_adapter);
        notification_rv.setHasFixedSize(true);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(false);
                // Controller.GetNotification(getContext(),notificationListner);
            }
        });
    }

}
