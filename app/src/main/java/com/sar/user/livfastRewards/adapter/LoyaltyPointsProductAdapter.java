package com.sar.user.livfastRewards.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.horizontalnumberpicker.HorizontalNumberPicker;
import com.horizontalnumberpicker.HorizontalNumberPickerListener;
import com.sar.user.livfastRewards.Interface.OnItemRemoveListner;
import com.sar.user.livfastRewards.Model.ProductLoyalityModel;
import com.sar.user.livfastRewards.R;

import java.util.List;

//import com.tagbin.user.liveguardloyalty.Model.RedeemProductModel;

/**
 * Created by Nav on 15-Jun-17.
 */

public class LoyaltyPointsProductAdapter extends RecyclerView.Adapter<LoyaltyPointsProductAdapter.MyViewHolder> {
    Context context;
    List<ProductLoyalityModel> totaladdedList;
    HorizontalNumberPickerListener horizontalNumberPickerListener;
    OnItemRemoveListner onItemRemoveListner;

    public LoyaltyPointsProductAdapter(Context context1, List<ProductLoyalityModel> productList1, HorizontalNumberPickerListener horizontalNumberPicker1, OnItemRemoveListner onItemRemoveListner1) {
        context = context1;
        totaladdedList = productList1;
        this.horizontalNumberPickerListener = horizontalNumberPicker1;
        this.onItemRemoveListner = onItemRemoveListner1;
    }

    @Override
    public LoyaltyPointsProductAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.loyalty_points_products, parent, false);
        MyViewHolder mvh = new MyViewHolder(v);
        return mvh;
    }

    @Override
    public void onBindViewHolder(final LoyaltyPointsProductAdapter.MyViewHolder holder, final int position) {

        holder.quantity.setListener(horizontalNumberPickerListener);

        int productQquantity = totaladdedList.get(position).getQuantity();

        holder.product_name.setText(totaladdedList.get(position).getProduct().toUpperCase());
        holder.quantity.setValue(productQquantity);
        holder.quantity.setMinValue(1);
        holder.quantity.setMaxValue(999);

        holder.quantity.setId(totaladdedList.get(position).getId());


        holder.quantity.setListener(new HorizontalNumberPickerListener() {
            @Override
            public void onHorizontalNumberPickerChanged(HorizontalNumberPicker horizontalNumberPicker, int id, int value) {
                horizontalNumberPickerListener.onHorizontalNumberPickerChanged(holder.quantity, id, value);
            }
        });

        holder.action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((Activity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("TotalArraySize", "" + totaladdedList.size());
                        totaladdedList.remove(position);
                        notifyItemRemoved(position);
                        onItemRemoveListner.onItemRemove(true);
                    }
                });

            }
        });
    }

    @Override
    public int getItemCount() {
        return totaladdedList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView product_name;
        ImageView action;
        HorizontalNumberPicker quantity;

        public MyViewHolder(View itemView) {
            super(itemView);
            product_name = (TextView) itemView.findViewById(R.id.product_name);
            quantity = (HorizontalNumberPicker) itemView.findViewById(R.id.quantity);
            action = (ImageView) itemView.findViewById(R.id.action);
        }
    }
}
