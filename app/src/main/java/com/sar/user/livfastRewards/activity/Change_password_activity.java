package com.sar.user.livfastRewards.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.sar.user.livfastRewards.Interface.RequestListener;
import com.sar.user.livfastRewards.Model.LoginModel;
import com.sar.user.livfastRewards.R;
import com.sar.user.livfastRewards.helper.Controller;
import com.sar.user.livfastRewards.helper.PrefManager;

import org.json.JSONException;

import java.text.ParseException;

public class Change_password_activity extends BaseActivity {
    public Button confirm_button;
    public TextView message, phone_no/*, resendOtp_tv*/;
    public EditText new_password, confirm_password, otp;
    String strlogin, strpassword, strotp, batPassword;
    ProgressBar progressBar;
    PrefManager prefManager;


    Context mContext;
    String forAction;
    RequestListener responseListner = new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @Override
        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
            Log.d("response ChangePassword", responseObject.toString());
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(mContext, "OTP is send successfully", Toast.LENGTH_SHORT).show();

                }
            });

        }

        @Override
        public void onRequestError(int errorCode, String message) {
            handelError(errorCode, message);
        }
    };
    RequestListener passwordListner = new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @SuppressLint("LongLogTag")
        @Override
        public void onRequestCompleted(final Object responseObject) throws JSONException, ParseException {
            Log.d("response passwordListner", responseObject.toString());
            if (responseObject.toString().equals("Invalid Otp !")) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(mContext, "Invalid Otp !", Toast.LENGTH_SHORT).show();
                    }
                });
            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (forAction.equalsIgnoreCase("user_change_password")) {
                            finish();
                        } else {
                            Intent intent = new Intent(mContext, LoginActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    }
                });
            }
        }

        @Override
        public void onRequestError(int errorCode, String message) {
            handelError(errorCode, message);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password_activity);

        mContext = this;
        initControl();
        prefManager = PrefManager.getInstance(mContext);

        phone_no.setText(prefManager.getPhoneNo());

        if (getIntent().getStringExtra("for_action") != null && getIntent().getExtras().containsKey("for_action")) {
            forAction = getIntent().getStringExtra("for_action");

            if (forAction.equalsIgnoreCase("user_change_password")) {
                LoginModel loginModel = new LoginModel();
                loginModel.setBat_code(prefManager.getBatCode());
                loginModel.setJob_Type("Forgot");

                Controller.manualLogin(mContext, loginModel, responseListner);
                message.setText("Set New Password");
            } else {
                message.setText("Set New Password");
            }

        } else {
            finish();
        }


        /*SpannableString content = new SpannableString("RE-Send OTP");
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        resendOtp_tv.setText(content);
        resendOtp_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LoginModel loginModel = new LoginModel();
                loginModel.setBat_code(prefManager.getBatCode());

                progressBar.setVisibility(View.VISIBLE);
                Controller.manualLogin(mContext, loginModel, responseListner);

            }
        });*/

        confirm_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strlogin = new_password.getText().toString();
                strpassword = confirm_password.getText().toString();
                strotp = otp.getText().toString();
                if (strlogin.equals("")) {
                    new_password.setError("Enter password");
                } else if (strpassword.equals("")) {
                    confirm_password.setError("Re-Enter the password");
                } else if (strotp.equals("")) {
                    otp.setError("Enter OTP");
                } else if (!strlogin.equals(strpassword)) {
                    Toast.makeText(mContext, "Both password is not same", Toast.LENGTH_SHORT).show();
                } else {
                    batPassword = strpassword;
                    LoginModel loginModel = new LoginModel();
                    loginModel.setBat_code(prefManager.getBatCode());

                    if (forAction.equalsIgnoreCase("user_first_time")) {
                        loginModel.setJob_Type("RegistrationInsert");
                    } else {
                        loginModel.setJob_Type("ForgotInsert");
                    }

                    loginModel.setPassword(strpassword);
                    loginModel.setVerification_otp(strotp);

                    progressBar.setVisibility(View.VISIBLE);
                    Controller.manualLogin(mContext, loginModel, passwordListner);
                }
            }
        });
    }

    private void initControl() {
        message = (TextView) findViewById(R.id.message_logo);
        phone_no = (TextView) findViewById(R.id.phone_no);
        confirm_button = (Button) findViewById(R.id.confirm_button);
        otp = (EditText) findViewById(R.id.otp_text);
        /*resendOtp_tv = (TextView) findViewById(R.id.resendOtp_tv);*/
        new_password = (EditText) findViewById(R.id.passwordone);
        confirm_password = (EditText) findViewById(R.id.passwordtwo);
        progressBar = (ProgressBar) findViewById(R.id.progressbar);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (forAction.equalsIgnoreCase("user_change_password")) {
            finish();
        } else {
            Intent intent = new Intent(mContext, LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }
}
