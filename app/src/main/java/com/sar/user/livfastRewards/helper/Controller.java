package com.sar.user.livfastRewards.helper;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.Volley;
import com.sar.user.livfastRewards.Interface.RequestListener;
import com.sar.user.livfastRewards.Interface.VolleyErrorListener;
import com.sar.user.livfastRewards.Model.AddProductModel;
import com.sar.user.livfastRewards.Model.AddServiceRequestModel;
import com.sar.user.livfastRewards.Model.AddWarrantyModel;
import com.sar.user.livfastRewards.Model.CalculateLoyaltyPointsModel;
import com.sar.user.livfastRewards.Model.EditProfileModel;
import com.sar.user.livfastRewards.Model.FileUploadModel;
import com.sar.user.livfastRewards.Model.FinalPointsRedeemModel;
import com.sar.user.livfastRewards.Model.GetCouponAPIModel;
import com.sar.user.livfastRewards.Model.GetRedeemHistoryAPIModel;
import com.sar.user.livfastRewards.Model.GetViewFlag;
import com.sar.user.livfastRewards.Model.HomeCountModel;
import com.sar.user.livfastRewards.Model.LoginModel;
import com.sar.user.livfastRewards.Model.LoyalityModel;
import com.sar.user.livfastRewards.Model.PInCodeModel;
import com.sar.user.livfastRewards.Model.PointsModel;
import com.sar.user.livfastRewards.Model.PutFeedbackModel;
import com.sar.user.livfastRewards.Model.RedeemSlabModel;
import com.sar.user.livfastRewards.Model.ScratchStatusAPIModel;
import com.sar.user.livfastRewards.Model.SelectRedeemModel;
import com.sar.user.livfastRewards.Model.SerialToUniqueModel;
import com.sar.user.livfastRewards.Model.WarrantyLoyalityModel;

import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.json.JSONException;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by user on 30-03-2017.
 */

public class Controller {
    private static final RetryPolicy DefaultRetryPolicy = new RetryPolicy() {
        @Override
        public void retry(VolleyError error) throws VolleyError {
            if (error.networkResponse.statusCode == 403 || error.networkResponse.statusCode == 401)
                throw new VolleyError(error);
        }

        @Override
        public int getCurrentTimeout() {
            return 0;
        }

        @Override
        public int getCurrentRetryCount() {
            return 1;
        }
    };
    public static RequestQueue mRequestQueue;
    static File mImageFile;
    static MultipartEntityBuilder mBuilder = MultipartEntityBuilder.create();
    private static Context mContext;
    private static NetworkResponse NoInternetResponse = new NetworkResponse(
            ERROR_CODES.NO_INTERNET, "No Internet Connection".getBytes(), null,
            false);

    public static final void init(Context context) {
        mContext = context;
        mRequestQueue = Volley.newRequestQueue(context);
    }

    public static Request<String> simplevolleyrequest(Context thecontext, int methodrequest, String url, Object params, final RequestListener oblistener) {
        Context context = thecontext;

        Log.d("MyTAG", JsonUtils.jsonify(params).toString());

        final Request<String> request = new JsonRequest<String>(methodrequest, url, JsonUtils.jsonify(params), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
            }
        }, new VolleyErrorListener(context, url, oblistener)) {
            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                Response<String> theResponse;
                if (response.statusCode >= 200) {
                    String responseBody = new String(response.data);
                    if (oblistener != null)
                        try {
                            oblistener.onRequestCompleted(responseBody);
                        } catch (JSONException e) {
                            Log.d("MyTAG", "controller error" + e.toString());
                            e.printStackTrace();
                        } catch (ParseException e) {
                            e.printStackTrace();
                            Log.d("MyTAG", "controller error" + e.toString());
                        }
                    theResponse = Response.success(responseBody,
                            HttpHeaderParser.parseCacheHeaders(response));
                } else {
                    parseNetworkError(new VolleyError(response));
                    theResponse = Response.error(new VolleyError(response.toString()));
                }
                return theResponse;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                if (GetAuth() != null) {
                    String key = GetAuth();
                    headers.put("Authorization", "Token " + key);
                }
                return headers;
            }
        };
        return request;
    }

    public static Request<String> multipartvolleyrequest(Context con, int methodrequest, String url, final Object newRequest, final RequestListener mListener) {
        Context context = con;
        //Log.d(Config.Tag,url);
        buildMultipartEntity();
        Request<String> tempRequest = new JsonRequest<String>(
                methodrequest, url, null,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                    }
                }, new VolleyErrorListener(context, url, mListener)) {
            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                Response<String> mResponse;
                if (response.statusCode >= 200 && response.statusCode <= 300) {
                    String responseBody = new String(response.data);
                    if (mListener != null)
                        try {
                            mListener.onRequestCompleted(responseBody);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    mResponse = Response.success(responseBody,
                            HttpHeaderParser.parseCacheHeaders(response));
                } else if (response.statusCode >= 400 && response.statusCode <= 500) {
                    parseNetworkError(new VolleyError(response));

                    mResponse = Response.error(new VolleyError(response));
                } else {
                    parseNetworkError(new VolleyError(response));
                    mResponse = Response.error(new VolleyError(response.toString()));
                }
                return mResponse;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                String key = GetAuth();
                headers.put("Authorization", "Token " + key);
                Log.d("key", "Token " + key);
                return headers;
            }

            @Override
            public byte[] getBody() {
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                try {
                    mBuilder.build().writeTo(bos);
                } catch (IOException e) {
                    VolleyLog.e("IOException writing to ByteArrayOutputStream bos, building the multipart request.");
                }

                return bos.toByteArray();
            }

            @Override
            public String getBodyContentType() {
                String contentTypeHeader = mBuilder.build().getContentType().getValue();
                return contentTypeHeader;
            }
        };
        return tempRequest;
    }

    public static Request<String> multiPartRequest(Context con, int methodrequest, String url, MultipartEntityBuilder multipartEntityBuilder, final RequestListener mListener) {
        Context context = con;
        //Log.d(Config.Tag,url);

        Request<String> tempRequest = new JsonRequest<String>(
                methodrequest, url, null,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                    }
                }, new VolleyErrorListener(context, url, mListener)) {
            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                Response<String> mResponse;
                if (response.statusCode >= 200 && response.statusCode <= 300) {
                    String responseBody = new String(response.data);
                    if (mListener != null)
                        try {
                            mListener.onRequestCompleted(responseBody);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    mResponse = Response.success(responseBody,
                            HttpHeaderParser.parseCacheHeaders(response));
                } else if (response.statusCode >= 400 && response.statusCode <= 500) {
                    parseNetworkError(new VolleyError(response));

                    mResponse = Response.error(new VolleyError(response));
                } else {
                    parseNetworkError(new VolleyError(response));
                    mResponse = Response.error(new VolleyError(response.toString()));
                }
                return mResponse;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                String key = GetAuth();
                headers.put("Authorization", "Token " + key);
                Log.d("key", "Token " + key);
                return headers;
            }

            @Override
            public byte[] getBody() {
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                try {
                    mBuilder.build().writeTo(bos);
                } catch (IOException e) {
                    VolleyLog.e(e.toString());
                }

                return bos.toByteArray();
            }

            @Override
            public String getBodyContentType() {
                String contentTypeHeader = mBuilder.build().getContentType().getValue();
                return contentTypeHeader;
            }
        };
        return tempRequest;
    }

    private static String GetAuth() {
        PrefManager pref = PrefManager.getInstance(mContext);
        if (pref.getToken() != null) {
            return pref.getToken();
        } else
            return null;
    }

    private static void dispatchToQueue(Request<String> request,
                                        Context mContext) {
        if (!NetworkChecker.isNetworkConnected(mContext)) {
            VolleyError error = new VolleyError(NoInternetResponse);
            if (request.getMethod() != Request.Method.GET) {
                request.deliverError(error);
            } else {
                request.setRetryPolicy(DefaultRetryPolicy);
                mRequestQueue.add(request);
            }
        } else {
            request.setRetryPolicy(DefaultRetryPolicy);
            mRequestQueue.add(request);
        }
    }

    public static void manualLogin(Context con, LoginModel loginModel, RequestListener requestListener) {
        String url = UrlResolver.path1(UrlResolver.GetUrl.Login);
        Log.d("url manualLogin", url);
        Request<String> volleyRequest = simplevolleyrequest(
                con, Request.Method.POST, url, loginModel, requestListener);
        volleyRequest.setShouldCache(false);
        dispatchToQueue(volleyRequest, con);
    }

    public static void SerialToUnique(Context con, SerialToUniqueModel serial_no, RequestListener requestListener) {
        String url = UrlResolver.path(UrlResolver.GetUrl.SerialToUnique);
        Log.d("url", url);
        Request<String> volleyRequest = simplevolleyrequest(
                con, Request.Method.POST, url, serial_no, requestListener);
        volleyRequest.setShouldCache(false);
        dispatchToQueue(volleyRequest, con);
    }

//    public static void setPassword(Context con, LoginModel loginModel, RequestListener requestListener) {
//        String url = UrlResolver.path(UrlResolver.GetUrl.SetPassword);
//        Log.d("url", url);
//        Request<String> volleyRequest = simplevolleyrequest(
//                con, Request.Method.POST, url, loginModel, requestListener);
//        volleyRequest.setShouldCache(false);
//        dispatchToQueue(volleyRequest, con);
//    }
//
//    public static void BatCode(Context con, LoginModel loginModel, RequestListener requestListener) {
//        String url = UrlResolver.path(UrlResolver.GetUrl.ForgotPassword);
//        Log.d("url", url);
//        Request<String> volleyRequest = simplevolleyrequest(
//                con, Request.Method.POST, url, loginModel, requestListener);
//        volleyRequest.setShouldCache(false);
//        dispatchToQueue(volleyRequest, con);
//    }

    public static void GetNotification(Context con, RequestListener requestListener) {
        String url = UrlResolver.path(UrlResolver.GetUrl.Notification);
        Log.d("url", url);
        Request<String> volleyreq = simplevolleyrequest(
                con, Request.Method.GET, url, null, requestListener);
        volleyreq.setShouldCache(false);
        dispatchToQueue(volleyreq, con);
    }

    //    public static void GetProductDetail(Context con, String start_date, String end_date,String user_id, RequestListener requestListener) {
//        String url = UrlResolver.path(UrlResolver.GetUrl.Product);
//        url += "?start_date=" + start_date + "&end_date=" + end_date+"&user_id="+user_id;;
//        Log.d("url", url);
//        Request<String> volleyRequest = simplevolleyrequest(
//                con, Request.Method.GET, url, null, requestListener);
//        volleyRequest.setShouldCache(false);
//        dispatchToQueue(volleyRequest, con);
//    }
    //CHANGE BY MOHIT
    public static void GetProductDetail(Context con, String start_date, String end_date, String user_id, RequestListener requestListener) {
        String url = UrlResolver.path1(UrlResolver.GetUrl.GetPurchaseLog);
        url += "?start_date=" + start_date + "&end_date=" + end_date + "&user_id=" + user_id;
        ;
        Log.d("url GetProductDetail", url);
        Request<String> volleyRequest = simplevolleyrequest(
                con, Request.Method.GET, url, null, requestListener);
        volleyRequest.setShouldCache(false);
        dispatchToQueue(volleyRequest, con);
    }

    public static void GetProductLoyality(Context con, RequestListener requestListener) {
        String url = UrlResolver.path1(UrlResolver.GetUrl.Target_Detail);
        Log.d("url", url);
        Request<String> volleyRequest = simplevolleyrequest(
                con, Request.Method.GET, url, null, requestListener);
        volleyRequest.setShouldCache(false);
        dispatchToQueue(volleyRequest, con);
    }

//    public static void GetTargets(Context con, RequestListener requestListener) {
//        String url = UrlResolver.path(UrlResolver.GetUrl.Targets);
//        Log.d("url", url);
//        Request<String> volleyreq = simplevolleyrequest(
//                con, Request.Method.GET, url, null, requestListener);
//        volleyreq.setShouldCache(false);
//        dispatchToQueue(volleyreq, con);
//    }

    public static void GetBanner(Context con, RequestListener requestListener) {
        String url = UrlResolver.path1(UrlResolver.GetUrl.Banner);
        Log.d("url GetBanner", url);
        Request<String> volleyRequest = simplevolleyrequest(
                con, Request.Method.GET, url, null, requestListener);
        volleyRequest.setShouldCache(false);
        dispatchToQueue(volleyRequest, con);
    }

    public static void GetSegment(Context con, RequestListener requestListener) {
        String url = UrlResolver.path(UrlResolver.GetUrl.Segment);
        Log.d("url", url);
        Request<String> volleyRequest = simplevolleyrequest(
                con, Request.Method.GET, url, null, requestListener);
        volleyRequest.setShouldCache(false);
        dispatchToQueue(volleyRequest, con);
    }

    public static void GetRedeemHistory(Context con, GetRedeemHistoryAPIModel getRedeemHistoryAPIModel, RequestListener requestListener) {
        String url = UrlResolver.API_GET_REDEEM_HISTORY;
        Log.d("url GetRedeemHistory", url);
        Request<String> volleyreq = simplevolleyrequest(
                con, Request.Method.POST, url, getRedeemHistoryAPIModel, requestListener);
        volleyreq.setShouldCache(false);
        dispatchToQueue(volleyreq, con);
    }

    public static void getCouponAPI(Context con, GetCouponAPIModel getCouponAPIModel, RequestListener requestListener) {
        String url = UrlResolver.API_GET_COUPON;
        Log.d("url getCouponAPI", url);
        Request<String> volleyreq = simplevolleyrequest(
                con, Request.Method.POST, url, getCouponAPIModel, requestListener);
        volleyreq.setShouldCache(false);
        dispatchToQueue(volleyreq, con);
    }

    @SuppressLint("LongLogTag")
    public static void setCouponScratchStatus(Context con, ScratchStatusAPIModel scratchStatusAPIModel, RequestListener requestListener) {
        String url = UrlResolver.API_SCRATCH_STATUS;
        Log.d("url setCouponScratchStatus", url);
        Request<String> volleyreq = simplevolleyrequest(
                con, Request.Method.POST, url, scratchStatusAPIModel, requestListener);
        volleyreq.setShouldCache(false);
        dispatchToQueue(volleyreq, con);
    }

    @SuppressLint("LongLogTag")
    public static void checkCouponScratchPoints(Context con, GetCouponAPIModel getCouponAPIModel, RequestListener requestListener) {
        String url = UrlResolver.API_SCRATCH_POINTS_CHECK;
        Log.d("url checkCouponScratchPoints", url);
        Request<String> volleyreq = simplevolleyrequest(
                con, Request.Method.POST, url, getCouponAPIModel, requestListener);
        volleyreq.setShouldCache(false);
        dispatchToQueue(volleyreq, con);
    }

    public static void TotalPointsRedeem(Context con, String user_id, FinalPointsRedeemModel finalPointsRedeemModel, RequestListener requestListener) {
        String url = UrlResolver.path(UrlResolver.GetUrl.GetRedeemHistory);
        url += user_id + "/";
        Log.d("url TotalPointsRedeem", url);
        Request<String> volleyreq = simplevolleyrequest(
                con, Request.Method.PUT, url, finalPointsRedeemModel, requestListener);
        volleyreq.setShouldCache(false);
        dispatchToQueue(volleyreq, con);
    }

    @SuppressLint("LongLogTag")
    public static void APIRedeemConfirmationPoints(Context con, FinalPointsRedeemModel finalPointsRedeemModel, RequestListener requestListener) {
        String url = UrlResolver.API_OTP_CONFIRMATION;
        Log.d("url APIRedeemConfirmationPoints", url);
        Request<String> volleyreq = simplevolleyrequest(
                con, Request.Method.POST, url, finalPointsRedeemModel, requestListener);
        volleyreq.setShouldCache(false);
        dispatchToQueue(volleyreq, con);
    }

    public static void GetCities(Context con, String letter, RequestListener requestListener) {
        String url = UrlResolver.path(UrlResolver.GetUrl.GetCitites);
        url += "?city=" + letter;
        Log.d("url GetCities", url);
        Request<String> volleyRequest = simplevolleyrequest(
                con, Request.Method.GET, url, null, requestListener);
        volleyRequest.setShouldCache(false);
        dispatchToQueue(volleyRequest, con);
    }

    public static void GetManufacturer(Context con, int segment_id, RequestListener requestListener) {
        String url = UrlResolver.path(UrlResolver.GetUrl.Manufacturer);
        url += "?car_segment=" + segment_id;
        Log.d("url", url);
        Request<String> volleyRequest = simplevolleyrequest(
                con, Request.Method.GET, url, null, requestListener);
        volleyRequest.setShouldCache(false);
        dispatchToQueue(volleyRequest, con);
    }

    public static void GetCarModel(Context con, int segment_id, int manufacture_id, RequestListener requestListener) {
        String url = UrlResolver.path(UrlResolver.GetUrl.CarModel);
        url += "?car_segment=" + segment_id + "&vehicle_manufacturer=" + manufacture_id;
        Log.d("url", url);
        Request<String> volleyreq = simplevolleyrequest(
                con, Request.Method.GET, url, null, requestListener);
        volleyreq.setShouldCache(false);
        dispatchToQueue(volleyreq, con);
    }

    //    public static void GetStock(Context con, RequestListener requestListener) {
//        String url = UrlResolver.path(UrlResolver.GetUrl.GetStock);
//        Log.d("url", url);
//        Request<String> volleyRequest = simplevolleyrequest(
//                con, Request.Method.GET, url, null, requestListener);
//        volleyRequest.setShouldCache(false);
//        dispatchToQueue(volleyRequest, con);
//    }
    //CHANGE BY MOHIT
    public static void GetStock(Context con, String user_id, RequestListener requestListener) {
        String url = UrlResolver.path1(UrlResolver.GetUrl.GetStock);
        url += "?user_id=" + user_id;
        Log.d("url GetStock", url);
        Request<String> volleyRequest = simplevolleyrequest(
                con, Request.Method.GET, url, null, requestListener);
        volleyRequest.setShouldCache(false);
        dispatchToQueue(volleyRequest, con);
    }

    //CHANGE BY MOHIT
    public static void GetRedeemSlab(Context con, RequestListener requestListener) {
        String url = UrlResolver.path1(UrlResolver.GetUrl.GetRedeemSlab);
        Log.d("url GetRedeemSlab", url);
        Request<String> volleyRequest = simplevolleyrequest(
                con, Request.Method.GET, url, null, requestListener);
        volleyRequest.setShouldCache(false);
        dispatchToQueue(volleyRequest, con);
    }


//    public static void GetRedeemSlab(Context con, RequestListener requestListener) {
//        String url = UrlResolver.path(UrlResolver.GetUrl.GetRedeemSlab);
//        Log.d("url", url);
//        Request<String> volleyRequest = simplevolleyrequest(
//                con, Request.Method.GET, url, null, requestListener);
//        volleyRequest.setShouldCache(false);
//        dispatchToQueue(volleyRequest, con);
//    }

    public static void GetSecRedeemSlab(Context con, RedeemSlabModel redeemSlabModel, RequestListener requestListener) {
        String url = UrlResolver.path1(UrlResolver.GetUrl.GetSecSlab);
        Log.d("url GetSecRedeemSlab", url);
        Request<String> volleyRequest = simplevolleyrequest(
                con, Request.Method.POST, url, redeemSlabModel, requestListener);
        volleyRequest.setShouldCache(false);
        dispatchToQueue(volleyRequest, con);
    }

    public static void Get4wSlab(Context con, RequestListener requestListener) {
        String url = UrlResolver.path1(UrlResolver.GetUrl.Get4wslab);
        Log.d("url Get4wSlab", url);
        Request<String> volleyRequest = simplevolleyrequest(
                con, Request.Method.GET, url, null, requestListener);
        volleyRequest.setShouldCache(false);
        dispatchToQueue(volleyRequest, con);
    }

    public static void GeterickSlab(Context con, RequestListener requestListener) {
        String url = UrlResolver.path1(UrlResolver.GetUrl.GetErickSlab);
        Log.d("url GeterickSlab", url);
        Request<String> volleyRequest = simplevolleyrequest(
                con, Request.Method.GET, url, null, requestListener);
        volleyRequest.setShouldCache(false);
        dispatchToQueue(volleyRequest, con);
    }

    public static void GeterickSlabPR(Context con, RequestListener requestListener) {
        String url = UrlResolver.path1(UrlResolver.GetUrl.GetErickSlabPR);
        Log.d("url GeterickSlabPR", url);
        Request<String> volleyRequest = simplevolleyrequest(
                con, Request.Method.GET, url, null, requestListener);
        volleyRequest.setShouldCache(false);
        dispatchToQueue(volleyRequest, con);
    }

    public static void getwarrantySlab(Context con, RequestListener requestListener) {
        String url = UrlResolver.path1(UrlResolver.GetUrl.Warranty_Redeem);
        Log.d("url getwarrantySlab", url);
        Request<String> volleyRequest = simplevolleyrequest(
                con, Request.Method.GET, url, null, requestListener);
        volleyRequest.setShouldCache(false);
        dispatchToQueue(volleyRequest, con);
    }

    public static void AddProductDetail(Context con, AddProductModel addProductModel, RequestListener requestListener) {
        String url = UrlResolver.path1(UrlResolver.GetUrl.Product);
        Log.d("url AddProductDetail", url);
        Request<String> volleyRequest = simplevolleyrequest(
                con, Request.Method.POST, url, addProductModel, requestListener);
        volleyRequest.setShouldCache(false);
        dispatchToQueue(volleyRequest, con);
    }

    public static void apiRedeemPoints(Context con, SelectRedeemModel selectRedeemModel, RequestListener requestListener) {
        String url = UrlResolver.API_REDEEM_POINTS;
        Log.d("url apiRedeemPoints", url);
        Request<String> volleyRequest = simplevolleyrequest(
                con, Request.Method.POST, url, selectRedeemModel, requestListener);
        volleyRequest.setShouldCache(false);
        dispatchToQueue(volleyRequest, con);
    }

    public static void AddserviceRequest(Context con, AddServiceRequestModel addServiceRequestModel, RequestListener requestListener) {
        String url = UrlResolver.path(UrlResolver.GetUrl.DealerService);
        Log.d("url AddserviceRequest", url);
        Request<String> volleyRequest = simplevolleyrequest(
                con, Request.Method.POST, url, addServiceRequestModel, requestListener);
        volleyRequest.setShouldCache(false);
        dispatchToQueue(volleyRequest, con);
    }

    public static void CalculateLoyalty(Context con, CalculateLoyaltyPointsModel calculateLoyaltyPointsModel, RequestListener requestListener) {
        String url = UrlResolver.path1(UrlResolver.GetUrl.CalculateLoyaly);
        Log.d("url CalculateLoyalty", url);
        Request<String> volleyRequest = simplevolleyrequest(
                con, Request.Method.POST, url, calculateLoyaltyPointsModel, requestListener);
        volleyRequest.setShouldCache(false);
        dispatchToQueue(volleyRequest, con);
    }

    public static void AddServiceFeedback(Context con, int id, PutFeedbackModel putFeedbackModel, RequestListener requestListener) {
        String url = UrlResolver.path(UrlResolver.GetUrl.DealerService);
        url += id + "/";
        Log.d("url AddServiceFeedback", url);
        Request<String> volleyRequest = simplevolleyrequest(
                con, Request.Method.PUT, url, putFeedbackModel, requestListener);
        volleyRequest.setShouldCache(false);
        dispatchToQueue(volleyRequest, con);
    }

    public static void GetWarranty(Context con, String start_date, String end_date, String user_id, RequestListener requestListener) {
        String url = UrlResolver.path1(UrlResolver.GetUrl.GETWARRANTY);
        url += "?start_date=" + start_date + "&end_date=" + end_date + "&user_id=" + user_id;
        Log.d("url GetWarranty", url);
        Request<String> volleyRequest = simplevolleyrequest(
                con, Request.Method.GET, url, null, requestListener);
        volleyRequest.setShouldCache(false);
        dispatchToQueue(volleyRequest, con);
    }

    public static void GetServiceRequest(Context con, String start_date, String end_date, RequestListener requestListener) {
        String url = UrlResolver.path(UrlResolver.GetUrl.DealerService);
        url += "?start_date=" + start_date + "&end_date=" + end_date;
        Log.d("url GetServiceRequest", url);
        Request<String> volleyRequest = simplevolleyrequest(
                con, Request.Method.GET, url, null, requestListener);
        volleyRequest.setShouldCache(false);
        dispatchToQueue(volleyRequest, con);
    }

    public static void warranty(Context con, AddWarrantyModel addWarrantyModel, RequestListener requestListener) {
        String url = UrlResolver.path1(UrlResolver.GetUrl.Warranty);
        Log.d("url warranty", url);
        Request<String> volleyRequest = simplevolleyrequest(
                con, Request.Method.POST, url, addWarrantyModel, requestListener);
        volleyRequest.setShouldCache(false);
        dispatchToQueue(volleyRequest, con);
    }

    public static void profileUpload(Context con, FileUploadModel fileuploadmodel, RequestListener requestListener) {
        String url = UrlResolver.path1(UrlResolver.GetUrl.Edit_Profile_Pic);
        Log.d("url", url);
        Request<String> volleyRequest = simplevolleyrequest(
                con, Request.Method.POST, url, fileuploadmodel, requestListener);
        volleyRequest.setShouldCache(false);
        dispatchToQueue(volleyRequest, con);
    }

    public static void getViewFlagAndData(Context con, GetViewFlag getViewFlag, RequestListener requestListener) {
        String url = UrlResolver.path1(UrlResolver.GetUrl.DEALERSTATECITY);
        Log.d("url", url);
        Request<String> volleyRequest = simplevolleyrequest(
                con, Request.Method.POST, url, getViewFlag, requestListener);
        volleyRequest.setShouldCache(false);
        dispatchToQueue(volleyRequest, con);

    }

//    public static void AddWarrantyWithImage(Context con,  AddWarrantyModel addWarrantyModel, RequestListener requestListener) {
//        String url = UrlResolver.path1(UrlResolver.GetUrl.Warranty);
//        Log.d("url", url);
////        Log.d("url", url);
////        Request<String> volleyRequest = simplevolleyrequest(
////                con, Request.Method.GET, url, null, requestListener);
////        volleyRequest.setShouldCache(false);
////        dispatchToQueue(volleyRequest, con);
////        File mImageFile = fileUploadModel.getFile();
////        final MultipartEntityBuilder mBuilder = MultipartEntityBuilder.create();
////
////        try {
////            mBuilder.addPart("unique_code", new StringBody(addWarrantyModel.getUnique_code()));
////            mBuilder.addPart("serial_number", new StringBody(addWarrantyModel.getSerial_number()));
////            mBuilder.addPart("comment ", new StringBody(addWarrantyModel.getComment()));
////            mBuilder.addPart("customer_name", new StringBody(addWarrantyModel.getCustomer_name()));
////            mBuilder.addPart("customer_name", new StringBody(addWarrantyModel.getCustomer_name()));
////            mBuilder.addPart("customer_phone", new StringBody(addWarrantyModel.getCustomer_phone()));
////            mBuilder.addPart("vehicle_number", new StringBody(addWarrantyModel.getVehicle_number()));
////            mBuilder.addPart("car_segment", new StringBody(String.valueOf(addWarrantyModel.getCar_segment())));
////            mBuilder.addPart("vehicle_model", new StringBody(String.valueOf(addWarrantyModel.getVehicle_model())));
////            mBuilder.addPart("vehicle_manufacturer", new StringBody(String.valueOf(addWarrantyModel.getVehicle_manufacturer())));
////           mBuilder.addPart("sell_date", new StringBody(String.valueOf(addWarrantyModel.getSell_date())));
////
////        } catch (UnsupportedEncodingException e) {
////            VolleyLog.e("UnsupportedEncodingException");
////        }
////        mBuilder.addPart("file", new FileBody(mImageFile));
////        mBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
////        mBuilder.setLaxMode().setBoundary("xx").setCharset(Charset.forName("UTF-8"));
//
//        Request<String> volleyReq = multiPartRequest(con, Request.Method.POST, url, mBuilder, requestListener);
//        volleyReq.setShouldCache(false);
//        dispatchToQueue(volleyReq, con);
//    }

//    public static void AddWarrantyWithoutImage(Context con, AddWarrantyModel addWarrantyModel, RequestListener requestListener) {
//        String url = UrlResolver.path(UrlResolver.GetUrl.Warranty);
//        Log.d("url", url);
//
////        final MultipartEntityBuilder mBuilder = MultipartEntityBuilder.create();
////
////        try {
////            mBuilder.addPart("unique_code", new StringBody(addWarrantyModel.getUnique_code()));
////            mBuilder.addPart("serial_number", new StringBody(addWarrantyModel.getSerial_number()));
////           mBuilder.addPart("comment ", new StringBody(addWarrantyModel.getComment()));
////            mBuilder.addPart("customer_name", new StringBody(addWarrantyModel.getCustomer_name()));
////            mBuilder.addPart("customer_name", new StringBody(addWarrantyModel.getCustomer_name()));
////            mBuilder.addPart("customer_phone", new StringBody(addWarrantyModel.getCustomer_phone()));
////          mBuilder.addPart("vehicle_number", new StringBody(addWarrantyModel.getVehicle_number()));
////           mBuilder.addPart("car_segment", new StringBody(addWarrantyModel.getCar_segment()));
////           mBuilder.addPart("vehicle_model", new StringBody(addWarrantyModel.getVehicle_model()));
////            mBuilder.addPart("vehicle_manufacturer", new StringBody(addWarrantyModel.getVehicle_manufacturer()));
////              mBuilder.addPart("sell_date", new StringBody(String.valueOf(addWarrantyModel.getSell_date())));
////
////        } catch (UnsupportedEncodingException e) {
////            VolleyLog.e("UnsupportedEncodingException");
////        }
////        mBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
////        mBuilder.setLaxMode().setBoundary("xx").setCharset(Charset.forName("UTF-8"));
//
//        Request<String> volleyReq = multiPartRequest(con, Request.Method.GET, url, mBuilder, requestListener);
//        volleyReq.setShouldCache(false);
//        dispatchToQueue(volleyReq, con);
//    }

//    public static void profileUpload(Context con, FileUploadModel fileuploadmodel, RequestListener requestListener) {
//        String url = UrlResolver.path(UrlResolver.GetUrl.Edit_Profile_Pic);
//        Log.d("url", url);
//        mImageFile = fileuploadmodel.getFile();
//        Request<String> volleyRequest = multipartvolleyrequest(
//                con, Request.Method.PUT, url, null, requestListener);
//        volleyRequest.setShouldCache(false);
//        dispatchToQueue(volleyRequest, con);
//    }

    //    public static void profileDetailUpadate(Context con, EditProfileModel editProfileModel, RequestListener requestListener) {
//        String url = UrlResolver.path(UrlResolver.GetUrl.Profile_Update);
//        Log.d("url", url);
//        Request<String> volleyRequest = simplevolleyrequest(
//                con, Request.Method.PUT, url, editProfileModel, requestListener);
//        volleyRequest.setShouldCache(false);
//        dispatchToQueue(volleyRequest, con);
//    }
    //CHANGE BY MOHIT
    public static void profileDetailUpadate(Context con, EditProfileModel editProfileModel, RequestListener requestListener) {
        String url = UrlResolver.path1(UrlResolver.GetUrl.Profile_Update);
        Log.d("url", url);
        Request<String> volleyRequest = simplevolleyrequest(
                con, Request.Method.POST, url, editProfileModel, requestListener);
        volleyRequest.setShouldCache(false);
        dispatchToQueue(volleyRequest, con);
    }

    public static void getloyality(Context con, LoyalityModel loyalityModel, RequestListener requestListener) {
        String url = UrlResolver.path1(UrlResolver.GetUrl.Loyality);
        Log.d("url getloyality", url);
        Request<String> volleyRequest = simplevolleyrequest(
                con, Request.Method.POST, url, loyalityModel, requestListener);
        volleyRequest.setShouldCache(false);
        dispatchToQueue(volleyRequest, con);
    }

    public static void getloyalitytert(Context con, PointsModel pointsModel, RequestListener requestListener) {
        String url = UrlResolver.path1(UrlResolver.GetUrl.TERT);
        Log.d("url getloyality", url);
        Request<String> volleyRequest = simplevolleyrequest(
                con, Request.Method.POST, url, pointsModel, requestListener);
        volleyRequest.setShouldCache(false);
        dispatchToQueue(volleyRequest, con);
    }

    public static void getWarrantyLoyality(Context con, WarrantyLoyalityModel warrantyLoyalityModel, RequestListener requestListener) {
        String url = UrlResolver.path1(UrlResolver.GetUrl.Warranty_Points);
        Log.d("url", url);
        Request<String> volleyRequest = simplevolleyrequest(
                con, Request.Method.POST, url, warrantyLoyalityModel, requestListener);
        volleyRequest.setShouldCache(false);
        dispatchToQueue(volleyRequest, con);
    }

    public static void getScheme(Context con, RequestListener requestListener) {
        String url = UrlResolver.path1(UrlResolver.GetUrl.SCHEMEURL);
        Log.d("url getScheme", url);
        Request<String> volleyreq = simplevolleyrequest(
                con, Request.Method.GET, url, null, requestListener);
        volleyreq.setShouldCache(false);
        dispatchToQueue(volleyreq, con);
    }

    public static void getScheme1(Context con, RequestListener requestListener) {
        String url = UrlResolver.path1(UrlResolver.GetUrl.SCHEMEURL1);
        Log.d("url getScheme1", url);
        Request<String> volleyreq = simplevolleyrequest(
                con, Request.Method.GET, url, null, requestListener);
        volleyreq.setShouldCache(false);
        dispatchToQueue(volleyreq, con);
    }

    public static void getScheme2(Context con, RequestListener requestListener) {
        String url = UrlResolver.path1(UrlResolver.GetUrl.SCHEMEURL2);
        Log.d("url getScheme2", url);
        Request<String> volleyreq = simplevolleyrequest(
                con, Request.Method.GET, url, null, requestListener);
        volleyreq.setShouldCache(false);
        dispatchToQueue(volleyreq, con);
    }

    public static void getScheme3(Context con, RequestListener requestListener) {
        String url = UrlResolver.path1(UrlResolver.GetUrl.SCHEMEURL3);
        Log.d("url getScheme3", url);
        Request<String> volleyreq = simplevolleyrequest(
                con, Request.Method.GET, url, null, requestListener);
        volleyreq.setShouldCache(false);
        dispatchToQueue(volleyreq, con);
    }

    public static void getScheme5(Context con, RequestListener requestListener) {
        String url = UrlResolver.path1(UrlResolver.GetUrl.SCHEMEURL5);
        Log.d("url getScheme5", url);
        Request<String> volleyreq = simplevolleyrequest(
                con, Request.Method.GET, url, null, requestListener);
        volleyreq.setShouldCache(false);
        dispatchToQueue(volleyreq, con);
    }

    public static void getStateCity(Context con, PInCodeModel pInCodeModel, RequestListener requestListener) {
        String url = UrlResolver.path2(UrlResolver.GetUrl.BATTERYTYPE);
        Log.d("url getStateCity", url);
        Request<String> volleyRequest = simplevolleyrequest(
                con, Request.Method.POST, url, pInCodeModel, requestListener);
        volleyRequest.setShouldCache(false);
        dispatchToQueue(volleyRequest, con);
    }

    public static void getPoints(Context con, PointsModel pointsModel, RequestListener requestListener) {
        String url = UrlResolver.path1(UrlResolver.GetUrl.POINTS);
        Log.d("url getPoints", url);
        Request<String> volleyRequest = simplevolleyrequest(
                con, Request.Method.POST, url, pointsModel, requestListener);
        volleyRequest.setShouldCache(false);
        dispatchToQueue(volleyRequest, con);
    }
    @SuppressLint("LongLogTag")
    public static void gettotalloyalty(Context con, LoyalityModel loyalityModel, RequestListener requestListener) {
        String url = UrlResolver.API_NEWSCHME_POINTS;
        Log.d("url checkCouponScratchPoints", url);
        Request<String> volleyreq = simplevolleyrequest(
                con, Request.Method.POST, url, loyalityModel, requestListener);
        volleyreq.setShouldCache(false);
        dispatchToQueue(volleyreq, con);
    }
    @SuppressLint("LongLogTag")
    public static void getsecondarypoints(Context con, LoyalityModel loyalityModel, RequestListener requestListener) {
        String url = UrlResolver.API_SECONDARY_POINTS;
        Log.d("url checkCouponScratchPoints", url);
        Request<String> volleyreq = simplevolleyrequest(
                con, Request.Method.POST, url, loyalityModel, requestListener);
        volleyreq.setShouldCache(false);
        dispatchToQueue(volleyreq, con);
    }
    @SuppressLint("LongLogTag")
    public static void getpopup(Context con, HomeCountModel homeCountModel, RequestListener requestListener) {
        String url = UrlResolver.API_POPUPS;
        Log.d("url checkCouponScratchPoints", url);
        Request<String> volleyreq = simplevolleyrequest(
                con, Request.Method.POST, url, homeCountModel, requestListener);
        volleyreq.setShouldCache(false);
        dispatchToQueue(volleyreq, con);
    }

    private static void buildMultipartEntity() {
        mBuilder.addBinaryBody("file", mImageFile, ContentType.create("image/jpeg"), mImageFile.getName());
        mBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        mBuilder.setLaxMode().setBoundary("xx").setCharset(Charset.forName("UTF-8"));
    }

    public interface ERROR_CODES {
        int BAD_REQUEST = 400;
        int UNAUTHORISED = 401;
        int UNAUTHORIZED_ACCESS = 403;
        int NOT_FOUND = 404;
        int USERNAME_NOT_AVAILABLE = 409;
        int SOURCE_FILE_DOESNT_EXIST = 920;
        int CUSTOM_ERROR_CODE = 1001;
        int SHIT_HAPPENED = 1022;
        int FILES_MISSING = 1023;
        int NO_INTERNET = 1025;
        int EMPTY_RESULTS = 1026;
        int API_FAILURE = 1027;

    }
}
