package com.sar.user.livfastRewards.Model;

/**
 * Created by Nav on 08-Aug-17.
 */

public class GetRedeemHistoryModel {
    public UserModel user;
    int id, value, points_redeem, redeem_scheme;
    String otp, redeem_status, verification_status, created_timestamp, updated_timestamp;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getLoyalty_point() {
        return points_redeem;
    }

    public void setLoyalty_point(int loyalty_point) {
        this.points_redeem = loyalty_point;
    }

    public int getRedeem_scheme() {
        return redeem_scheme;
    }

    public void setRedeem_scheme(int redeem_scheme) {
        this.redeem_scheme = redeem_scheme;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getRedeem_state() {
        return redeem_status;
    }

    public void setRedeem_state(String redeem_state) {
        this.redeem_status = redeem_state;
    }

    public String getVerification_status() {
        return verification_status;
    }

    public void setVerification_status(String verification_status) {
        this.verification_status = verification_status;
    }

    public String getCreated_timestamp() {
        return created_timestamp;
    }

    public void setCreated_timestamp(String created_timestamp) {
        this.created_timestamp = created_timestamp;
    }

    public String getUpdated_timestamp() {
        return updated_timestamp;
    }

    public void setUpdated_timestamp(String updated_timestamp) {
        this.updated_timestamp = updated_timestamp;
    }

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }

    class UserModel {
        String batcode;
        int total_loyalty, dist_code;

        public String getBatcode() {
            return batcode;
        }

        public void setBatcode(String batcode) {
            this.batcode = batcode;
        }

        public int getTotal_loyalty() {
            return total_loyalty;
        }

        public void setTotal_loyalty(int total_loyalty) {
            this.total_loyalty = total_loyalty;
        }

        public int getDist_code() {
            return dist_code;
        }

        public void setDist_code(int dist_code) {
            this.dist_code = dist_code;
        }
    }
}
