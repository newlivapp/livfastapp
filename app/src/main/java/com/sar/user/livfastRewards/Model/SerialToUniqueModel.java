package com.sar.user.livfastRewards.Model;

/**
 * Created by Nav on 31-Jul-17.
 */

public class SerialToUniqueModel {
    public String serial_number;

    public String getSerial_number() {
        return serial_number;
    }

    public void setSerial_number(String serial_number) {
        this.serial_number = serial_number;
    }
}
