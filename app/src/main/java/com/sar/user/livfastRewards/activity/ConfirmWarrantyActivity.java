package com.sar.user.livfastRewards.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.sar.user.livfastRewards.Interface.RequestListener;
import com.sar.user.livfastRewards.Model.AddWarrantyModel;
import com.sar.user.livfastRewards.Model.FileUploadModel;
import com.sar.user.livfastRewards.Model.LoyalityModel;
import com.sar.user.livfastRewards.Model.ProductWarrantyDetailModel;
import com.sar.user.livfastRewards.R;
import com.sar.user.livfastRewards.helper.Controller;
import com.sar.user.livfastRewards.helper.JsonUtils;
import com.sar.user.livfastRewards.helper.PrefManager;

import org.json.JSONException;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ConfirmWarrantyActivity extends BaseActivity {
    EditText qrCode, customer_name, contact_no;
    ProgressBar progressBar;
    TextView dateView;
    Toolbar toolbar;
    ImageView back_button;
    ImageView back_click;
    Button register;
    Calendar calendar = Calendar.getInstance();
    int year = calendar.get(Calendar.YEAR);
    int month = calendar.get(Calendar.MONTH);
    int day = calendar.get(Calendar.DAY_OF_MONTH);
    String image_path, unique_code, strCustomerName, strContactNo, strVehicleNo, segment_id, vehicle_manufacturer_id, vehicle_model_id, user_id;
    String pincode, state, city, remarks;
    String select_date;
    RequestListener loyalityListner = new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @Override
        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
            Log.d("response getloyality", responseObject.toString());
            final LoyalityModel loyalityModel = JsonUtils.objectify(responseObject.toString(), LoyalityModel.class);
            Intent intent = new Intent(ConfirmWarrantyActivity.this, ProductDetailActivity.class);
            startActivity(intent);
            finish();

        }

        @Override
        public void onRequestError(int errorCode, String message) {

        }
    };
    RequestListener addWarrantyListener = new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @Override
        public void onRequestCompleted(final Object responseObject) throws JSONException, ParseException {
            Log.d("response warranty", responseObject.toString());
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                    register.setEnabled(true);
                }
            });
            final ProductWarrantyDetailModel productWarrantyDetailModel = JsonUtils.objectify(responseObject.toString(), ProductWarrantyDetailModel.class);
            if (responseObject.toString().equals("Please Add Product In Add Purchase First")) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(ConfirmWarrantyActivity.this, "Please Add Product In Add Purchase First", Toast.LENGTH_SHORT).show();
                    }
                });
            } else if (responseObject.toString().equals("Something Went Wrong")) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(ConfirmWarrantyActivity.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
                    }
                });
            } else if (responseObject.toString().equals("Serial Number is not in Scheme")) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(ConfirmWarrantyActivity.this, "Serial Number is not in Scheme", Toast.LENGTH_SHORT).show();
                    }
                });
            } else if (responseObject.toString().equals("Serial No Already Scanned !")) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(ConfirmWarrantyActivity.this, "This Product is Already Scanned", Toast.LENGTH_SHORT).show();
                    }
                });
            } else if (responseObject.toString().equals("Invalid Serial Number")) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(ConfirmWarrantyActivity.this, "Invalid Serial Number", Toast.LENGTH_SHORT).show();
                    }
                });
            } else if (responseObject.toString().equals("Not Exist In CRM !")) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(ConfirmWarrantyActivity.this, "Not Exist In CRM !", Toast.LENGTH_SHORT).show();
                    }
                });

            } else {

                if (productWarrantyDetailModel == null || productWarrantyDetailModel.getDealer_product() == null || productWarrantyDetailModel.getDealer_product().isEmpty()) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(ConfirmWarrantyActivity.this, responseObject.toString(), Toast.LENGTH_SHORT).show();
                        }
                    });
                    return;
                }

                //Custome Analytics Fabric
                Answers.getInstance().logCustom(new CustomEvent("Tertiary")
                        .putCustomAttribute("BAT_CODE", PrefManager.getInstance(ConfirmWarrantyActivity.this).getBatCode())
                        .putCustomAttribute("USER_NAME", PrefManager.getInstance(ConfirmWarrantyActivity.this).getDealershipName())
                        .putCustomAttribute("UNIQUE_NUMBER", qrCode.getText().toString().trim()));

                //   pref.saveProductModel(productWarrantyDetailModel);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                        final Dialog dialog = new Dialog(ConfirmWarrantyActivity.this);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.popup_massage);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                        final Button addmore = (Button) dialog.findViewById(R.id.addmore);
                        final TextView close = (TextView) dialog.findViewById(R.id.close);
                        final TextView msg = (TextView) dialog.findViewById(R.id.bettarymsg);
                        addmore.setText("Add More Warranty");

                        msg.setText("Warranty of " + productWarrantyDetailModel.getDealer_product() + " battery has been added successfully");

                        addmore.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                Intent intent = new Intent(ConfirmWarrantyActivity.this, ScannerActivity.class);
                                intent.putExtra("AddWarranty", "NewWarranty");
                                startActivity(intent);
                                finish();
                            }
                        });
                        close.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                LoyalityModel loyalityModel = new LoyalityModel();
                                loyalityModel.setUser_id(user_id);
                                Controller.getloyality(ConfirmWarrantyActivity.this, loyalityModel, loyalityListner);
                            }
                        });
                        if (ConfirmWarrantyActivity.this != null)
                            ConfirmWarrantyActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (!(ConfirmWarrantyActivity.this).isFinishing()) {
                                        try {
                                            dialog.show();
                                        } catch (WindowManager.BadTokenException e) {
                                            Log.e("WindowManagerBad ", e.toString());
                                        } catch (Exception e) {
                                            Log.e("Exception ", e.toString());
                                        }
                                    }
                                }
                            });
                    }
                });

            }

        }

        @Override
        public void onRequestError(final int errorCode, String message) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    register.setEnabled(true);
                    progressBar.setVisibility(View.GONE);
                }
            });

            handelError(errorCode, message);
        }
    };
    View.OnClickListener backClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_warranty);
        toolbar = findViewById(R.id.toolbar);
        back_button = toolbar.findViewById(R.id.back_button);
        back_click = toolbar.findViewById(R.id.back_button);
        back_button.setOnClickListener(backClick);
        qrCode = findViewById(R.id.qrcode);
        customer_name = findViewById(R.id.customer_name);
        contact_no = findViewById(R.id.contact_no);
        progressBar = findViewById(R.id.progressbar);
        dateView = findViewById(R.id.date);
        register = findViewById(R.id.add_button);

        unique_code = getIntent().getStringExtra("qrCode");
        strCustomerName = getIntent().getStringExtra("customer_name");
        strContactNo = getIntent().getStringExtra("contact_no");
        strVehicleNo = getIntent().getStringExtra("vehicle_no");
        segment_id = getIntent().getStringExtra("segment_id");
        vehicle_manufacturer_id = getIntent().getStringExtra("vehicle_manufacturer_id");
        vehicle_model_id = getIntent().getStringExtra("vehicle_model_id");
        image_path = getIntent().getStringExtra("image_path");
        pincode = getIntent().getStringExtra("pincode");
        state = getIntent().getStringExtra("state");
        city = getIntent().getStringExtra("city");
        remarks = getIntent().getStringExtra("remarks");

        qrCode.setText(unique_code);
        customer_name.setText(strCustomerName);
        contact_no.setText(strContactNo);
        dateView.setText("");
        user_id = PrefManager.getInstance(this).getÜserId();

        final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        final String[] toDate = {dateFormat.format(date)};
        dateView.setText(dateFormat.format(date));
        dateView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final com.wdullaer.materialdatetimepicker.date.DatePickerDialog dpd = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance(new com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(com.wdullaer.materialdatetimepicker.date.DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                         select_date = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        dateView.setText(select_date);
                        toDate[0] =select_date;
                    }
                }, year, month, day);
                dpd.setMaxDate(calendar);
                dpd.show(getFragmentManager(), "DATE_PICKER_TAG");
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                register.setEnabled(false);
                AddWarrantyModel addWarrantyModel = new AddWarrantyModel();
                addWarrantyModel.setUser_id(user_id);
                addWarrantyModel.setUnique_code(unique_code);
                addWarrantyModel.setSell_date(toDate[0]);
                addWarrantyModel.setCustomer_name(strCustomerName);
                addWarrantyModel.setCustomer_phone(strContactNo);
                addWarrantyModel.setCar_segment(segment_id);
                addWarrantyModel.setVehicle_manufacturer(segment_id);
                addWarrantyModel.setVehicle_model(segment_id);
                addWarrantyModel.setVehicle_number(strVehicleNo);
                addWarrantyModel.setPincode(pincode);
                addWarrantyModel.setState(state);
                addWarrantyModel.setCity(city);
                addWarrantyModel.setRemarks(remarks);
                if (image_path != null) {
                    FileUploadModel fileUploadModel = new FileUploadModel();
                    fileUploadModel.setFile(image_path);
                    Controller.warranty(ConfirmWarrantyActivity.this, addWarrantyModel, addWarrantyListener);
                    progressBar.setVisibility(View.VISIBLE);
                } else {
                    Controller.warranty(ConfirmWarrantyActivity.this, addWarrantyModel, addWarrantyListener);
                    progressBar.setVisibility(View.VISIBLE);
                }
            }
        });
    }

}
