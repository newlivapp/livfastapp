package com.sar.user.livfastRewards.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.sar.user.livfastRewards.Model.ProductListModel;

import java.util.List;

public class ListOfAddedProductAdapter extends RecyclerView.Adapter<ListOfAddedProductAdapter.MyViewHolder> {
    private Context context;
    private List<ProductListModel> productListModelList;

    public ListOfAddedProductAdapter(Context context, List<ProductListModel> productListModelList1) {
        this.context = context;
        this.productListModelList = productListModelList1;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public MyViewHolder(View itemView) {
            super(itemView);
        }
    }
}
