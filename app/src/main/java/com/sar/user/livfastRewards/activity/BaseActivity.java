package com.sar.user.livfastRewards.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.sar.user.livfastRewards.Model.APIErrorResponseModel;
import com.sar.user.livfastRewards.Model.ErrorResponseModel;
import com.sar.user.livfastRewards.R;
import com.sar.user.livfastRewards.dialog.InfoDialog;
import com.sar.user.livfastRewards.helper.JsonUtils;
import com.sar.user.livfastRewards.helper.MyUtils;
import com.sar.user.livfastRewards.helper.PrefManager;

import java.util.concurrent.ExecutionException;

public class BaseActivity extends AppCompatActivity {

    AlertDialog alert;
    private boolean isNetDialogShowing = false;
    private Dialog internetDialog;
    public BroadcastReceiver internetConnectionReciever = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            ConnectivityManager connectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetInfo = connectivityManager
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            NetworkInfo activeWIFIInfo = connectivityManager
                    .getNetworkInfo(connectivityManager.TYPE_WIFI);

            if (activeWIFIInfo.isConnected() || activeNetInfo.isConnected()) {
                isOldVersion();
                removeInternetDialog();
            } else {
                showInternetDialog();
            }
        }
    };
    private boolean isInternetReceiverRegistered = false;
    private BroadcastReceiver mRegistrationBroadcastReceiver;

    public void handelError(final int errorCode, final String message) {
        Log.d("message", message);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (message == "Network Not Available") {
                    Toast.makeText(BaseActivity.this, " Network Not Available" + errorCode, Toast.LENGTH_SHORT).show();
                } else {
                    if (errorCode >= 400 && errorCode < 500) {
                        if (errorCode == 403) {
                            Toast.makeText(BaseActivity.this, "UnAuthorised!", Toast.LENGTH_SHORT).show();
                        } else if (errorCode == 401) {
                            logout();
                        } else {
                            ErrorResponseModel errorResponseModel = JsonUtils.objectify(message, ErrorResponseModel.class);
                            if (errorResponseModel == null || errorResponseModel.getErr() == null || errorResponseModel.getErr().isEmpty()) {
                                APIErrorResponseModel apiErrorResponseModel = JsonUtils.objectify(message, APIErrorResponseModel.class);
                                if (apiErrorResponseModel != null && apiErrorResponseModel.getMsg() != null && !apiErrorResponseModel.getMsg().isEmpty()) {
                                    Toast.makeText(BaseActivity.this, apiErrorResponseModel.getMsg(), Toast.LENGTH_SHORT).show();
                                    return;
                                }
                                Toast.makeText(BaseActivity.this, "Something is wrong.! Please try again later.", Toast.LENGTH_SHORT).show();
                                return;
                            }
                            Toast.makeText(BaseActivity.this, errorResponseModel.getErr(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(BaseActivity.this, "Something is wrong.! Please try again later.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private void logout() {
        PrefManager.getInstance(this).clearSession();
        Intent go = new Intent(this, LoginActivity.class);
        startActivity(go);
        finish();
    }

    @Override
    protected void onResume() {
        isOldVersion();
        super.onResume();
    }

    private boolean isOldVersion() {

        String currentVersion = "";
        String Latest_verion = "";

        GetVersionCode getVersionCode = new GetVersionCode();
        try {
            Latest_verion = getVersionCode.execute().get();
            Log.d("version", String.valueOf(Latest_verion));
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            PackageInfo pInfo = BaseActivity.this.getPackageManager().getPackageInfo(BaseActivity.this.getPackageName(), 0);
            currentVersion = (pInfo.versionName);
            Log.d("version", String.valueOf(currentVersion));

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        //-------Rendering to playstore if current version is less or more  than playstore-----\\
        if (Latest_verion != null && !Latest_verion.equals(""))
            if (!currentVersion.equals(Latest_verion)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(BaseActivity.this);

                builder.setTitle("Our App got Update");
                builder.setIcon(R.mipmap.ic_mainlauncher_round);
                builder.setCancelable(false);
                builder.setMessage("New version available, select update to update our app")
                        .setPositiveButton("UPDATE", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (alert != null) {
                                    alert.hide();
                                    alert.cancel();
                                }
                                final String appName = BaseActivity.this.getPackageName();
                                try {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appName)));
                                } catch (android.content.ActivityNotFoundException anfe) {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appName)));
                                }

                            }
                        });

                alert = builder.create();
                alert.show();

                return true;
            }

        return false;
    }

    private void checkInternet() {
        registerReceiver(internetConnectionReciever, new IntentFilter(
                "android.net.conn.CONNECTIVITY_CHANGE"));
        isInternetReceiverRegistered = true;
    }

    private void removeInternetDialog() {
        if (internetDialog != null && internetDialog.isShowing()) {
            internetDialog.dismiss();
            isNetDialogShowing = false;
            internetDialog = null;

        }
    }


    private void showInternetDialog() {
        MyUtils.removeCustomeProgressDialog();
        isNetDialogShowing = true;

        internetDialog = new InfoDialog(this,
                getString(R.string.dialog_no_inter_message_restart),
                R.string.dialog_enable) {

            @Override
            public void onPositiveClicked(DialogInterface dialog) {
                // TODO Auto-generated method stub
                removeInternetDialog();
                Intent intent = new Intent(
                        android.provider.Settings.ACTION_SETTINGS);
                startActivity(intent);
            }

            @Override
            public void onNegativeClicked(DialogInterface dialog) {
                // TODO Auto-generated method stub
               /* removeInternetDialog();
                finish();
                Intent intent = new Intent(BaseActivity.this, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);*/
            }
        }.create();
        internetDialog.show();

    }


    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        if (isInternetReceiverRegistered) {
            unregisterReceiver(internetConnectionReciever);
            isInternetReceiverRegistered = false;

        }
        super.onPause();
    }
}
