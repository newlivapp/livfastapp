package com.sar.user.livfastRewards.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sar.user.livfastRewards.Model.ProductWarrantyDetailModel;
import com.sar.user.livfastRewards.R;
import com.sar.user.livfastRewards.helper.MyUtils;

import java.util.List;

public class WarrantyRecyclerViewAdapter extends RecyclerView.Adapter<WarrantyRecyclerViewAdapter.MyViewHolder> {
    Context context;
    List<ProductWarrantyDetailModel> warrantyList;

    public WarrantyRecyclerViewAdapter(Context context1, List<ProductWarrantyDetailModel> warrantyList1) {
        context = context1;
        warrantyList = warrantyList1;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.waranty_detail, parent, false);
        MyViewHolder mvh = new MyViewHolder(v);
        return mvh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        ProductWarrantyDetailModel productWarrantyDetailModel = warrantyList.get(position);
        String date = productWarrantyDetailModel.getCreated_timestamp();
        holder.date.setText(MyUtils.getValidDateFormat(date));
        holder.warranty.setText(productWarrantyDetailModel.getLoyalty_points());
        holder.product_detail.setText(productWarrantyDetailModel.getDealer_product().toUpperCase());
    }

    @Override
    public int getItemCount() {
        return warrantyList.size();
    }

    public class MyViewHolder extends ViewHolder {
        TextView date, product_detail, warranty;

        public MyViewHolder(View itemView) {
            super(itemView);
            date = (TextView) itemView.findViewById(R.id.date);
            product_detail = (TextView) itemView.findViewById(R.id.product_name);
            warranty = (TextView) itemView.findViewById(R.id.warranty);
        }
    }
}
