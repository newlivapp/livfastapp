package com.sar.user.livfastRewards.Database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.sar.user.livfastRewards.Model.ModelOfCarModel;
import com.sar.user.livfastRewards.Model.ProductListModel;
import com.sar.user.livfastRewards.Model.ProductLoyalityModel;

import java.util.ArrayList;

/**
 * Created by Nav on 02-Aug-17.
 */

public class DatabaseBackend extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "DLPLoayalty";
    private static final int DATABASE_VERSION = 6;
    //
    public static String CREATE_FILTER_DATA_STATEMENT = "create table "
            + ProductLoyalityModel.TABLE_NAME + "("
            + ProductLoyalityModel.ID + " NUMBER PRIMARY KEY, "
            + ProductLoyalityModel.BRAND + " TEXT, "
            + ProductLoyalityModel.PRODUCT + " TEXT, "
            + ProductLoyalityModel.WARRANTY + " TEXT, "
            + ProductLoyalityModel.CAPACITY + " TEXT, "
            + ProductLoyalityModel.QUANTITY + " NUMBER, " +
            "UNIQUE(" + ProductLoyalityModel.ID + ") ON CONFLICT REPLACE" +
            ");";
    public static String CREATE_BATTERY_DATA_STATEMENT = "create table "
            + ModelOfCarModel.TABLE_NAME + "("
            + ModelOfCarModel.ID + " NUMBER PRIMARY KEY, "
            + ModelOfCarModel.VEHICLE_MANUFACTURER + " NUMBER, "
            + ModelOfCarModel.CAR_SEGMENT + " TEXT, "
            + ModelOfCarModel.VEHICLE_MODEL + " TEXT, "
            + ModelOfCarModel.FUEL + " TEXT, "
            + ModelOfCarModel.PRODUCT_JSON + " TEXT, " +
            "UNIQUE(" + ModelOfCarModel.ID + ") ON CONFLICT REPLACE" +
            ");";
    public static String CREATE_PRODUCT_LIST = "create table "
            + ProductListModel.TABLE_NAME + "("
            + ProductListModel.UNIQUE_CODE + " TEXT, "
            + ProductListModel.COMMENT + " TEXT, "
            + ProductListModel.PRODUCT_IMAGE_PATH + " TEXT " +
            ");";
    private static DatabaseBackend instance = null;


    public DatabaseBackend(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static synchronized DatabaseBackend getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseBackend(context);
        }
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_FILTER_DATA_STATEMENT);
        db.execSQL(CREATE_BATTERY_DATA_STATEMENT);
        db.execSQL(CREATE_PRODUCT_LIST);
    }

    public void createFilterData(ProductLoyalityModel filterDataModel) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(filterDataModel.TABLE_NAME, null, filterDataModel.getContentValues());
    }

    public void createFilterBatteryData(ModelOfCarModel modelOfCarModel) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(modelOfCarModel.TABLE_NAME, null, modelOfCarModel.getContentValues());
    }

    public void createProductListData(ProductListModel model) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(model.TABLE_NAME, null, model.getContentValues());
    }


//
//    public void deleteFilterData() {
//        SQLiteDatabase db = this.getWritableDatabase();
//        db.delete(ProductLoyalityModel.TABLE_NAME, null, null);
//    }
//
//    public void deleteProductListData() {
//        SQLiteDatabase db = this.getWritableDatabase();
//        db.delete(ProductListModel.TABLE_NAME, null, null);
//    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d("version", "" + oldVersion + "--------" + newVersion);
        db.execSQL("DROP TABLE IF EXISTS " + ProductLoyalityModel.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + ModelOfCarModel.TABLE_NAME);
        db.execSQL(CREATE_FILTER_DATA_STATEMENT);
        db.execSQL(CREATE_BATTERY_DATA_STATEMENT);
        //  db.execSQL(CREATE_REPORT_DATA_STATEMENT);
//        db.execSQL(CREATE_PRODUCT_LIST);
    }

    public ArrayList<ProductLoyalityModel> getFilterableProduct(String make, String model) {
        ArrayList<ProductLoyalityModel> arrayList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor;
        String[] arg = {make, model};
        cursor = db.query(ProductLoyalityModel.TABLE_NAME, null, ProductLoyalityModel.CAPACITY
                + "=? and " + ProductLoyalityModel.WARRANTY + "=?", arg, null, null, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                ProductLoyalityModel message = ProductLoyalityModel.fromCursor(cursor);
                arrayList.add(message);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return arrayList;
    }

    public ArrayList<String> getFilterableBatteryWarranty(String ah) {
        ArrayList<String> arrayList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor;
        String[] arg = {ah};
        cursor = db.query(ProductLoyalityModel.TABLE_NAME, null, ProductLoyalityModel.CAPACITY
                + "=? ", arg, null, null, null);
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                String cv = cursor.getString(cursor.getColumnIndex(ProductLoyalityModel.WARRANTY));
                if (!arrayList.contains(cv))
                    arrayList.add(cv);
                cursor.moveToNext();
            }
        }
        cursor.close();
        return arrayList;
    }

    public ArrayList<ModelOfCarModel> getBatteryFuelData(String segment, int vehicle_manufacturer, String vehicle_model) {
        ArrayList<ModelOfCarModel> list = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor;

        String[] arg = {segment, String.valueOf(vehicle_manufacturer), vehicle_model};
        cursor = db.query(ModelOfCarModel.TABLE_NAME, null, ModelOfCarModel.CAR_SEGMENT
                        + "=? and " + ModelOfCarModel.VEHICLE_MANUFACTURER + "=? and " + ModelOfCarModel.VEHICLE_MODEL + "=?", arg,
                null, null, null, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                ModelOfCarModel message = ModelOfCarModel.fromCursor(cursor);
                list.add(message);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return list;
    }

    public ArrayList<ModelOfCarModel> getBatteryData(String segment, int vehicle_manufacturer, String vehicle_model, String fuelType) {
        ArrayList<ModelOfCarModel> list = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor;

        String[] arg = {segment, String.valueOf(vehicle_manufacturer), vehicle_model, fuelType};
        cursor = db.query(ModelOfCarModel.TABLE_NAME, null, ModelOfCarModel.CAR_SEGMENT
                        + "=? and " + ModelOfCarModel.VEHICLE_MANUFACTURER + "=? and " + ModelOfCarModel.VEHICLE_MODEL + "=? and " + ModelOfCarModel.FUEL + "=?", arg,
                null, null, null, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                ModelOfCarModel message = ModelOfCarModel.fromCursor(cursor);
                list.add(message);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return list;
    }

    public ArrayList<ProductListModel> getFilterableBatteryWarranty() {
        ArrayList<ProductListModel> arrayList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor;
        cursor = db.query(ProductLoyalityModel.TABLE_NAME, null, null, null, null, null, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                ProductListModel message = ProductListModel.fromCursor(cursor);
                arrayList.add(message);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return arrayList;
    }

//    public ArrayList<SaveReportModel> getBatteryFuelData(String segment, int vehicle_manufacturer, String vehicle_model) {
//        ArrayList<ModelOfCarModel> list = new ArrayList<>();
//        SQLiteDatabase db = this.getReadableDatabase();
//        Cursor cursor;
//
//        String[] arg = {segment, String.valueOf(vehicle_manufacturer), vehicle_model};
//        cursor = db.query(ModelOfCarModel.TABLE_NAME, null, ModelOfCarModel.CAR_SEGMENT
//                        + "=? and " + ModelOfCarModel.VEHICLE_MANUFACTURER + "=? and " + ModelOfCarModel.VEHICLE_MODEL + "=?", arg,
//                null, null, null, null);
//
//        if (cursor.getCount() > 0) {
//            cursor.moveToFirst();
//            do {
//                ModelOfCarModel message = ModelOfCarModel.fromCursor(cursor);
//                list.add(message);
//            } while (cursor.moveToNext());
//        }
//        cursor.close();
//        return list;
//    }
}
