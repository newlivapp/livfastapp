package com.sar.user.livfastRewards.activity;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sar.user.livfastRewards.Interface.RequestListener;
import com.sar.user.livfastRewards.Model.GetRedeemHistoryAPIModel;
import com.sar.user.livfastRewards.Model.GetRedeemHistoryModel;
import com.sar.user.livfastRewards.R;
import com.sar.user.livfastRewards.adapter.RedeemHistoryRVAdapter;
import com.sar.user.livfastRewards.helper.Controller;
import com.sar.user.livfastRewards.helper.PrefManager;

import org.json.JSONException;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class RedeemHistoryActivity extends BaseActivity {
    final int SCREEN_IB_UPS = 1;
    final int SCREEN_FOUR_W = 2;
    final int SCREEN_E_RICK = 3;
    Toolbar toolbar;
    ImageView back_button;
    RecyclerView redeemhistory_rv;
    RedeemHistoryRVAdapter redeemHistoryRVAdapter;
    List<GetRedeemHistoryModel> redeemhistoryList;
    //    ProgressBar progressBar;
    SwipeRefreshLayout swipeRefreshLayout;
    RelativeLayout emplty_rl;
    TextView tvTitle;
    int SCREEN = 0;

    GetRedeemHistoryAPIModel getRedeemHistoryAPIModel;
    RequestListener redeemListner = new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @Override
        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
            Log.d("response redeemListner", responseObject.toString());
            redeemhistoryList.clear();
            Type collection = new TypeToken<List<GetRedeemHistoryModel>>() {
            }.getType();
            List<GetRedeemHistoryModel> grhm = (List<GetRedeemHistoryModel>) new Gson().fromJson(responseObject.toString(), collection);
            redeemhistoryList.addAll(grhm);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (redeemhistoryList.isEmpty()) {
                        emplty_rl.setVisibility(View.VISIBLE);
                        redeemhistory_rv.setVisibility(View.GONE);
                    } else {
                        emplty_rl.setVisibility(View.GONE);
                        redeemhistory_rv.setVisibility(View.VISIBLE);
                    }
                    redeemHistoryRVAdapter.notifyDataSetChanged();
                    swipeRefreshLayout.setRefreshing(false);
                }
            });
        }

        @Override
        public void onRequestError(int errorCode, String message) {
            handelError(errorCode, message);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    swipeRefreshLayout.setRefreshing(false);
                }
            });
        }
    };
    View.OnClickListener backClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_redeem_history);


        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("screen"))
            SCREEN = getIntent().getExtras().getInt("screen");
        else
            finish();

        getRedeemHistoryAPIModel = new GetRedeemHistoryAPIModel();
        getRedeemHistoryAPIModel.setUser_id(PrefManager.getInstance(RedeemHistoryActivity.this).getÜserId());
        if (SCREEN == SCREEN_IB_UPS)
            getRedeemHistoryAPIModel.setCategory("IB(Tertiary)");
        else if (SCREEN == SCREEN_FOUR_W)
            getRedeemHistoryAPIModel.setCategory("4W(Tertiary)");
        else if (SCREEN == SCREEN_E_RICK)
            getRedeemHistoryAPIModel.setCategory("ERICK(Tertiary)");

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        back_button = (ImageView) toolbar.findViewById(R.id.back_button);
        tvTitle = toolbar.findViewById(R.id.toolbar_title);
        redeemhistory_rv = (RecyclerView) findViewById(R.id.redeemhistory_rv);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);
        emplty_rl = (RelativeLayout) findViewById(R.id.empty_rl);
        back_button.setOnClickListener(backClick);
//        progressBar= (ProgressBar) findViewById(R.id.progressbar);
        redeemhistoryList = new ArrayList<GetRedeemHistoryModel>();
//        progressBar.setVisibility(View.VISIBLE);

        //set title based on screen
        if (SCREEN == SCREEN_IB_UPS)
            tvTitle.setText("IB+UPS Redeem History");
        else if (SCREEN == SCREEN_FOUR_W)
            tvTitle.setText("4W Redeem History");
        else if (SCREEN == SCREEN_E_RICK)
            tvTitle.setText("E-RICK Redeem History");
        else //if there is no screen found than close this
            finish();


        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Controller.GetRedeemHistory(RedeemHistoryActivity.this, getRedeemHistoryAPIModel, redeemListner);
            }
        });

        Controller.GetRedeemHistory(RedeemHistoryActivity.this, getRedeemHistoryAPIModel, redeemListner);

        redeemHistoryRVAdapter = new RedeemHistoryRVAdapter(RedeemHistoryActivity.this, redeemhistoryList);
        redeemhistory_rv.setLayoutManager(new LinearLayoutManager(RedeemHistoryActivity.this, LinearLayoutManager.VERTICAL, false));
        redeemhistory_rv.setAdapter(redeemHistoryRVAdapter);
        redeemhistory_rv.setHasFixedSize(true);
    }
}
