package com.sar.user.livfastRewards.Model;

/**
 * Created by i5tagbin2 on 20/9/17.
 */

public class RedeemResponseModel {
    int id, value, loyalty_point, redeem_scheme, user;
    String otp, redeem_state, verification_status, created_timestamp, updated_timestamp;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getLoyalty_point() {
        return loyalty_point;
    }

    public void setLoyalty_point(int loyalty_point) {
        this.loyalty_point = loyalty_point;
    }

    public int getRedeem_scheme() {
        return redeem_scheme;
    }

    public void setRedeem_scheme(int redeem_scheme) {
        this.redeem_scheme = redeem_scheme;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getRedeem_state() {
        return redeem_state;
    }

    public void setRedeem_state(String redeem_state) {
        this.redeem_state = redeem_state;
    }

    public String getVerification_status() {
        return verification_status;
    }

    public void setVerification_status(String verification_status) {
        this.verification_status = verification_status;
    }

    public String getCreated_timestamp() {
        return created_timestamp;
    }

    public void setCreated_timestamp(String created_timestamp) {
        this.created_timestamp = created_timestamp;
    }

    public String getUpdated_timestamp() {
        return updated_timestamp;
    }

    public void setUpdated_timestamp(String updated_timestamp) {
        this.updated_timestamp = updated_timestamp;
    }
}
