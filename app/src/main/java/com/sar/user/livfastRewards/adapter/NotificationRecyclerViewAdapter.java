package com.sar.user.livfastRewards.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.format.DateUtils;
import android.text.style.URLSpan;
import android.text.util.Linkify;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.sar.user.livfastRewards.Model.NotificationModel;
import com.sar.user.livfastRewards.R;
import com.sar.user.livfastRewards.activity.WebViewActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by Navneet on 02-04-2017.
 */

public class NotificationRecyclerViewAdapter extends RecyclerView.Adapter<NotificationRecyclerViewAdapter.MyViewHolder> {
    Context context;
    List<NotificationModel> notificationModelList;

    public NotificationRecyclerViewAdapter(Context context1, List<NotificationModel> notificationModelList1) {
        context = context1;
        notificationModelList = notificationModelList1;
    }

    @Override
    public NotificationRecyclerViewAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.notification_detail, parent, false);
        MyViewHolder mvh = new MyViewHolder(v);
        return mvh;
    }

    @Override
    public void onBindViewHolder(final NotificationRecyclerViewAdapter.MyViewHolder holder, final int position) {
        holder.notification.setText(notificationModelList.get(position).getNotification());

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        long time = 0;
        try {
            time = sdf.parse(notificationModelList.get(position).getCreated_timestamp()).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long now = System.currentTimeMillis();

        CharSequence ago = DateUtils.getRelativeTimeSpanString(time, now, DateUtils.MINUTE_IN_MILLIS);

        final String date = notificationModelList.get(position).getCreated_timestamp();
        holder.time.setText(ago);
        holder.content_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.notification_popup);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                final ImageView banner = (ImageView) dialog.findViewById(R.id.banner);
                final ImageView dissmiss = (ImageView) dialog.findViewById(R.id.dissmiss);
                final TextView notification = (TextView) dialog.findViewById(R.id.title);
                final TextView description = (TextView) dialog.findViewById(R.id.description);
                final TextView link = (TextView) dialog.findViewById(R.id.link);
                ImageLoader.getInstance().loadImage(notificationModelList.get(position).getLarge_image(), new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        banner.setImageBitmap(loadedImage);
                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {
                    }
                });

                notification.setText(notificationModelList.get(position).getNotification());
                description.setText(notificationModelList.get(position).getDescription());
                link.setText(notificationModelList.get(position).getLink());
                SpannableStringBuilder ssb = new SpannableStringBuilder();
                ssb.append(link.getText());
                ssb.setSpan(new URLSpan("#"), 0, ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                link.setText(ssb, TextView.BufferType.SPANNABLE);
                link.setAutoLinkMask(Linkify.ALL);

                dissmiss.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                link.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, WebViewActivity.class);
                        Log.d("NotificationLink", link.getText().toString());
                        intent.putExtra("link", link.getText().toString());
                        context.startActivity(intent);
                    }
                });
                dialog.show();
                dialog.setCancelable(false);
            }
        });
    }

    @Override
    public int getItemCount() {
        return notificationModelList.size();
    }

    public class MyViewHolder extends ViewHolder {
        TextView notification, time;
        LinearLayout content_layout;

        public MyViewHolder(View itemView) {
            super(itemView);
            notification = (TextView) itemView.findViewById(R.id.notification);
            time = (TextView) itemView.findViewById(R.id.notification_time);
            content_layout = (LinearLayout) itemView.findViewById(R.id.content_layout);
        }
    }
}
