package com.sar.user.livfastRewards.Model;

/**
 * Created by Nav on 01-Aug-17.
 */

public class GetStockModel {
    int count;
    String model_number;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }


    public String getModel_number() {
        return model_number;
    }

    public void setModel_number(String model_number) {
        this.model_number = model_number;
    }


}
