package com.sar.user.livfastRewards.Model;

import java.util.List;

/**
 * Created by Nav on 03-Aug-17.
 */

public class CalculateLoyaltyPointsModel {
    List<ProductListForCalculateModel> products;
    String end_date;

    public List<ProductListForCalculateModel> getProducts() {
        return products;
    }

    public void setProducts(List<ProductListForCalculateModel> products) {
        this.products = products;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }
}
