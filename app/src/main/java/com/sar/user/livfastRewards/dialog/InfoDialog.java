package com.sar.user.livfastRewards.dialog;

import android.content.Context;
import android.content.DialogInterface;


abstract public class InfoDialog extends BaseAlertDialog {

    public InfoDialog(Context context, int msg, int positiveBtnName, int nagativeBtnName) {
        super(context);

        setCancelable(false);
        setPositiveButton(positiveBtnName,
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        onPositiveClicked(dialog);
                    }
                });

        setNegativeButton(nagativeBtnName,
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        onNegativeClicked(dialog);
                    }
                });
        setMessage(msg);
        // TODO Auto-generated constructor stub
    }


    public InfoDialog(Context context, String msg, int positiveBtnName) {
        super(context);

        setCancelable(false);
        setPositiveButton(positiveBtnName,
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        onPositiveClicked(dialog);
                    }
                });
        setMessage(msg);
    }

    public abstract void onPositiveClicked(DialogInterface dialog);

    public abstract void onNegativeClicked(DialogInterface dialog);
}
