package com.sar.user.livfastRewards.Model;

/**
 * Created by user on 31-03-2017.
 */

public class LoginModel {
    private String bat_code;
    private String password;
    private String verification_otp;
    private String job_type;

    public String getJob_Type() {
        return job_type;
    }

    public void setJob_Type(String job_type) {
        this.job_type = job_type;
    }

    public String getBat_code() {
        return bat_code;
    }

    public void setBat_code(String bat_code) {
        this.bat_code = bat_code;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getVerification_otp() {
        return verification_otp;
    }

    public void setVerification_otp(String verification_otp) {
        this.verification_otp = verification_otp;
    }


}
