package com.sar.user.livfastRewards.Model;

/**
 * Created by user on 05-04-2017.
 */

public class BannerModel {
    int id;
    String small_image, large_image;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSmall_image() {
        return small_image;
    }

    public void setSmall_image(String small_image) {
        this.small_image = small_image;
    }

    public String getLarge_image() {
        return large_image;
    }

    public void setLarge_image(String large_image) {
        this.large_image = large_image;
    }
}
