package com.sar.user.livfastRewards.Model;

/**
 * Created by user on 03-04-2017.
 */

public class PurchaseProductModelNew {
    int id, user;
    double loyalty_point;
    String created_timestamp;
    String model_number;

    /* public DealeProductModel model_number;

    public DealeProductModel getModel_number() {
        return model_number;
    }

    public void setModel_number(DealeProductModel model_number) {
        this.model_number = model_number;
    }*/

    public String getModel_number() {
        return model_number;
    }

    public void setModel_number(String model_number) {
        this.model_number = model_number;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public double getLoyalty_point() {
        return loyalty_point;
    }

    public void setLoyalty_point(double loyalty_point) {
        this.loyalty_point = loyalty_point;
    }

    public String getCreated_timestamp() {
        return created_timestamp;
    }

    public void setCreated_timestamp(String created_timestamp) {
        this.created_timestamp = created_timestamp;
    }
}
