package com.sar.user.livfastRewards.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.sar.user.livfastRewards.Interface.RequestListener;
import com.sar.user.livfastRewards.Model.APIErrorResponseModel;
import com.sar.user.livfastRewards.Model.GetCouponAPIModel;
import com.sar.user.livfastRewards.R;
import com.sar.user.livfastRewards.dialog.InfoDialog;
import com.sar.user.livfastRewards.fragment.Logs_fragment;
import com.sar.user.livfastRewards.fragment.Notification_fragment;
import com.sar.user.livfastRewards.fragment.Target_fragmnet;
import com.sar.user.livfastRewards.fragment.home_fragment;
import com.sar.user.livfastRewards.helper.Controller;
import com.sar.user.livfastRewards.helper.JsonUtils;
import com.sar.user.livfastRewards.helper.MyUtils;
import com.sar.user.livfastRewards.helper.PrefManager;

import org.json.JSONException;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class ProductDetailActivity extends AppCompatActivity {
    ViewPager mainViewPager;
    FloatingActionsMenu main_menu;
    RelativeLayout main_realtive_container;
    int position = 0;
    AlertDialog alert;
    RequestListener apiCheckPointsRequestListener = new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @Override
        public void onRequestCompleted(final Object responseObject) throws JSONException, ParseException {

            runOnUiThread(new Runnable() {
                @SuppressLint("LongLogTag")
                @Override
                public void run() {
                    Log.e("response checkCouponScratchPoints", responseObject.toString());
                    APIErrorResponseModel apiErrorResponseModel = JsonUtils.objectify(responseObject.toString(), APIErrorResponseModel.class);
                    if (apiErrorResponseModel != null && apiErrorResponseModel.getStatus() != null && !apiErrorResponseModel.getStatus().isEmpty() && apiErrorResponseModel.getStatus().equalsIgnoreCase("success")) {

                        final Dialog dialog = new Dialog(ProductDetailActivity.this);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.popup_win_coupon);
                        dialog.setCancelable(false);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

                        TextView tvCouponMessage = dialog.findViewById(R.id.tvCouponMessage);
                        tvCouponMessage.setText(apiErrorResponseModel.getMsg());

                        TextView tvClose = dialog.findViewById(R.id.tvClose);
                        tvClose.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });

                        Button btnCouponWallet = dialog.findViewById(R.id.btnCouponWallet);
                        btnCouponWallet.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                startActivity(new Intent(ProductDetailActivity.this, ScratchListActivity.class));
                            }
                        });

                        if (ProductDetailActivity.this != null)
                            ProductDetailActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (!(ProductDetailActivity.this).isFinishing()) {
                                        try {
                                            dialog.show();
                                        } catch (WindowManager.BadTokenException e) {
                                            Log.e("WindowManagerBad ", e.toString());
                                        } catch (Exception e) {
                                            Log.e("Exception ", e.toString());
                                        }
                                    }
                                }
                            });
                    }
                }
            });
        }

        @Override
        public void onRequestError(int errorCode, String message) {

        }
    };
    private TabLayout tabLayout;
    private boolean isNetDialogShowing = false;
    private Dialog internetDialog;
    public BroadcastReceiver internetConnectionReciever = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            ConnectivityManager connectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetInfo = connectivityManager
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            NetworkInfo activeWIFIInfo = connectivityManager
                    .getNetworkInfo(connectivityManager.TYPE_WIFI);

            if (activeWIFIInfo.isConnected() || activeNetInfo.isConnected()) {
                isOldVersion();
                removeInternetDialog();
            } else {
                showInternetDialog();
            }
        }
    };
    private boolean isInternetReceiverRegistered = false;
    private BroadcastReceiver mRegistrationBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mainViewPager = (ViewPager) findViewById(R.id.main_viewpager);
        setupViewPager(mainViewPager);
        tabLayout = (TabLayout) findViewById(R.id.tabbar);
        tabLayout.setupWithViewPager(mainViewPager);
        setUpTabIcons();
        main_realtive_container = (RelativeLayout) findViewById(R.id.background_layout);
        main_menu = (FloatingActionsMenu) findViewById(R.id.multiple_actions);
        final FloatingActionButton register_warranty = (FloatingActionButton) findViewById(R.id.register_warranty);
        final FloatingActionButton new_purchanse = (FloatingActionButton) findViewById(R.id.new_purchase);
        final FloatingActionButton serive_request = (FloatingActionButton) findViewById(R.id.service_request);
        Bundle extra = getIntent().getExtras();
        if (extra != null) {
            if (getIntent().getStringExtra("product_tab") != null && getIntent().getStringExtra("product_tab").equals("product")) {
                position = extra.getInt("tab");
                mainViewPager.setCurrentItem(position);
            } else if (getIntent().getStringExtra("warranty_tab") != null && getIntent().getStringExtra("warranty_tab").equals("warranty")) {
                position = extra.getInt("tab");
                mainViewPager.setCurrentItem(position);
            } else if (getIntent().getStringExtra("service_tab") != null && getIntent().getStringExtra("service_tab").equals("service")) {
                position = extra.getInt("tab");
                mainViewPager.setCurrentItem(position);
            }
        }

        main_menu.setOnFloatingActionsMenuUpdateListener(new FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {
            @Override
            public void onMenuExpanded() {
                main_realtive_container.setVisibility(View.VISIBLE);
                main_realtive_container.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {
                        main_menu.collapse();
                        return true;
                    }
                });
            }

            @Override
            public void onMenuCollapsed() {
                main_realtive_container.setVisibility(View.GONE);
                main_realtive_container.setOnTouchListener(null);
            }
        });
        new_purchanse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                main_menu.collapse();
                Intent intent = new Intent(ProductDetailActivity.this, ScannerActivity.class);
                intent.putExtra("AddProduct", "NewProduct");
                startActivity(intent);
            }
        });
        register_warranty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                main_menu.collapse();
                Intent intent1 = new Intent(ProductDetailActivity.this, ScannerActivity.class);
                intent1.putExtra("AddWarranty", "NewWarranty");
                startActivity(intent1);
            }
        });
        serive_request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                main_menu.collapse();
                Intent intent = new Intent(ProductDetailActivity.this, Service_request_activity.class);
                startActivity(intent);
            }
        });

        Log.e("userId", PrefManager.getInstance(this).getÜserId());
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkInternet();
        //isOldVersion();
        GetCouponAPIModel getCouponAPIModel = new GetCouponAPIModel();
        getCouponAPIModel.setUser_id(PrefManager.getInstance(this).getÜserId());
//        Controller.checkCouponScratchPoints(this, getCouponAPIModel, apiCheckPointsRequestListener);
    }

    private boolean isOldVersion() {

        String currentVersion = "";
        String Latest_verion = "";

        GetVersionCode getVersionCode = new GetVersionCode();
        try {
            Latest_verion = getVersionCode.execute().get();
            Log.d("version", String.valueOf(Latest_verion));
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            PackageInfo pInfo = ProductDetailActivity.this.getPackageManager().getPackageInfo(ProductDetailActivity.this.getPackageName(), 0);
            currentVersion = (pInfo.versionName);
            Log.d("version", String.valueOf(currentVersion));

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        //-------Rendering to playstore if current version is less or more  than playstore-----\\
        if (Latest_verion != null && !Latest_verion.equals(""))
            if (!currentVersion.equals(Latest_verion)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ProductDetailActivity.this);

                builder.setTitle("Our App got Update");
                builder.setIcon(R.mipmap.ic_mainlauncher_round);
                builder.setCancelable(false);
                builder.setMessage("New version available, select update to update our app")
                        .setPositiveButton("UPDATE", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (alert != null && alert.isShowing()) {
                                    alert.hide();
                                    alert.cancel();
                                }
                                final String appName = ProductDetailActivity.this.getPackageName();
                                try {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appName)));
                                } catch (android.content.ActivityNotFoundException anfe) {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appName)));
                                }

                            }
                        });

                alert = builder.create();
                if (alert != null && !alert.isShowing()) {
                    alert.show();
                }

                return true;
            }

        return false;
    }

    private void checkInternet() {
        registerReceiver(internetConnectionReciever, new IntentFilter(
                "android.net.conn.CONNECTIVITY_CHANGE"));
        isInternetReceiverRegistered = true;
    }

    private void removeInternetDialog() {
        if (internetDialog != null && internetDialog.isShowing()) {
            internetDialog.dismiss();
            isNetDialogShowing = false;
            internetDialog = null;

        }
    }

    private void showInternetDialog() {
        MyUtils.removeCustomeProgressDialog();
        isNetDialogShowing = true;

        internetDialog = new InfoDialog(this,
                getString(R.string.dialog_no_inter_message_restart),
                R.string.dialog_enable) {

            @Override
            public void onPositiveClicked(DialogInterface dialog) {
                // TODO Auto-generated method stub
                removeInternetDialog();
                Intent intent = new Intent(
                        android.provider.Settings.ACTION_SETTINGS);
                startActivity(intent);
            }

            @Override
            public void onNegativeClicked(DialogInterface dialog) {
                // TODO Auto-generated method stub
               /* removeInternetDialog();
                finish();
                Intent intent = new Intent(BaseActivity.this, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);*/
            }
        }.create();
        internetDialog.show();

    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        if (isInternetReceiverRegistered) {
            unregisterReceiver(internetConnectionReciever);
            isInternetReceiverRegistered = false;

        }
        super.onPause();
    }

    private void setUpTabIcons() {
        TextView tabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabOne.setText("Home");
        tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.home_tab_image, 0, 0);
        tabLayout.getTabAt(0).setCustomView(tabOne);

        TextView tabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabTwo.setText("Logs");
        tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.log_tab_image, 0, 0);
        tabLayout.getTabAt(1).setCustomView(tabTwo);

        TextView tabThree = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabThree.setText("Notifications");
        tabThree.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.notification_tab_image, 0, 0);
        tabLayout.getTabAt(2).setCustomView(tabThree);

        TextView tabFour = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabFour.setText("Scheme");
        tabFour.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.target_tab_image, 0, 0);
        tabLayout.getTabAt(3).setCustomView(tabFour);

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new home_fragment(this), "Home");
        adapter.addFragment(new Logs_fragment(this), "Logs");
        adapter.addFragment(new Notification_fragment(this), "Notifications");
        adapter.addFragment(new Target_fragmnet(this), "Scheme");
        viewPager.setAdapter(adapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_logout, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.change_password) {
            Intent intent = new Intent(ProductDetailActivity.this, Change_password_activity.class);
            intent.putExtra("for_action", "user_change_password");
            startActivity(intent);
        } else if (item.getItemId() == R.id.logout) {
            final Dialog dialog = new Dialog(ProductDetailActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.popup_logout);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            final Button addmore = (Button) dialog.findViewById(R.id.addmore);
            final TextView close = (TextView) dialog.findViewById(R.id.close);

            addmore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    PrefManager.getInstance(ProductDetailActivity.this).clearSession();
                    Intent intent1 = new Intent(ProductDetailActivity.this, LoginActivity.class);
                    startActivity(intent1);
                    ProductDetailActivity.this.finish();
                }
            });
            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.cancel();
                }
            });
            if (ProductDetailActivity.this != null)
                ProductDetailActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (!(ProductDetailActivity.this).isFinishing()) {
                            try {
                                dialog.show();
                            } catch (WindowManager.BadTokenException e) {
                                Log.e("WindowManagerBad ", e.toString());
                            } catch (Exception e) {
                                Log.e("Exception ", e.toString());
                            }
                        }
                    }
                });

        } else if (item.getItemId() == R.id.profile_detail) {
            Intent intent = new Intent(this, EditProfileActivity.class);
            startActivity(intent);
        } else if (item.getItemId() == R.id.downloadreport) {
            startActivity(new Intent(ProductDetailActivity.this, SaveReportActivity.class));
        } else if (item.getItemId() == R.id.scratch_cards) {
            startActivity(new Intent(ProductDetailActivity.this, ScratchListActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
        System.exit(0);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        finish();
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
