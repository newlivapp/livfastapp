package com.sar.user.livfastRewards.Model;

/**
 * Created by Nav on 03-Aug-17.
 */

public class ProductModel {
    String warranty, product, brand__name, capacity;

    public String getWarranty() {
        return warranty;
    }

    public void setWarranty(String warranty) {
        this.warranty = warranty;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getBrand__name() {
        return brand__name;
    }

    public void setBrand__name(String brand__name) {
        this.brand__name = brand__name;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }
}
