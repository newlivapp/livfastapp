package com.sar.user.livfastRewards.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sar.user.livfastRewards.Interface.CityItemSelectedListner;
import com.sar.user.livfastRewards.Model.GetCityModel;
import com.sar.user.livfastRewards.R;

import java.util.List;

/**
 * Created by i5tagbin2 on 8/9/17.
 */

public class CityRVAdapter extends RecyclerView.Adapter<CityRVAdapter.MyViewHolder> {
    Context context;
    List<GetCityModel> getCityList;
    CityItemSelectedListner cityItemSelectedListner;

    public CityRVAdapter(Context context, List<GetCityModel> cityModels, CityItemSelectedListner cityItemSelectedListner1) {
        this.context = context;
        this.getCityList = cityModels;
        this.cityItemSelectedListner = cityItemSelectedListner1;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.item_search, parent, false);
        MyViewHolder mvh = new MyViewHolder(v);
        return mvh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.cityTv.setText(getCityList.get(position).getName());
        holder.cityRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cityItemSelectedListner.onSelect(getCityList.get(position).getName(), getCityList.get(position).getId());

            }
        });
    }

    @Override
    public int getItemCount() {
        return getCityList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView cityTv;
        RelativeLayout cityRL;

        public MyViewHolder(View itemView) {
            super(itemView);
            cityTv = (TextView) itemView.findViewById(R.id.tvAutocomName);
            cityRL = (RelativeLayout) itemView.findViewById(R.id.city_RL);
        }
    }
}
