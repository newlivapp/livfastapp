package com.sar.user.livfastRewards.Model;

public class GetRedeemHistoryAPIModel {
    private String user_id, category;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
