package com.sar.user.livfastRewards.fragment;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.Toast;

import com.sar.user.livfastRewards.Model.ErrorResponseModel;
import com.sar.user.livfastRewards.helper.JsonUtils;

public class BaseFragment extends Fragment {

    public void handelError(final int errorCode, final String message) {
        Log.d("response", message);

        if ((Activity) getContext() != null) {
            ((Activity) getContext()).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (errorCode >= 400 && errorCode < 500) {
                        if (errorCode == 403) {
                            Toast.makeText(getContext(), "UnAuthorised!", Toast.LENGTH_SHORT).show();

                        } else {
                            final ErrorResponseModel errorResponseModel = JsonUtils.objectify(message, ErrorResponseModel.class);

                            assert errorResponseModel != null;
                            Toast.makeText(getContext(), errorResponseModel.getErr(), Toast.LENGTH_SHORT).show();

                        }
                    } else {
                        Toast.makeText(getContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }
}
