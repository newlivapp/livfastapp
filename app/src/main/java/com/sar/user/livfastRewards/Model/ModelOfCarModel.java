package com.sar.user.livfastRewards.Model;

import android.content.ContentValues;
import android.database.Cursor;

import com.sar.user.livfastRewards.Database.AbstractEntry;

import java.util.List;

/**
 * Created by user on 17-04-2017.
 */

public class ModelOfCarModel extends AbstractEntry {
    public static final String TABLE_NAME = "batteryFilterData";
    public static final String ID = "id";
    public static final String VEHICLE_MANUFACTURER = "vehicle_manufacturer";
    public static final String CAR_SEGMENT = "car_segment";
    public static final String VEHICLE_MODEL = "vehicle_model";
    public static final String FUEL = "fuel";
    public static final String PRODUCT_JSON = "product_json";
    int id, vehicle_manufacturer;
    String car_segment, vehicle_model, fuel, product_json;
    List<ProductModel> product;

    public ModelOfCarModel(int id, int vehicle_manufacturer, String car_segment, String vehicle_model, String fuel, String product_json) {
        this.id = id;
        this.vehicle_manufacturer = vehicle_manufacturer;
        this.car_segment = car_segment;
        this.vehicle_model = vehicle_model;
        this.fuel = fuel;
        this.product_json = product_json;
    }

    public static ModelOfCarModel fromCursor(Cursor cursor) {
        return new ModelOfCarModel(cursor.getInt(cursor.getColumnIndex(ID)),
                cursor.getInt(cursor.getColumnIndex(VEHICLE_MANUFACTURER)),
                cursor.getString(cursor.getColumnIndex(CAR_SEGMENT)),
                cursor.getString(cursor.getColumnIndex(VEHICLE_MODEL)),
                cursor.getString(cursor.getColumnIndex(FUEL)),
                cursor.getString(cursor.getColumnIndex(PRODUCT_JSON)));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getVehicle_manufacturer() {
        return vehicle_manufacturer;
    }

    public void setVehicle_manufacturer(int vehicle_manufacturer) {
        this.vehicle_manufacturer = vehicle_manufacturer;
    }

    public String getCar_segment() {
        return car_segment;
    }

    public void setCar_segment(String car_segment) {
        this.car_segment = car_segment;
    }

    public String getVehicle_model() {
        return vehicle_model;
    }

    public void setVehicle_model(String vehicle_model) {
        this.vehicle_model = vehicle_model;
    }

    public String getFuel() {
        return fuel;
    }

    public void setFuel(String fuel) {
        this.fuel = fuel;
    }

    public List<ProductModel> getProduct() {
        return product;
    }

    public void setProduct(List<ProductModel> product) {
        this.product = product;
    }

    public String getProduct_json() {
        return product_json;
    }

    public void setProduct_json(String product_json) {
        this.product_json = product_json;
    }

    @Override
    public ContentValues getContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ID, getId());
        contentValues.put(VEHICLE_MANUFACTURER, getVehicle_manufacturer());
        contentValues.put(CAR_SEGMENT, getCar_segment());
        contentValues.put(VEHICLE_MODEL, getVehicle_model());
        contentValues.put(FUEL, getFuel());
        contentValues.put(PRODUCT_JSON, getProduct_json());
        return contentValues;
    }
}
