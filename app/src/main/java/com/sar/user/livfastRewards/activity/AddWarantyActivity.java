package com.sar.user.livfastRewards.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sar.user.livfastRewards.Interface.RequestListener;
import com.sar.user.livfastRewards.Model.CarManufacturerModel;
import com.sar.user.livfastRewards.Model.ModelOfCarModel;
import com.sar.user.livfastRewards.Model.PInCodeModel;
import com.sar.user.livfastRewards.Model.SegmentModel;
import com.sar.user.livfastRewards.R;
import com.sar.user.livfastRewards.fragment.PicModeSelectDialogFragment;
import com.sar.user.livfastRewards.helper.Controller;
import com.sar.user.livfastRewards.helper.PrefManager;
import com.sar.user.livfastRewards.helper.ProfileImage.GOTOConstants;
import com.sar.user.livfastRewards.helper.ProfileImage.ImageCropActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.sar.user.livfastRewards.R.id.view_previous;

public class AddWarantyActivity extends BaseActivity {
    private static int SELECT_PICTURE = 1;
    private static String[] PERMISSIONS_LOCATION = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
    final int REQUEST_LOCATION = 2;
    ////
    final Calendar myCalendar = Calendar.getInstance();
    EditText customer_name, contact_no, vehicle_no, qrcode, Remarks;
    TextView dateofpurchase;
    Toolbar toolbar;
    EditText pincode;
    TextView statecapital, citycapital;
    ImageView back_button;
    String result;
    Button register;
    TextView upload_warranty_card;
    ImageView view_Previous, scanner_icon, warranty_card_image;
    ProgressBar progressBar;
    Spinner segment_spinner, manufacturer_spinner, model_spinner;
    List<SegmentModel> segmentList;
    List<CarManufacturerModel> manufacturerlList;
    List<ModelOfCarModel> modelsList;
    String user_id;
    int segment_id, vehicle_manufacturer_id, vehicle_model_id;
    String image_path;
    String strUniqueNo, strCustomerName, strContactNo, strptime, strdateofpurchase, strPincode, strremarks;
    List<String> segmentSpinnerList, manufacureSpinnerList, modelSpinnerList;
    //change by mohit 28-02-2019
    String strSegmentId = "";
    String strManufactureId = "";
    String strModelId = "";
    String state, city;
    PrefManager pref;
    Calendar calendar = Calendar.getInstance();
    int year = calendar.get(Calendar.YEAR);
    int month = calendar.get(Calendar.MONTH);
    int day = calendar.get(Calendar.DAY_OF_MONTH);
    int hours = calendar.get(Calendar.HOUR_OF_DAY);
    int minutes = calendar.get(Calendar.MINUTE);
    int seconds = calendar.get(Calendar.SECOND);
    //    private void updateLabel() {
//        String myFormat = "MM/dd/yy";
//        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
//        date_of_purchase.setText(sdf.format(myCalendar.getTime()));
//
//    }
    RequestListener CitystateListner = new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @Override
        public void onRequestCompleted(final Object responseObject) throws JSONException, ParseException {
            Log.d("response getStateCity", responseObject.toString());
            if (responseObject.toString().equals("Pincode not available")) {
                runOnUiThread(new Runnable() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(AddWarantyActivity.this, "Pincode not available, Enter Again", Toast.LENGTH_SHORT).show();
                        pincode.setText("");
                    }
                });

            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        register.setEnabled(false);
                        progressBar.setVisibility(View.GONE);
                        String jsonResponse = responseObject.toString();
                        try {
                            JSONArray jsonArray = new JSONArray(jsonResponse);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(0);
                                state = jsonObject1.optString("state");
                                city = jsonObject1.optString("city");
                                statecapital.setText(state.trim().toUpperCase());
                                citycapital.setText(city.trim().toUpperCase());
                                register.setEnabled(true);

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        }

        @Override
        public void onRequestError(int errorCode, String message) {

        }
    };
    View.OnClickListener uploadClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (hasLocationPermissionGranted())
                showAddProfilePicDialog1();
            else
                requestLocationPermission();
        }
    };
    View.OnClickListener backClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };
    RequestListener segmentListener = new RequestListener() {
        @Override
        public void onRequestStarted() {
        }

        @Override
        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
            Log.d("response", responseObject.toString());
            Type list = new TypeToken<List<SegmentModel>>() {
            }.getType();
            final List<SegmentModel> segmentModels = (List<SegmentModel>) new Gson().fromJson(responseObject.toString(), list);
            segmentList.addAll(segmentModels);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    for (int i = 0; i < segmentList.size(); i++) {
                        segmentSpinnerList.add(segmentList.get(i).getCar_segment());
                    }
                    progressBar.setVisibility(View.GONE);
                    ArrayAdapter<String> vehicleManufactureSpinnerArrayAdapter = new ArrayAdapter<String>(AddWarantyActivity.this, R.layout.spinner_items, segmentSpinnerList);
                    vehicleManufactureSpinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_drop_down);
                    segment_spinner.setAdapter(vehicleManufactureSpinnerArrayAdapter);
                    segment_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            if (position == 0) {
                                segment_spinner.setSelected(false);
                                strSegmentId = "";
                            } else {
                                strSegmentId = String.valueOf(segmentList.get(position - 1).getId());
                                manufacturer_spinner.setEnabled(true);
                                Log.d("SegmentID", "" + segmentList.get(position - 1).getId());
                                segment_id = segmentList.get(position - 1).getId();
                                //  Controller.GetManufacturer(AddWarantyActivity.this, segment_id, manufacturerListener);
                                //   progressBar.setVisibility(View.VISIBLE);
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }
            });
        }

        @Override
        public void onRequestError(int errorCode, String message) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                }
            });

            handelError(errorCode, message);

        }
    };
    RequestListener manufacturerListener = new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @Override
        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
            Log.d("response", responseObject.toString());
            Type list = new TypeToken<List<CarManufacturerModel>>() {
            }.getType();
            final List<CarManufacturerModel> segmentModels = (List<CarManufacturerModel>) new Gson().fromJson(responseObject.toString(), list);
            manufacturerlList.addAll(segmentModels);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    progressBar.setVisibility(View.GONE);
                    for (int i = 0; i < manufacturerlList.size(); i++) {
                        manufacureSpinnerList.add(manufacturerlList.get(i).getVehicle_manufacturer());
                    }
                }
            });

        }

        @Override
        public void onRequestError(int errorCode, String message) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                }
            });

            handelError(errorCode, message);

        }
    };
    RequestListener CarModelListener = new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @Override
        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
            Log.d("response", responseObject.toString());
            Type list = new TypeToken<List<ModelOfCarModel>>() {
            }.getType();
            final List<ModelOfCarModel> segmentModels = (List<ModelOfCarModel>) new Gson().fromJson(responseObject.toString(), list);
            modelsList.addAll(segmentModels);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    progressBar.setVisibility(View.GONE);
                    for (int i = 0; i < modelsList.size(); i++) {
                        modelSpinnerList.add(modelsList.get(i).getVehicle_model());
                    }
                }
            });
        }

        @Override
        public void onRequestError(int errorCode, String message) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                }
            });

            handelError(errorCode, message);
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_waranty);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        back_button = (ImageView) toolbar.findViewById(R.id.back_button);
        back_button.setOnClickListener(backClick);
        dateofpurchase = (TextView) findViewById(R.id.date_of_purchase);
        //time=(TextView)findViewById(R.id.time) ;
        qrcode = (EditText) findViewById(R.id.qrcode);
        customer_name = (EditText) findViewById(R.id.customer_name);
        contact_no = (EditText) findViewById(R.id.contact_no);
        vehicle_no = (EditText) findViewById(R.id.vehicle_no);
        //date_of_purchase=(EditText)findViewById(R.id.date_of_purchase);
        pincode = (EditText) findViewById(R.id.pincode);
        statecapital = (TextView) findViewById(R.id.sate);
        citycapital = (TextView) findViewById(R.id.city);
        Remarks = (EditText) findViewById(R.id.Remarks);
        register = (Button) findViewById(R.id.register_button);
        scanner_icon = (ImageView) findViewById(R.id.scanner_icon);
        view_Previous = (ImageView) findViewById(view_previous);
        progressBar = (ProgressBar) findViewById(R.id.progressbar);
        segment_spinner = (Spinner) findViewById(R.id.segment_spinner);
        manufacturer_spinner = (Spinner) findViewById(R.id.manufacturer_spinner);
        model_spinner = (Spinner) findViewById(R.id.car_model_spinner);
        warranty_card_image = (ImageView) findViewById(R.id.warranty_card_image);
        upload_warranty_card = (TextView) findViewById(R.id.upload_warranty_card_tv);

        result = getIntent().getStringExtra("result");
        qrcode.setText(result);
        qrcode.setSelection(qrcode.getText().length());
        segmentSpinnerList = new ArrayList<>();
        segmentSpinnerList.add("Segment");
        manufacureSpinnerList = new ArrayList<>();
        manufacureSpinnerList.add(" Manufacture");
        modelSpinnerList = new ArrayList<>();
        modelSpinnerList.add("Model");

        pincode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @SuppressLint("SetTextI18n")
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                statecapital.setText("");
                citycapital.setText("");
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 6) {
                    strPincode = pincode.getText().toString();
                    PInCodeModel pInCodeModel = new PInCodeModel();
                    pInCodeModel.setJobType("get_areadetail");
                    pInCodeModel.setPincode(strPincode);
                    progressBar.setVisibility(View.VISIBLE);
                    Controller.getStateCity(AddWarantyActivity.this, pInCodeModel, CitystateListner);
                }
            }
        });


        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                register.setEnabled(false);
                strContactNo = contact_no.getText().toString();
                strUniqueNo = qrcode.getText().toString();
                strCustomerName = customer_name.getText().toString();
                strdateofpurchase = dateofpurchase.getText().toString();
                strPincode = pincode.getText().toString();
                strremarks = Remarks.getText().toString();

                if (strUniqueNo.equals("")) {
                    final Snackbar snackbar;
                    snackbar = Snackbar.make(qrcode, "Please Enter Unique No.", Snackbar.LENGTH_SHORT);
                    View snackBarView = snackbar.getView();
                    snackBarView.layout(20, 20, 20, 20);
                    snackBarView.setBackgroundColor(Color.RED);
                    TextView textView = (TextView) snackBarView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);
                    snackBarView.setMinimumHeight(103);
                    textView.setGravity(Gravity.CENTER);
                    snackbar.setActionTextColor(Color.WHITE);
                    snackbar.show();
                } else if (strContactNo.length() != 10) {
                    final Snackbar snackbar;
                    snackbar = Snackbar.make(contact_no, "Please Check Contact No.", Snackbar.LENGTH_SHORT);
                    View snackBarView = snackbar.getView();
                    snackBarView.layout(20, 20, 20, 20);
                    snackBarView.setBackgroundColor(Color.RED);
                    TextView textView = (TextView) snackBarView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);
                    snackBarView.setMinimumHeight(103);
                    textView.setGravity(Gravity.CENTER);
                    snackbar.setActionTextColor(Color.WHITE);
                    snackbar.show();
                } else if (strCustomerName.equals("")) {
                    final Snackbar snackbar;
                    snackbar = Snackbar.make(customer_name, "Please Enter Customer name", Snackbar.LENGTH_SHORT);
                    View snackBarView = snackbar.getView();
                    snackBarView.layout(20, 20, 20, 20);
                    snackBarView.setBackgroundColor(Color.RED);
                    TextView textView = (TextView) snackBarView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);
                    snackBarView.setMinimumHeight(103);
                    textView.setGravity(Gravity.CENTER);
                    snackbar.setActionTextColor(Color.WHITE);
                    snackbar.show();
                } else if (strPincode.equals("") || strPincode.length() != 6) {
                    final Snackbar snackbar;
                    snackbar = Snackbar.make(pincode, "Please Enter Pin Code", Snackbar.LENGTH_SHORT);
                    View snackBarView = snackbar.getView();
                    snackBarView.layout(20, 20, 20, 20);
                    snackBarView.setBackgroundColor(Color.RED);
                    TextView textView = (TextView) snackBarView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);
                    snackBarView.setMinimumHeight(103);
                    textView.setGravity(Gravity.CENTER);
                    snackbar.setActionTextColor(Color.WHITE);
                    snackbar.show();

                } else {
                    Intent intent = new Intent(AddWarantyActivity.this, ConfirmWarrantyActivity.class);
                    intent.putExtra("qrCode", qrcode.getText().toString().toUpperCase());
                    intent.putExtra("customer_name", customer_name.getText().toString());
                    intent.putExtra("vehicle_no", vehicle_no.getText().toString());
                    intent.putExtra("contact_no", strContactNo);
                    intent.putExtra("segment_id", strSegmentId);
                    intent.putExtra("pincode", strPincode);
                    intent.putExtra("state", state);
                    intent.putExtra("city", city);
                    intent.putExtra("remarks", strremarks);
                    intent.putExtra("vehicle_manufacturer_id", strManufactureId);
                    intent.putExtra("vehicle_model_id", strManufactureId);
                    intent.putExtra("image_path", image_path);
                    startActivity(intent);
                    finish();
                }
                register.setEnabled(true);

            }
        });
        view_Previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddWarantyActivity.this, ProductDetailActivity.class);
                intent.putExtra("warranty_tab", "warranty");
                intent.putExtra("tab", 1);
                startActivity(intent);
                finish();
            }
        });
        scanner_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddWarantyActivity.this, ScannerActivity.class);
                intent.putExtra("AddWarranty", "NewWarranty");
                startActivity(intent);
                finish();
            }
        });

        segmentList = new ArrayList<SegmentModel>();
        //  Controller.GetSegment(AddWarantyActivity.this, segmentListener);
//        progressBar.setVisibility(View.VISIBLE);

        manufacturerlList = new ArrayList<CarManufacturerModel>();
        modelsList = new ArrayList<ModelOfCarModel>();

        upload_warranty_card.setOnClickListener(uploadClick);
        warranty_card_image.setOnClickListener(uploadClick);
        manufacturer_spinner.setEnabled(false);
        model_spinner.setEnabled(false);


        ArrayAdapter<String> vehicleManufactureSpinnerArrayAdapter = new ArrayAdapter<String>(AddWarantyActivity.this, R.layout.spinner_items, manufacureSpinnerList);
        vehicleManufactureSpinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_drop_down);
        manufacturer_spinner.setAdapter(vehicleManufactureSpinnerArrayAdapter);
        manufacturer_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    manufacturer_spinner.setSelected(false);
                    strManufactureId = "";
                } else {
                    strManufactureId = String.valueOf(manufacturerlList.get(position - 1).getId());
                    model_spinner.setEnabled(true);
                    Log.d("ManufactureId", "" + manufacturerlList.get(position - 1).getId());
                    vehicle_manufacturer_id = manufacturerlList.get(position - 1).getId();
                    //   Controller.GetCarModel(AddWarantyActivity.this, segment_id, vehicle_manufacturer_id, CarModelListener);
                    //  progressBar.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        ArrayAdapter<String> vehicleModeeSpinnerArrayAdapter = new ArrayAdapter<String>(AddWarantyActivity.this, R.layout.spinner_items, modelSpinnerList);
        vehicleManufactureSpinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_drop_down);
        model_spinner.setAdapter(vehicleModeeSpinnerArrayAdapter);
        model_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    model_spinner.setSelected(false);
                    strModelId = "";
                } else {
                    strModelId = String.valueOf(modelsList.get(position - 1).getId());
                    Log.d("ModelID", "" + modelsList.get(position - 1).getId());
                    vehicle_model_id = modelsList.get(position - 1).getId();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private Bitmap showCroppedImage(String imagePath) {
        if (imagePath != null) {
            Bitmap myBitmap = BitmapFactory.decodeFile(imagePath);
            return myBitmap;

        }
        return null;
    }

    public boolean hasLocationPermissionGranted() {
        return ContextCompat.checkSelfPermission(AddWarantyActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(AddWarantyActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    private void showAddProfilePicDialog1() {
        PicModeSelectDialogFragment dialogFragment = new PicModeSelectDialogFragment();
        dialogFragment.setiPicModeSelectListener(new PicModeSelectDialogFragment.IPicModeSelectListener() {
            @Override
            public void onPicModeSelected(String mode) {
                String action = mode.equalsIgnoreCase(GOTOConstants.PicModes.CAMERA) ? GOTOConstants.IntentExtras.ACTION_CAMERA : GOTOConstants.IntentExtras.ACTION_GALLERY;
                Intent intent = new Intent(AddWarantyActivity.this, ImageCropActivity.class);
                intent.putExtra("ACTION", action);
                startActivityForResult(intent, SELECT_PICTURE);
            }
        });
        dialogFragment.show(getFragmentManager(), "picModeSelector");
    }

    public void requestLocationPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            ActivityCompat.requestPermissions(AddWarantyActivity.this, PERMISSIONS_LOCATION,
                    REQUEST_LOCATION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_LOCATION:
                if (grantResults.length > 0) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        //Storage permission is enabled
                        showAddProfilePicDialog1();

                    } else if (ActivityCompat.shouldShowRequestPermissionRationale(AddWarantyActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) && ActivityCompat.shouldShowRequestPermissionRationale(AddWarantyActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) && ActivityCompat.shouldShowRequestPermissionRationale(AddWarantyActivity.this, Manifest.permission.CAMERA)) {
                        //User has deny from permission dialog
                        final AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(AddWarantyActivity.this, R.style.AppCompatAlertDialogStyle);
                        alertDialog1.setTitle("Reading Permission Denied");
                        alertDialog1.setMessage("Are you sure you want to deny this permission?");
                        alertDialog1.setPositiveButton("I'M SURE", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                requestLocationPermission();
                            }
                        });
                        alertDialog1.setNegativeButton("RETRY", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                requestLocationPermission();
                            }
                        });
                        alertDialog1.show();
                    } else {
                        // User has deny permission and checked never show permission dialog so you can redirect to Application settings page
                        AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(AddWarantyActivity.this, R.style.AppCompatAlertDialogStyle);
                        alertDialog1.setMessage("It looks like you have turned off permission required for this feature. It can be enabled under Phone Settings > Apps > Saathi > Permissions");
                        alertDialog1.setPositiveButton("GO TO SETTINGS", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent();
                                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package", getPackageName(), null);
                                intent.setData(uri);
                                startActivity(intent);
                            }
                        });
                        alertDialog1.show();
                    }
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SELECT_PICTURE) {
            if (resultCode == RESULT_OK) {
                image_path = data.getStringExtra(GOTOConstants.IntentExtras.IMAGE_PATH);
                progressBar.setVisibility(View.GONE);
                upload_warranty_card.setVisibility(View.GONE);
                warranty_card_image.setVisibility(View.VISIBLE);
                warranty_card_image.setImageBitmap(showCroppedImage(image_path));

            } else if (resultCode == RESULT_CANCELED) {
                //TODO : Handle case
            } else {
                String errorMsg = data.getStringExtra(ImageCropActivity.ERROR_MSG);
                Toast.makeText(AddWarantyActivity.this, errorMsg, Toast.LENGTH_LONG).show();
            }
        }
    }

}
