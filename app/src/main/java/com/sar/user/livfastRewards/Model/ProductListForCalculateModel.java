package com.sar.user.livfastRewards.Model;

/**
 * Created by Nav on 19-Aug-17.
 */

public class ProductListForCalculateModel {
    String id;
    String quantity;
    String product;

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
}
