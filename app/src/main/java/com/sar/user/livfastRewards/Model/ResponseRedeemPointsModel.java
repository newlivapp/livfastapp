package com.sar.user.livfastRewards.Model;

/**
 * Created by Nav on 08-Aug-17.
 */

public class ResponseRedeemPointsModel {
    int id, user, loyalty_point, bonus;
    String dealer_product, created_timestamp;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public int getLoyalty_point() {
        return loyalty_point;
    }

    public void setLoyalty_point(int loyalty_point) {
        this.loyalty_point = loyalty_point;
    }

    public String getDealer_product() {
        return dealer_product;
    }

    public void setDealer_product(String dealer_product) {
        this.dealer_product = dealer_product;
    }

    public String getCreated_timestamp() {
        return created_timestamp;
    }

    public void setCreated_timestamp(String created_timestamp) {
        this.created_timestamp = created_timestamp;
    }

    public int getBonus() {
        return bonus;
    }

    public void setBonus(int bonus) {
        this.bonus = bonus;
    }
}
