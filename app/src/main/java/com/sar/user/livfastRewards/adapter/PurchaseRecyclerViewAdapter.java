package com.sar.user.livfastRewards.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sar.user.livfastRewards.Model.DealeProductModel;
import com.sar.user.livfastRewards.Model.PurchaseProductModel;
import com.sar.user.livfastRewards.R;
import com.sar.user.livfastRewards.helper.MyUtils;

import java.util.List;

/**
 * Created by user on 02-04-2017.
 */

public class PurchaseRecyclerViewAdapter extends RecyclerView.Adapter<PurchaseRecyclerViewAdapter.MyViewHolder> {
    Context context;
    List<PurchaseProductModel> productList;

    public PurchaseRecyclerViewAdapter(Context context1, List<PurchaseProductModel> productList1) {
        context = context1;
        productList = productList1;
    }

    @Override
    public PurchaseRecyclerViewAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.purchase_detail, parent, false);
        MyViewHolder mvh = new MyViewHolder(v);
        return mvh;
    }

    @Override
    public void onBindViewHolder(final PurchaseRecyclerViewAdapter.MyViewHolder holder, int position) {
        String date = productList.get(position).getCreated_timestamp();
        holder.date.setText(MyUtils.getValidDateFormat(date));
        holder.points.setText("" + productList.get(position).getLoyalty_point());
        DealeProductModel dealeProductModel = productList.get(position).getModel_number();
        if (productList.get(position).getModel_number() != null) {
            holder.product_detail.setText(dealeProductModel.getModel_number().toUpperCase());
        }

    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView date, product_detail, points;

        public MyViewHolder(View itemView) {
            super(itemView);
            date = (TextView) itemView.findViewById(R.id.date);
            product_detail = (TextView) itemView.findViewById(R.id.product_name);
            points = (TextView) itemView.findViewById(R.id.points);

        }
    }
}
