package com.sar.user.livfastRewards.Model;

/**
 * Created by user on 11-04-2017.
 */

public class CarManufacturerModel {
    int id, car_segment;
    String vehicle_manufacturer;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCar_segment() {
        return car_segment;
    }

    public void setCar_segment(int car_segment) {
        this.car_segment = car_segment;
    }

    public String getVehicle_manufacturer() {
        return vehicle_manufacturer;
    }

    public void setVehicle_manufacturer(String vehicle_manufacturer) {
        this.vehicle_manufacturer = vehicle_manufacturer;
    }
}
