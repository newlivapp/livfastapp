package com.sar.user.livfastRewards.Model;

import java.math.BigInteger;

/**
 * Created by i5tagbin2 on 4/9/17.
 */

public class DataModel {
    int id;
    float total_loyalty;
    int total_loyalty_p;
    BigInteger phone;
    String first_name;
    String last_name;
    String username;
    String email;
    String address;
    String dealership_name;
    String distributor_name;
    String distributor_code;
    String verification_status;
    String small_image;
    String large_image;
    String city;
    String state;
    String pincode;
    String salespersonname;
    String salespersoncode;
    String user_id;

    public int getTotal_loyalty_p() {
        return total_loyalty_p;
    }

    public void setTotal_loyalty_p(int total_loyalty_p) {
        this.total_loyalty_p = total_loyalty_p;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BigInteger getPhone() {
        return phone;
    }

    public void setPhone(BigInteger phone) {
        this.phone = phone;
    }

    public float getTotal_loyalty() {
        return total_loyalty;
    }

    public void setTotal_loyalty(float total_loyalty) {
        this.total_loyalty = total_loyalty;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDealership_name() {
        return dealership_name;
    }

    public void setDealership_name(String dealership_name) {
        this.dealership_name = dealership_name;
    }

    public String getVerification_status() {
        return verification_status;
    }

    public void setVerification_status(String verification_status) {
        this.verification_status = verification_status;
    }

    public String getSmall_image() {
        return small_image;
    }

    public void setSmall_image(String small_image) {
        this.small_image = small_image;
    }

    public String getLarge_image() {
        return large_image;
    }

    public void setLarge_image(String large_image) {
        this.large_image = large_image;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getSalespersoncode() {
        return salespersoncode;
    }

    public void setSalespersoncode(String salespersoncode) {
        this.salespersoncode = salespersoncode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getSalespersonname() {
        return salespersonname;
    }

    public void setSalespersonname(String salespersonname) {
        this.salespersonname = salespersonname;
    }

    public String getDistributor_name() {
        return distributor_name;
    }

    public void setDistributor_name(String distributor_name) {
        this.distributor_name = distributor_name;
    }

    public String getDistributor_code() {
        return distributor_code;
    }

    public void setDistributor_code(String distributor_code) {
        this.distributor_code = distributor_code;
    }

}
