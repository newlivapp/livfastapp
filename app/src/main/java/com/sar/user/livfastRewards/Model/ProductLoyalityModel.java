package com.sar.user.livfastRewards.Model;

import android.content.ContentValues;
import android.database.Cursor;

import com.sar.user.livfastRewards.Database.AbstractEntry;

/**
 * Created by Nav on 02-Aug-17.
 */

public class ProductLoyalityModel extends AbstractEntry {
    public static final String TABLE_NAME = "battery_filter";
    public static final String ID = "id";
    public static final String BRAND = "brand";
    public static final String PRODUCT = "product";
    public static final String WARRANTY = "warranty";
    public static final String CAPACITY = "capacity";
    public static final String QUANTITY = "quantity";

    int id, quantity;
    String brand, product, warranty, capacity;

    public ProductLoyalityModel(int id, String brand, String product, String warranty, String capacity, int quantity) {
        this.id = id;
        this.brand = brand;
        this.product = product;
        this.warranty = warranty;
        this.capacity = capacity;
        this.quantity = quantity;
    }

    public static ProductLoyalityModel fromCursor(Cursor cursor) {
        return new ProductLoyalityModel(cursor.getInt(cursor.getColumnIndex(ID)),
                cursor.getString(cursor.getColumnIndex(BRAND)),
                cursor.getString(cursor.getColumnIndex(PRODUCT)),
                cursor.getString(cursor.getColumnIndex(WARRANTY)),
                cursor.getString(cursor.getColumnIndex(CAPACITY)),
                cursor.getInt(cursor.getColumnIndex(QUANTITY)));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getWarranty() {
        return warranty;
    }

    public void setWarranty(String warranty) {
        this.warranty = warranty;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public ContentValues getContentValues() {
        ContentValues values = new ContentValues();
        values.put(ID, getId());
        values.put(BRAND, getBrand());
        values.put(PRODUCT, getProduct());
        values.put(WARRANTY, getWarranty());
        values.put(CAPACITY, getCapacity());
        values.put(QUANTITY, getQuantity());
        return values;
    }
}
