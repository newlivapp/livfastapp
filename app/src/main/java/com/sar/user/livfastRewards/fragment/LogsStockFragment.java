package com.sar.user.livfastRewards.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sar.user.livfastRewards.Interface.RequestListener;
import com.sar.user.livfastRewards.Model.GetStockModel;
import com.sar.user.livfastRewards.R;
import com.sar.user.livfastRewards.adapter.StockRVAdapter;
import com.sar.user.livfastRewards.helper.Controller;
import com.sar.user.livfastRewards.helper.PrefManager;

import org.json.JSONException;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Nav on 24-Jul-17.
 */

public class LogsStockFragment extends BaseFragment {
    final DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    final Date date = new Date();
    TextView current_date;
    RecyclerView stock_rv;
    StockRVAdapter stockRVAdapter;
    List<GetStockModel> stockList;
    String todaydate = dateFormat.format(date);
    SwipeRefreshLayout swipeRefreshLayout;
    RelativeLayout empty_rl;
    String user_id;
    RequestListener stockListner = new RequestListener() {
        @Override
        public void onRequestStarted() {
        }

        @Override
        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
            Log.d("response GetStock", responseObject.toString());
            stockList.clear();
            Type collectionType = new TypeToken<List<GetStockModel>>() {
            }.getType();
            List<GetStockModel> ca = (List<GetStockModel>) new Gson().fromJson(responseObject.toString(), collectionType);
            if (ca != null && ca.size() > 0)
                stockList.addAll(ca);
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (stockList.isEmpty()) {
                        empty_rl.setVisibility(View.VISIBLE);
                        stock_rv.setVisibility(View.GONE);
                    } else {
                        empty_rl.setVisibility(View.GONE);
                        stock_rv.setVisibility(View.VISIBLE);
                    }
                    swipeRefreshLayout.setRefreshing(false);
                    stockRVAdapter.notifyDataSetChanged();
                }
            });
        }

        @Override
        public void onRequestError(int errorCode, String message) {

            if (getActivity() != null)
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefreshLayout.setRefreshing(false);

                    }
                });

            handelError(errorCode, message);
        }
    };

    @SuppressLint("ValidFragment")
    public LogsStockFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragmnet_logs_stock, container, false);
    }

    @SuppressLint({"SetTextI18n", "NewApi"})
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        stock_rv = (RecyclerView) getView().findViewById(R.id.stock_rv);
        swipeRefreshLayout = (SwipeRefreshLayout) getView().findViewById(R.id.swiperefresh);
        empty_rl = (RelativeLayout) getView().findViewById(R.id.empty_rl);
        user_id = PrefManager.getInstance(getContext()).getÜserId();
        stockList = new ArrayList<GetStockModel>();
        Controller.GetStock(getContext(), user_id, stockListner);
        stockRVAdapter = new StockRVAdapter(getContext(), stockList);
        stock_rv.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        stock_rv.setAdapter(stockRVAdapter);
        stock_rv.setHasFixedSize(true);
        current_date = (TextView) getView().findViewById(R.id.current_date);
        current_date.setText("Stock as on : " + todaydate);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                Controller.GetStock(getContext(), user_id, stockListner);
            }
        });

    }
}
