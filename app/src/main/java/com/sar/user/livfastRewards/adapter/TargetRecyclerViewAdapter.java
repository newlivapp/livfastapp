package com.sar.user.livfastRewards.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.sar.user.livfastRewards.Model.TargetsModel;
import com.sar.user.livfastRewards.R;

import java.util.List;

public class TargetRecyclerViewAdapter extends RecyclerView.Adapter<TargetRecyclerViewAdapter.MyViewHolder> {
    Context context;
    List<TargetsModel> targetsList;

    public TargetRecyclerViewAdapter(Context context1, List<TargetsModel> targetsList1) {
        context = context1;
        targetsList = targetsList1;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.target_detail, parent, false);
        MyViewHolder mvh = new MyViewHolder(v);
        return mvh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        ImageLoader.getInstance().loadImage(targetsList.get(position).getLarge_image(), new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {

            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                holder.target_image.setImageBitmap(loadedImage);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {

            }
        });

    }

    @Override
    public int getItemCount() {
        return targetsList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView target_image;

        public MyViewHolder(View itemView) {
            super(itemView);
            target_image = (ImageView) itemView.findViewById(R.id.target_image);
        }
    }
}
