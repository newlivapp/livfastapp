package com.sar.user.livfastRewards.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sar.user.livfastRewards.Interface.RequestListener;
import com.sar.user.livfastRewards.Model.GetCouponAPIModel;
import com.sar.user.livfastRewards.Model.ScratchCardModel;
import com.sar.user.livfastRewards.R;
import com.sar.user.livfastRewards.adapter.ScratchAdapter;
import com.sar.user.livfastRewards.helper.Controller;
import com.sar.user.livfastRewards.helper.PrefManager;
import com.sar.user.livfastRewards.listener.RecyclerItemClickListener;

import org.json.JSONException;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class ScratchListActivity extends BaseActivity {

    RecyclerView recyclerView;
    GridLayoutManager gridLayoutManager;
    ScratchAdapter scratchAdapter;
    ArrayList<ScratchCardModel> scratchCardModels = new ArrayList<>();
    RelativeLayout rlNoData;
    ImageView back_button;
    Toolbar toolbar;
    TextView toolbar_title;
    ProgressBar progressbar;
    RequestListener couponAPIRequestListener = new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @Override
        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
            Log.e("response getCouponAPI", responseObject.toString());
            scratchCardModels.clear();
            Type collectionType = new TypeToken<List<ScratchCardModel>>() {
            }.getType();
            try {
                List<ScratchCardModel> ca = (List<ScratchCardModel>) new Gson().fromJson(responseObject.toString(), collectionType);
                scratchCardModels.addAll(ca);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (scratchCardModels == null || scratchCardModels.isEmpty()) {
                //Show No records sreen
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressbar.setVisibility(View.GONE);
                        rlNoData.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                    }
                });
                handelError(449, responseObject.toString());
                return;
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressbar.setVisibility(View.GONE);
                    rlNoData.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);

                    if (scratchAdapter == null)
                        setupRecyclerView();
                    else
                        scratchAdapter.notifyDataSetChanged();
                }
            });
        }

        @Override
        public void onRequestError(int errorCode, final String message) {
            handelError(errorCode, message);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressbar.setVisibility(View.GONE);
                    rlNoData.setVisibility(View.GONE);
                }
            });
        }
    };
    View.OnClickListener backClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scratch_list);

        initControl();
        setupRecyclerView();

        //all setup now you can set click and lisner here
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        // do whatever
                        ScratchCardModel scratchCardModel = scratchCardModels.get(position);

                        //check for already scratch
                        if (scratchCardModel.getRedeem_status().equals("sratch")) {
                            return;
                        }

                        //Date Validation
                        try {
                            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                            SimpleDateFormat dateOnlySimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                            Date scratchDate = dateOnlySimpleDateFormat.parse(dateOnlySimpleDateFormat.format(simpleDateFormat.parse(scratchCardModel.getCoupon_scratch_date())));
                            Date currentDate = dateOnlySimpleDateFormat.parse(dateOnlySimpleDateFormat.format(simpleDateFormat.parse(scratchCardModel.getCurrent_date())));
                            if (currentDate.getTime() < scratchDate.getTime()) {
                                return;
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                            return;
                        }

                        //Coupon text check it is null no need to go Ahead
                        if (scratchCardModel.getCoupon_text() == null || scratchCardModel.getCoupon_text().isEmpty())
                            return;

                        Intent intent = new Intent(ScratchListActivity.this, ScratchCardsActivity.class);
                        intent.putExtra("scratch_model", scratchCardModel);
                        startActivity(intent);
                    }
                })
        );
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Get Coupen Data
        GetCouponAPIModel getCouponAPIModel = new GetCouponAPIModel();
        getCouponAPIModel.setUser_id(PrefManager.getInstance(this).getÜserId());
        progressbar.setVisibility(View.VISIBLE);
        Controller.getCouponAPI(this, getCouponAPIModel, couponAPIRequestListener);
    }

    @SuppressLint("NewApi")
    private void initControl() {
        recyclerView = findViewById(R.id.recyclerView);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        back_button = (ImageView) toolbar.findViewById(R.id.back_button);
        back_button.setOnClickListener(backClick);
        toolbar_title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        if (!PrefManager.getInstance(ScratchListActivity.this).getDealershipName().isEmpty()) {
            toolbar_title.setText(PrefManager.getInstance(ScratchListActivity.this).getDealershipName() + " - Coupon Wallet");
        }

        rlNoData = findViewById(R.id.rlNoData);
        progressbar = findViewById(R.id.progressbar);
        progressbar.setVisibility(View.GONE);
    }

    private void setupRecyclerView() {
        gridLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(gridLayoutManager);
        scratchAdapter = new ScratchAdapter(this, scratchCardModels);
        recyclerView.setAdapter(scratchAdapter);
        recyclerView.invalidate();
    }
}
