package com.sar.user.livfastRewards.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.sar.user.livfastRewards.Interface.RequestListener;
import com.sar.user.livfastRewards.Model.DataModel;
import com.sar.user.livfastRewards.Model.EditProfileModel;
import com.sar.user.livfastRewards.Model.FileUploadModel;
import com.sar.user.livfastRewards.Model.LoginResponseModel;
import com.sar.user.livfastRewards.R;
import com.sar.user.livfastRewards.fragment.PicModeSelectDialogFragment;
import com.sar.user.livfastRewards.helper.Controller;
import com.sar.user.livfastRewards.helper.JsonUtils;
import com.sar.user.livfastRewards.helper.PrefManager;
import com.sar.user.livfastRewards.helper.ProfileImage.GOTOConstants;
import com.sar.user.livfastRewards.helper.ProfileImage.ImageCropActivity;

import org.json.JSONException;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.ParseException;

import de.hdodenhof.circleimageview.CircleImageView;

public class EditProfileActivity extends BaseActivity {
    private static int SELECT_PICTURE = 1;
    private static String[] PERMISSIONS_LOCATION = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
    final int REQUEST_LOCATION = 2;
    Button button_save;
    EditText customer_name, contact_no, email_id;
    ProgressBar progressBar;
    CircleImageView profile_pic;
    ImageView edit_profile_pic, back_button;
    Toolbar toolbar;
    String phone_no;

    PrefManager prefManager;

    String user_id,imageUrl;
    RequestListener mUploadListener = new RequestListener() {
        @Override
        public void onRequestStarted() {
        }

        @Override
        public void onRequestCompleted(Object responseObject) {
            Log.d("response", responseObject.toString());
           // imageUrl = responseObject.toString();
            DataModel dataModel = JsonUtils.objectify(responseObject.toString(), DataModel.class);
            //dataModel.setSmall_image(PrefManager.getInstance(EditProfileActivity.this).getSmallImage());
            PrefManager.getInstance(EditProfileActivity.this).setUserData(dataModel);


            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                }
            });

        }

        @Override
        public void onRequestError(int errorCode, String message) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                }
            });
            handelError(errorCode, message);
        }
    };
    RequestListener editProfileListner = new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @Override
        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
            Log.d("response", responseObject.toString());
            if (phone_no.equals(contact_no.getText().toString())) {
                DataModel profileModel = JsonUtils.objectify(responseObject.toString(), DataModel.class);

                PrefManager.getInstance(EditProfileActivity.this).setUserData(profileModel);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(EditProfileActivity.this, "your profile is updated Successfully", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                });
            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(EditProfileActivity.this, "OTP is send successfully", Toast.LENGTH_SHORT).show();
                        final Dialog dialog = new Dialog(EditProfileActivity.this);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.otp_massage_popup);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                        final TextView msgText = (TextView) dialog.findViewById(R.id.msg_text);
                        final ImageView dismiss = (ImageView) dialog.findViewById(R.id.dissmiss);
                        final TextView phone_no_msg = (TextView) dialog.findViewById(R.id.phone_no_massage);
                        final EditText otp_text = (EditText) dialog.findViewById(R.id.otp_text);
                        final TextView resendOtp_tv = (TextView) dialog.findViewById(R.id.resend_otp_tv);
                        SpannableString content = new SpannableString("RE-Send OTP");
                        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
                        resendOtp_tv.setText(content);
                        resendOtp_tv.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                EditProfileModel editProfileModel = new EditProfileModel();
                                editProfileModel.setPhone(contact_no.getText().toString());
//                            Controller.profileDetailUpadate(EditProfileActivity.this, editProfileModel, new RequestListener() {
//                                @Override
//                                public void onRequestStarted() {
//
//                                }
//
//                                @Override
//                                public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
//                                    Log.d("response",responseObject.toString());
//                                    runOnUiThread(new Runnable() {
//                                        @Override
//                                        public void run() {
//                                            Toast.makeText(EditProfileActivity.this,"OTP sent successfully",Toast.LENGTH_SHORT).show();
//                                        }
//                                    });
//                                }
//
//                                @Override
//                                public void onRequestError(int errorCode, String message) {
//                                    Log.d("response",message);
//                                    if (errorCode >= 400 && errorCode < 500) {
//                                        if (errorCode == 403){
//                                            runOnUiThread(new Runnable() {
//                                                @Override
//                                                public void run() {
//                                                    progressBar.setVisibility(View.GONE);
//                                                    Toast.makeText(EditProfileActivity.this, "UnAuthorised!", Toast.LENGTH_SHORT).show();
//                                                }
//                                            });
//                                        }
//                                        else if(errorCode==401) {
//
//                                            runOnUiThread(new Runnable() {
//                                                @Override
//                                                public void run() {
//                                                    progressBar.setVisibility(View.GONE);
////                                           logout();
//                                                }
//                                            });
//                                        }
//                                        else {
//                                            final ErrorResponseModel errorResponseModel = JsonUtils.objectify(message, ErrorResponseModel.class);
//                                            runOnUiThread(new Runnable() {
//                                                @Override
//                                                public void run() {
//                                                    progressBar.setVisibility(View.GONE);
//                                                    Toast.makeText(EditProfileActivity.this, errorResponseModel.getErr(), Toast.LENGTH_SHORT).show();
//                                                }
//                                            });
//                                        }
//                                    } else {
//                                        runOnUiThread(new Runnable() {
//                                            @Override
//                                            public void run() {
//                                                progressBar.setVisibility(View.GONE);
//                                                Toast.makeText(EditProfileActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
//                                            }
//                                        });
//                                    }
//                                }
//                            });
                            }
                        });
                        phone_no_msg.setText("Enter OTP send to +91" + PrefManager.getInstance(EditProfileActivity.this).getPhoneNo());
                        msgText.setText("Once you confirm the OTP, we will update" + "\n" + "your new no. to your account");
                        final Button verify = (Button) dialog.findViewById(R.id.verify_button);
                        verify.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                EditProfileModel editProfileModel = new EditProfileModel();
                                editProfileModel.setFirst_name(customer_name.getText().toString());
                                editProfileModel.setEmail(email_id.getText().toString());
//                        int phone_no = Integer.parseInt(contact_no.getText().toString());
                                editProfileModel.setPhone(contact_no.getText().toString());
                                editProfileModel.setVerification_otp(otp_text.getText().toString());
                                //  Controller.profileDetailUpadate(EditProfileActivity.this, editProfileModel, editProfilewithotpListner);
                            }
                        });
                        dismiss.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });
                        if (EditProfileActivity.this != null)
                            EditProfileActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (!(EditProfileActivity.this).isFinishing()) {
                                        try {
                                            dialog.show();
                                            dialog.setCancelable(false);
                                        } catch (WindowManager.BadTokenException e) {
                                            Log.e("WindowManagerBad ", e.toString());
                                        } catch (Exception e) {
                                            Log.e("Exception ", e.toString());
                                        }
                                    }
                                }
                            });
                    }
                });


            }

        }

        @Override
        public void onRequestError(int errorCode, String message) {
            handelError(errorCode, message);
        }
    };
    RequestListener editProfilewithotpListner = new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @Override
        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
            Log.d("response", responseObject.toString());
            DataModel profileModel = JsonUtils.objectify(responseObject.toString(), DataModel.class);
            PrefManager.getInstance(EditProfileActivity.this).setUserData(profileModel);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(EditProfileActivity.this, "Profile updated successfully", Toast.LENGTH_SHORT).show();
                    finish();
                }
            });
        }

        @Override
        public void onRequestError(int errorCode, String message) {
            handelError(errorCode, message);
        }
    };
    View.OnClickListener backClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        back_button = (ImageView) toolbar.findViewById(R.id.back_button);
        button_save = (Button) findViewById(R.id.save_button);
        customer_name = (EditText) findViewById(R.id.customer_name);
        contact_no = (EditText) findViewById(R.id.contact_no);
        email_id = (EditText) findViewById(R.id.email_id);
        progressBar = (ProgressBar) findViewById(R.id.progressbar);
        profile_pic = (CircleImageView) findViewById(R.id.ivProfilePicmain);
        edit_profile_pic = (ImageView) findViewById(R.id.ivProfileChange);

        prefManager = PrefManager.getInstance(this);
        user_id = String.valueOf(prefManager.getÜserId());

        edit_profile_pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (hasLocationPermissionGranted())
                    showAddProfilePicDialog1();
                else
                    requestLocationPermission();
            }
        });

        phone_no = String.valueOf(prefManager.getPhoneNo());
        customer_name.setText(prefManager.getDealershipName());
        customer_name.setSelection(customer_name.getText().length());
        contact_no.setText("" + prefManager.getPhoneNo());
        contact_no.setSelection(contact_no.getText().length());
        email_id.setText(prefManager.getEmail());
        email_id.setSelection(email_id.getText().length());

        /*ImageLoader.getInstance().loadImage(prefManager.getSmallImage(), new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                profile_pic.setImageBitmap(loadedImage);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
            }
        });*/

        Glide
                .with(this)
                .load(prefManager.getSmallImage())
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .centerCrop()
                .into(profile_pic);

        back_button.setOnClickListener(backClick);


        button_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (phone_no.equals(contact_no.getText().toString())) {
                    EditProfileModel editProfileModel = new EditProfileModel();
                    editProfileModel.setFirst_name(customer_name.getText().toString());
                    editProfileModel.setEmail(email_id.getText().toString());
                    editProfileModel.setUser_id(user_id);
                    //image
                    editProfileModel.setProfile_pic(imageUrl);
                    Controller.profileDetailUpadate(EditProfileActivity.this,editProfileModel,editProfileListner);
                    progressBar.setVisibility(View.VISIBLE);

                } else {

                    EditProfileModel editProfileModel = new EditProfileModel();
                    editProfileModel.setFirst_name(customer_name.getText().toString());
                    editProfileModel.setEmail(email_id.getText().toString());
                    editProfileModel.setPhone(contact_no.getText().toString());
                    Controller.profileDetailUpadate(EditProfileActivity.this, editProfileModel, editProfileListner);


                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SELECT_PICTURE) {
            if (resultCode == RESULT_OK) {
                String imagePath = data.getStringExtra(GOTOConstants.IntentExtras.IMAGE_PATH);
                String imgBase64Path = getFileToByte(imagePath);
                FileUploadModel fileUploadModel = new FileUploadModel();
                fileUploadModel.setUser_id(user_id + "");
                fileUploadModel.setFile(imgBase64Path);
                //progressBar.setVisibility(View.VISIBLE);
                profile_pic.setImageBitmap(showCroppedImage(imagePath));

                //fileUploadModel.setFile(new File(imagePath));

                //fileUploadModel.setUser_id(user_id);
                Controller.profileUpload(EditProfileActivity.this, fileUploadModel, mUploadListener);
                //profile_pic.setImageBitmap(showCroppedImage(imagePath));

            } else if (resultCode == RESULT_CANCELED) {
                //TODO : Handle case
            } else {
                String errorMsg = data.getStringExtra(ImageCropActivity.ERROR_MSG);
                Toast.makeText(EditProfileActivity.this, errorMsg, Toast.LENGTH_LONG).show();
            }
        }
    }

    public static String getFileToByte(String filePath) {
        Bitmap bmp = null;
        ByteArrayOutputStream bos = null;
        byte[] bt = null;
        String encodeString = null;
        try {
            bmp = BitmapFactory.decodeFile(filePath);
            bos = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, bos);
            bt = bos.toByteArray();
            encodeString = Base64.encodeToString(bt, Base64.DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return encodeString;
    }

    private Bitmap showCroppedImage(String imagePath) {
        if (imagePath != null) {
            Bitmap myBitmap = BitmapFactory.decodeFile(imagePath);
            return myBitmap;

        }
        return null;
    }

    public boolean hasLocationPermissionGranted() {
        return ContextCompat.checkSelfPermission(EditProfileActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(EditProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(EditProfileActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
    }

    private void showAddProfilePicDialog1() {
        PicModeSelectDialogFragment dialogFragment = new PicModeSelectDialogFragment();
        dialogFragment.setiPicModeSelectListener(new PicModeSelectDialogFragment.IPicModeSelectListener() {
            @Override
            public void onPicModeSelected(String mode) {
                String action = mode.equalsIgnoreCase(GOTOConstants.PicModes.CAMERA) ? GOTOConstants.IntentExtras.ACTION_CAMERA : GOTOConstants.IntentExtras.ACTION_GALLERY;
                Intent intent = new Intent(EditProfileActivity.this, ImageCropActivity.class);
                intent.putExtra("ACTION", action);
                startActivityForResult(intent, SELECT_PICTURE);
            }
        });
        dialogFragment.show(getFragmentManager(), "picModeSelector");
    }

    public void requestLocationPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            ActivityCompat.requestPermissions(EditProfileActivity.this, PERMISSIONS_LOCATION,
                    REQUEST_LOCATION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_LOCATION:
                if (grantResults.length > 0) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        //Storage permission is enabled
                        showAddProfilePicDialog1();

                    } else if (ActivityCompat.shouldShowRequestPermissionRationale(EditProfileActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) && ActivityCompat.shouldShowRequestPermissionRationale(EditProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) && ActivityCompat.shouldShowRequestPermissionRationale(EditProfileActivity.this, Manifest.permission.CAMERA)) {
                        //User has deny from permission dialog
                        final AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(EditProfileActivity.this, R.style.AppCompatAlertDialogStyle);
                        alertDialog1.setTitle("Reading Permission Denied");
                        alertDialog1.setMessage("Are you sure you want to deny this permission?");
                        alertDialog1.setPositiveButton("I'M SURE", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                requestLocationPermission();
                            }
                        });
                        alertDialog1.setNegativeButton("RETRY", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                requestLocationPermission();
                            }
                        });
                        alertDialog1.show();
                    } else {
                        // User has deny permission and checked never show permission dialog so you can redirect to Application settings page
                        AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(EditProfileActivity.this, R.style.AppCompatAlertDialogStyle);
                        alertDialog1.setMessage("It looks like you have turned off permission required for this feature. It can be enabled under Phone Settings > Apps > Partner > Permissions");
                        alertDialog1.setPositiveButton("GO TO SETTINGS", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent();
                                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package", getPackageName(), null);
                                intent.setData(uri);
                                startActivity(intent);
                            }
                        });
                        alertDialog1.show();
                    }
                }
                break;
        }
    }

    private void logout() {
        PrefManager.getInstance(this).clearSession();
        Intent go = new Intent(this, LoginActivity.class);
        startActivity(go);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
