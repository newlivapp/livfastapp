package com.sar.user.livfastRewards.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.sar.user.livfastRewards.Interface.RequestListener;
import com.sar.user.livfastRewards.Model.APIErrorResponseModel;
import com.sar.user.livfastRewards.Model.ScratchCardModel;
import com.sar.user.livfastRewards.Model.ScratchStatusAPIModel;
import com.sar.user.livfastRewards.R;
import com.sar.user.livfastRewards.helper.Controller;
import com.sar.user.livfastRewards.helper.JsonUtils;
import com.sar.user.livfastRewards.scratchview.ScratchTextView;

import org.json.JSONException;

import java.text.ParseException;

public class ScratchCardsActivity extends BaseActivity {

    ScratchTextView scratchTextView;
    ImageView ivClose;

    ScratchCardModel scratchCardModel;
    ProgressBar progressbar;
    boolean isApiCall = true;
    RequestListener onScratchStatusAPI = new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @SuppressLint("LongLogTag")
        @Override
        public void onRequestCompleted(final Object responseObject) throws JSONException, ParseException {
            Log.e("response setCouponScratchStatus", responseObject.toString());
            runOnUiThread(new Runnable() {
                @SuppressLint("NewApi")
                @Override
                public void run() {
                    progressbar.setVisibility(View.GONE);
                    APIErrorResponseModel apiErrorResponseModel = JsonUtils.objectify(responseObject.toString(), APIErrorResponseModel.class);
                    if (apiErrorResponseModel != null && !apiErrorResponseModel.getMsg().isEmpty()) {
                        Toast.makeText(ScratchCardsActivity.this, apiErrorResponseModel.getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

        @Override
        public void onRequestError(int errorCode, String message) {
            handelError(errorCode, message);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressbar.setVisibility(View.GONE);

                }
            });
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scratch_cards);

        scratchCardModel = (ScratchCardModel) getIntent().getExtras().getSerializable("scratch_model");

        if (scratchCardModel == null)
            finish();

        scratchTextView = findViewById(R.id.scratchTextView);
        progressbar = findViewById(R.id.progressbar);
        scratchTextView.setText(scratchCardModel.getCoupon_text());
        ivClose = findViewById(R.id.ivClose);
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        scratchTextView.setRevealListener(new ScratchTextView.IRevealListener() {
            @Override
            public void onRevealed(ScratchTextView tv) {
                //on reveal
                Log.e("scratchTextView", "ScratchTextView Done");

            }

            @Override
            public void onRevealPercentChangedListener(ScratchTextView stv, float percent) {
                // on text percent reveal
                Log.e("scratchTextView", "ScratchTextView done : " + percent);

                if (isApiCall && percent >= 0.50) {
                    isApiCall = false;
                    //Call API
                    ScratchStatusAPIModel scratchStatusAPIModel = new ScratchStatusAPIModel();
                    scratchStatusAPIModel.setId(scratchCardModel.getId());
                    scratchStatusAPIModel.setStatus("sratch");
                    progressbar.setVisibility(View.VISIBLE);
                    Controller.setCouponScratchStatus(ScratchCardsActivity.this, scratchStatusAPIModel, onScratchStatusAPI);
                }
            }
        });
    }

}
