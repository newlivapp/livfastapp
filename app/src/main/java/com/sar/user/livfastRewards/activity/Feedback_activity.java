package com.sar.user.livfastRewards.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sar.user.livfastRewards.Interface.RequestListener;
import com.sar.user.livfastRewards.Model.PutFeedbackModel;
import com.sar.user.livfastRewards.R;
import com.sar.user.livfastRewards.helper.Controller;

import org.json.JSONException;

import java.text.ParseException;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;

public class Feedback_activity extends AppCompatActivity {
    Button accept_button, report_button, submit_button;
    RelativeLayout buttons_RL, complaits_RL;
    MaterialRatingBar customer_rating;
    Toolbar toolbar;
    ImageView back_button;
    EditText customer_name, contact_no, ticket_no, serial_no, status, complaint_Box;
    int userrating, id;
    String strCtaStatus;
    RequestListener feedbackListner = new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @SuppressLint("LongLogTag")
        @Override
        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
            Log.d("response AddServiceFeedback", responseObject.toString());
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    final Dialog dialog = new Dialog(Feedback_activity.this);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.popup_massage);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    final Button addmore = (Button) dialog.findViewById(R.id.addmore);
                    addmore.setVisibility(View.GONE);
                    final TextView close = (TextView) dialog.findViewById(R.id.close);
                    final TextView msg = (TextView) dialog.findViewById(R.id.bettarymsg);
                    msg.setText("Your Feedback is Submitted");
                    close.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            finish();
                        }
                    });
                    if (Feedback_activity.this != null)
                        Feedback_activity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (!(Feedback_activity.this).isFinishing()) {
                                    try {
                                        dialog.show();
                                    } catch (WindowManager.BadTokenException e) {
                                        Log.e("WindowManagerBad ", e.toString());
                                    } catch (Exception e) {
                                        Log.e("Exception ", e.toString());
                                    }
                                }
                            }
                        });
                }
            });
        }

        @Override
        public void onRequestError(int errorCode, String message) {

        }
    };
    View.OnClickListener acceptClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (userrating == 0) {
                final Snackbar snackbar;
                snackbar = Snackbar.make(customer_rating, "Please Rate the distributer", Snackbar.LENGTH_SHORT);
                View snackBarView = snackbar.getView();
                snackBarView.layout(20, 20, 20, 20);
                snackBarView.setBackgroundColor(Color.RED);
                TextView textView = (TextView) snackBarView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(Color.WHITE);
                snackBarView.setMinimumHeight(103);
                textView.setGravity(Gravity.CENTER);
                snackbar.setActionTextColor(Color.WHITE);
                snackbar.show();
            } else {
                PutFeedbackModel putFeedbackModel = new PutFeedbackModel();
                putFeedbackModel.setComment(complaint_Box.getText().toString());
                putFeedbackModel.setCta_status(strCtaStatus);
                putFeedbackModel.setRating(userrating);
                putFeedbackModel.setTicketID(String.valueOf(id));
                Controller.AddServiceFeedback(Feedback_activity.this, id, putFeedbackModel, feedbackListner);
            }
        }
    };
    View.OnClickListener reportClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (complaits_RL.getVisibility() == View.GONE) {
                complaits_RL.setVisibility(View.VISIBLE);
                accept_button.setBackgroundResource(R.drawable.opacity_green_button);
                accept_button.setClickable(false);
            } else {
                complaits_RL.setVisibility(View.GONE);
                accept_button.setBackgroundResource(R.drawable.green_rounded_button_background);
                accept_button.setClickable(true);
                complaint_Box.setText("");
            }

        }
    };
    View.OnClickListener backClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback_activity);
        customer_name = (EditText) findViewById(R.id.customer_name);
        buttons_RL = (RelativeLayout) findViewById(R.id.buttons_RL);
        complaits_RL = (RelativeLayout) findViewById(R.id.complaint_RL);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        back_button = (ImageView) toolbar.findViewById(R.id.back_button);
        back_button.setOnClickListener(backClick);
        customer_rating = (MaterialRatingBar) findViewById(R.id.customerrating);
        accept_button = (Button) findViewById(R.id.accept_button);
        report_button = (Button) findViewById(R.id.report_button);
        submit_button = (Button) findViewById(R.id.submit_button);
        contact_no = (EditText) findViewById(R.id.customer_phone);
        ticket_no = (EditText) findViewById(R.id.ticket_no);
        serial_no = (EditText) findViewById(R.id.Battery_serial_no);
        status = (EditText) findViewById(R.id.battery_status);
        complaint_Box = (EditText) findViewById(R.id.complaint_box);
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            Log.d("FeedbackResponce-----", bundle.getString("mobile") + "----" + bundle.getString("ticket") + "----" + bundle.getString("status") + "-----" + bundle.getString("serial") + bundle.getInt("id", 0));
            customer_name.setText(bundle.getString("name"));
            contact_no.setText(bundle.getString("mobile"));
            ticket_no.setText(bundle.getString("ticket"));
            status.setText(bundle.getString("status"));
            serial_no.setText(bundle.getString("serial"));
            strCtaStatus = bundle.getString("cta_status");
            id = bundle.getInt("id", 0);
        }
        accept_button.setOnClickListener(acceptClick);
        submit_button.setOnClickListener(acceptClick);

        report_button.setOnClickListener(reportClick);

        LayerDrawable stars = (LayerDrawable) customer_rating.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(Color.parseColor("#F2C94C"), PorterDuff.Mode.SRC_ATOP);
        customer_rating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                userrating = (int) ratingBar.getRating();
            }
        });
    }
}
