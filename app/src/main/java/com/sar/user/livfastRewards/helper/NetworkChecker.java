package com.sar.user.livfastRewards.helper;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.io.IOException;

/**
 * Created by user on 30-03-2017.
 */

public class NetworkChecker {

    public static boolean isNetworkConnected(Context mContext) {
        ConnectivityManager cm = (ConnectivityManager) mContext
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return ni != null;
    }

    public static boolean isOnline() {
        Runtime runtime = Runtime.getRuntime();
        try {
            java.lang.Process ipprocess = runtime.exec("system/bin/ping -c 1 8.8.8.8");
            int exitValue = ipprocess.waitFor();

            return (exitValue == 0);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }
}
