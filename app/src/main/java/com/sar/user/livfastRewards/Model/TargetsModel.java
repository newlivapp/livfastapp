package com.sar.user.livfastRewards.Model;

/**
 * Created by user on 04-04-2017.
 */

public class TargetsModel {
    int id, loyalty_point, offer_loyalty;
    String expiry_timestamp, offer_detail, small_image, large_image, created_timestamp;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLoyalty_point() {
        return loyalty_point;
    }

    public void setLoyalty_point(int loyalty_point) {
        this.loyalty_point = loyalty_point;
    }

    public int getOffer_loyalty() {
        return offer_loyalty;
    }

    public void setOffer_loyalty(int offer_loyalty) {
        this.offer_loyalty = offer_loyalty;
    }

    public String getExpiry_timestamp() {
        return expiry_timestamp;
    }

    public void setExpiry_timestamp(String expiry_timestamp) {
        this.expiry_timestamp = expiry_timestamp;
    }

    public String getOffer_detail() {
        return offer_detail;
    }

    public void setOffer_detail(String offer_detail) {
        this.offer_detail = offer_detail;
    }

    public String getSmall_image() {
        return small_image;
    }

    public void setSmall_image(String small_image) {
        this.small_image = small_image;
    }

    public String getLarge_image() {
        return large_image;
    }

    public void setLarge_image(String large_image) {
        this.large_image = large_image;
    }

    public String getCreated_timestamp() {
        return created_timestamp;
    }

    public void setCreated_timestamp(String created_timestamp) {
        this.created_timestamp = created_timestamp;
    }
}
