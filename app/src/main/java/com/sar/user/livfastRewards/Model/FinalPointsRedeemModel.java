package com.sar.user.livfastRewards.Model;

/**
 * Created by Nav on 08-Aug-17.
 */

public class FinalPointsRedeemModel {
    String id, otp;
    int resend_otp;
    String user_id, loyalty_point, username;

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getResend_otp() {
        return resend_otp;
    }

    public void setResend_otp(int resend_otp) {
        this.resend_otp = resend_otp;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getLoyalty_point() {
        return loyalty_point;
    }

    public void setLoyalty_point(String loyalty_point) {
        this.loyalty_point = loyalty_point;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
