package com.sar.user.livfastRewards.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sar.user.livfastRewards.Interface.CityItemSelectedListner;
import com.sar.user.livfastRewards.Interface.RequestListener;
import com.sar.user.livfastRewards.Model.ErrorResponseModel;
import com.sar.user.livfastRewards.Model.GetCityModel;
import com.sar.user.livfastRewards.R;
import com.sar.user.livfastRewards.adapter.CityRVAdapter;
import com.sar.user.livfastRewards.helper.Controller;
import com.sar.user.livfastRewards.helper.JsonUtils;

import org.json.JSONException;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class SearchCityActivity extends AppCompatActivity implements CityItemSelectedListner {
    EditText searchCity_tv;
    List<GetCityModel> cityList;
    CityRVAdapter cityRVAdapter;
    RecyclerView cityRv;
    Toolbar toolbar;
    ImageView back_button;
    String customerName, mobileno;
    View.OnClickListener backClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(SearchCityActivity.this, Service_request_activity.class);
            intent.putExtra("name", customerName);
            intent.putExtra("mobileno", mobileno);
            startActivity(intent);
            finish();
        }
    };
    RequestListener GetcityListner = new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @Override
        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
            Log.d("response GetCities", responseObject.toString());
            cityList.clear();
            Type list = new TypeToken<List<GetCityModel>>() {
            }.getType();
            final List<GetCityModel> cityModel = (List<GetCityModel>) new Gson().fromJson(responseObject.toString(), list);
            cityList.addAll(cityModel);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    cityRVAdapter.notifyDataSetChanged();
                }
            });
        }

        @Override
        public void onRequestError(int errorCode, String message) {
            Log.d("response", message);
            if (errorCode >= 400 && errorCode < 500) {
                if (errorCode == 403) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(SearchCityActivity.this, "UnAuthorised!", Toast.LENGTH_SHORT).show();
                        }
                    });
                } else if (errorCode == 401) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                        }
                    });
                } else {
                    final ErrorResponseModel errorResponseModel = JsonUtils.objectify(message, ErrorResponseModel.class);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(SearchCityActivity.this, errorResponseModel.getErr(), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
//                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(SearchCityActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_city);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        back_button = (ImageView) toolbar.findViewById(R.id.back_button);
        back_button.setOnClickListener(backClick);
        searchCity_tv = (EditText) findViewById(R.id.actUserCreatedSearch);
        cityRv = (RecyclerView) findViewById(R.id.city_rv);
        if (Build.VERSION.SDK_INT > 19) {
            searchCity_tv.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_search_black_24dp, 0, 0, 0);
        }
        if (getIntent() != null) {
            customerName = getIntent().getStringExtra("name");
            mobileno = getIntent().getStringExtra("mobileno");
        }
        cityList = new ArrayList<>();
        Controller.GetCities(SearchCityActivity.this, "", GetcityListner);

        searchCity_tv.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Controller.GetCities(SearchCityActivity.this, charSequence.toString(), GetcityListner);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        cityRVAdapter.notifyDataSetChanged();
                    }
                });
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        cityRVAdapter = new CityRVAdapter(this, cityList, this);
        cityRv.setLayoutManager(new LinearLayoutManager(SearchCityActivity.this, LinearLayoutManager.VERTICAL, false));
        cityRv.setHasFixedSize(true);
        cityRv.setAdapter(cityRVAdapter);
    }

    @Override
    public void onSelect(String name, int id) {
        Intent intent = new Intent(SearchCityActivity.this, Service_request_activity.class);
        intent.putExtra("cityname", name);
        intent.putExtra("cityId", id);
        intent.putExtra("name", customerName);
        intent.putExtra("mobileno", mobileno);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(SearchCityActivity.this, Service_request_activity.class);
        intent.putExtra("name", customerName);
        intent.putExtra("mobileno", mobileno);
        startActivity(intent);
        finish();
    }
}