package com.sar.user.livfastRewards.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.sar.user.livfastRewards.R;


public class Logs_fragment extends Fragment {

    Context context;
    Bundle bundle;
    private TabLayout tabLayout;

    @SuppressLint("ValidFragment")
    public Logs_fragment(Context context1) {
        context = context1;
    }

    public Logs_fragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.log_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LinearLayout container = (LinearLayout) getView().findViewById(R.id.fragment_container);
        tabLayout = (TabLayout) getView().findViewById(R.id.tabbar);

        tabLayout.addTab(tabLayout.newTab().setText("Stock"));
        tabLayout.addTab(tabLayout.newTab().setText("Secondary"));
        tabLayout.addTab(tabLayout.newTab().setText("Tertiary"));
        //tabLayout.addTab(tabLayout.newTab().setText("Service Request"));
        replaceFragment(new LogsStockFragment());
        Intent intent = getActivity().getIntent();
        bundle = intent.getExtras();
        if (bundle != null) {
            if (bundle.getString("product_tab") != null && bundle.getString("product_tab").equals("product")) {
                tabLayout.getTabAt(1).select();
                replaceFragment(new LogsPurchaseFragment());
            } else if (bundle.getString("warranty_tab") != null && bundle.getString("warranty_tab").equals("warranty")) {
                tabLayout.getTabAt(2).select();
                replaceFragment(new LogsWarrantyFragment());
            } else if (bundle.getString("service_tab") != null && bundle.getString("service_tab").equals("service")) {
                tabLayout.getTabAt(3).select();
                replaceFragment(new LogsServiceRequestFragment());
            } else {
                replaceFragment(new LogsStockFragment());
            }
        }

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    replaceFragment(new LogsStockFragment());
                } else if (tab.getPosition() == 1) {
                    replaceFragment(new LogsPurchaseFragment());
                } else if (tab.getPosition() == 2) {
                    replaceFragment(new LogsWarrantyFragment());
                } else {
                    replaceFragment(new LogsServiceRequestFragment());
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


    }

    private void replaceFragment(Fragment fragment) {
        /*FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.fragment_container, fragment);
        transaction.commitAllowingStateLoss();*/
        FragmentManager fragmentManager = getChildFragmentManager();
        android.support.v4.app.FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.fragment_container, fragment);
        transaction.commitAllowingStateLoss();
    }

}
