package com.sar.user.livfastRewards.Model;

public class warrantyRedeemModel {
    int id, loyalty_point, battery_scheme;
    boolean is_manual, is_active;
    String created_timestamp, updated_timestamp;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLoyalty_point() {
        return loyalty_point;
    }

    public void setLoyalty_point(int loyalty_point) {
        this.loyalty_point = loyalty_point;
    }

    public int getBattery_scheme() {
        return battery_scheme;
    }

    public void setBattery_scheme(int battery_scheme) {
        this.battery_scheme = battery_scheme;
    }

    public boolean is_manual() {
        return is_manual;
    }

    public void setIs_manual(boolean is_manual) {
        this.is_manual = is_manual;
    }

    public boolean is_active() {
        return is_active;
    }

    public void setIs_active(boolean is_active) {
        this.is_active = is_active;
    }

    public String getCreated_timestamp() {
        return created_timestamp;
    }

    public void setCreated_timestamp(String created_timestamp) {
        this.created_timestamp = created_timestamp;
    }

    public String getUpdated_timestamp() {
        return updated_timestamp;
    }

    public void setUpdated_timestamp(String updated_timestamp) {
        this.updated_timestamp = updated_timestamp;
    }
}
