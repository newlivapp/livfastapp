package com.sar.user.livfastRewards.Database;

import android.content.ContentValues;

/**
 * Created by Nav on 02-Aug-17.
 */

public abstract class AbstractEntry {
    public static final String UUID = "uuid";

    protected String uuid;

    public String getUuid() {
        return this.uuid;
    }

    public abstract ContentValues getContentValues();

    public boolean equals(AbstractEntry entity) {
        return this.getUuid().equals(entity.getUuid());
    }
}
