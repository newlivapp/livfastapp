package com.sar.user.livfastRewards.Model;

public class wDetailModel {
    String jobType, warranty_num;

    public String getJobType() {
        return jobType;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }


    public String getWarranty_num() {
        return warranty_num;
    }

    public void setWarranty_num(String warranty_num) {
        this.warranty_num = warranty_num;
    }
}
