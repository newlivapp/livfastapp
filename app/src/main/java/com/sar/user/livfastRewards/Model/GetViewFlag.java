package com.sar.user.livfastRewards.Model;

public class GetViewFlag {

    private String user_id;
    private String state;

    public String getId() {
        return user_id;
    }

    public void setId(String id) {
        this.user_id = id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
