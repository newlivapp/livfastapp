package com.sar.user.livfastRewards.helper;

import android.util.SparseArray;

public class UrlResolver {

    public static String BaseUrl = "http://dlpdevelopment-env.ap-south-1.elasticbeanstalk.com/";
    public static String BaseURl1 = "http://35.154.12.82:3002/";
//    public static String BaseURl1 = "http://13.234.24.112:3000/";
    public static String BaseUrl2 = "http://13.127.176.185/LivCRM/index.php";
    public static SparseArray<String> urldata2 = null;


    public static SparseArray<String> urldata = null;
    public static SparseArray<String> urldata1 = null;
    //API by Sanjay
    public static String API_REDEEM_POINTS = BaseURl1 + "redeempoints";
    public static String API_REDEEM_CONFIRMATION_POINTS = BaseURl1 + "redeemconfirmationpoints";
    public static String API_OTP_CONFIRMATION = BaseURl1 + "otpconfirmation";
    public static String API_GET_REDEEM_HISTORY = BaseURl1 + "getRedeemhistory";
    public static String API_GET_COUPON = BaseURl1 + "getcoupon";
    public static String API_SCRATCH_STATUS = BaseURl1 + "scratchstatus";
    public static String API_SCRATCH_POINTS_CHECK = BaseURl1 + "pointscheck";
    public static String API_NEWSCHME_POINTS = BaseURl1 + "schemepoints";
    public static  String API_SECONDARY_POINTS=BaseURl1+"secondarypoints";
    public static  String API_POPUPS=BaseURl1+"getDealerpoints1";


    public static final String path(int item) {
        if (urldata == null) {
            urlmapper();
        }
        String url = BaseUrl.concat(urldata.get(item));
        return url;
    }

    public static final String path1(int item) {
        if (urldata1 == null) {
            urlmapper1();
        }
        String url = BaseURl1.concat(urldata1.get(item));
        return url;
    }

    public static final String path2(int item) {
        if (urldata2 == null) {
            urlmapper2();
        }
        String url = BaseUrl2.concat(urldata2.get(item));
        return url;
    }

    private static void urlmapper() {
        urldata = new SparseArray<String>();
//        urldata1.put(GetUrl.Login,"registration");
//        urldata.put(GetUrl.SetPassword,"v1/setpassword/");
//        urldata.put(GetUrl.ForgotPassword,"v1/forgotpassword/");
        //  urldata.put(GetUrl.ProfileUpdate,"v1/profileupdate/");
        urldata.put(GetUrl.Notification, "v1/notification/");
        // urldata.put(GetUrl.Product,"v1/dealerproduct/");
        //  urldata.put(GetUrl.Warranty,"v1/productwarranty/");
        //  urldata.put(GetUrl.Targets,"v1/getlatestofferscheme/");
//        urldata.put(GetUrl.Edit_Profile_Pic, "v1/profilepicupload/");
        // urldata.put(GetUrl.Banner,"v1/getproductbanner/");
        //urldata.put(GetUrl.Target_Detail,"v1/productloyalty/");
        // urldata.put(GetUrl.Profile_Update,"v1/profileupdate/");
        urldata.put(GetUrl.Segment, "v1/carsegment/");
        urldata.put(GetUrl.Manufacturer, "v1/carmanufacturer/");
        urldata.put(GetUrl.CarModel, "v1/carmodel");
        urldata.put(GetUrl.SerialToUnique, "v1/serialtounique/");
        urldata.put(GetUrl.GetCitites, "v1/getcities/");
        urldata.put(GetUrl.DealerService, "v1/dealerservice/");
        //urldata.put(GetUrl.GetStock,"v1/getsku/");
        //  urldata.put(GetUrl.GetRedeemHistory,"v1/dealerredeem/");
        //  urldata.put(GetUrl.CalculateLoyaly,"v1/newcalculateproductloyalty/");
        // urldata.put(GetUrl.GetRedeemSlab,"v1/getredeemslab/");


    }

    private static void urlmapper1() {
        urldata1 = new SparseArray<String>();
        urldata1.put(GetUrl.Login, "registration");
        urldata1.put(GetUrl.Warranty, "serialnumber");
        urldata1.put(GetUrl.Loyality, "totalloyalty");
        urldata1.put(GetUrl.GETWARRANTY, "logdetail");
        urldata1.put(GetUrl.SCHEMEURL, "schemeurl");
        urldata1.put(GetUrl.SCHEMEURL1, "scheme4wurl");
        urldata1.put(GetUrl.SCHEMEURL2, "schemeerurl");
        urldata1.put(GetUrl.SCHEMEURL3, "schemesecondaryurl");
        urldata1.put(GetUrl.SCHEMEURL5, "schemeurlerick");
        urldata1.put(GetUrl.Banner, "getbanner");
        urldata1.put(GetUrl.Product, "addpurchage");
        urldata1.put(GetUrl.GetRedeemSlab, "redeemslabibups");
        urldata1.put(GetUrl.Get4wslab, "redeemslab4w");
        urldata1.put(GetUrl.GetErickSlab, "redeemslaber");
        urldata1.put(GetUrl.GetErickSlabPR, "ericksecredeemslab");
        urldata1.put(GetUrl.GetSecSlab, "addredeemslab");
        urldata1.put(GetUrl.GetPurchaseLog, "addpurchagelogdetail");
        urldata1.put(GetUrl.GetStock, "addstockdetail");
        urldata1.put(GetUrl.Profile_Update, "editprofile");
//        urldata1.put(GetUrl.Profile_Update, "addupdateprofile");
        urldata1.put(GetUrl.Warranty_Redeem, "addredeemslabwarranty");
        urldata1.put(GetUrl.Warranty_Points, "warrantytotalloyaltyp");
        urldata1.put(GetUrl.CalculateLoyaly, "calcltpnt");
        urldata1.put(GetUrl.Target_Detail, "calculatorinputs");
        urldata1.put(GetUrl.POINTS, "qualificationcount");
        urldata1.put(GetUrl.Edit_Profile_Pic, "conversion");
        urldata1.put(GetUrl.DEALERSTATECITY, "dealerstatecity");
        urldata1.put(GetUrl.TERT,"totalloyaltytert");
    }

    private static void urlmapper2() {
        urldata2 = new SparseArray<String>();
        urldata2.put(GetUrl.BATTERYTYPE, "?entryPoint=Drona");
    }

    public interface GetUrl {
        int Login = 0;
        //        int SetPassword=1;
//        int ForgotPassword=2;
        int ProfileUpdate = 3;
        int Notification = 4;
        int Product = 5;
        int GETWARRANTY = 6;
        int Warranty = 7;
        //    int Targets=8;
        int Edit_Profile_Pic = 9;
        int Banner = 10;
        int Target_Detail = 11;
        int Profile_Update = 12;
        int Segment = 13;
        int Manufacturer = 14;
        int CarModel = 15;
        int SerialToUnique = 16;
        int GetCitites = 17;
        int DealerService = 18;
        int GetStock = 19;
        int GetRedeemHistory = 20;
        int CalculateLoyaly = 21;
        int GetRedeemSlab = 22;
        int Loyality = 23;
        int SCHEMEURL = 24;
        int GetPurchaseLog = 25;
        int Warranty_Redeem = 26;
        int Warranty_Points = 27;
        int BATTERYTYPE = 28;
        int POINTS = 29;
        int Get4wslab = 30;
        int GetErickSlab = 31;
        int SCHEMEURL1 = 32;
        int SCHEMEURL2 = 33;
        int GetSecSlab = 34;
        int SCHEMEURL3 = 35;
        int GetErickSlabPR = 36;
        int SCHEMEURL5 = 37;
        int DEALERSTATECITY = 38;
        int TERT=39;
    }
}
