package com.sar.user.livfastRewards.Model;

import java.math.BigInteger;

/**
 * Created by user on 31-03-2017.
 */

public class BattCodeResponseModel {
    String message;
    BigInteger phone_no;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public BigInteger getPhone_no() {
        return phone_no;
    }

    public void setPhone_no(BigInteger phone_no) {
        this.phone_no = phone_no;
    }
}
