package com.sar.user.livfastRewards.activity;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.sar.user.livfastRewards.Interface.RequestListener;
import com.sar.user.livfastRewards.Model.AddProductModel;
import com.sar.user.livfastRewards.Model.DealeProductModel;
import com.sar.user.livfastRewards.Model.ProductListModel;
import com.sar.user.livfastRewards.Model.PurchaseProductModelNew;
import com.sar.user.livfastRewards.R;
import com.sar.user.livfastRewards.fragment.PicModeSelectDialogFragment;
import com.sar.user.livfastRewards.helper.Controller;
import com.sar.user.livfastRewards.helper.JsonUtils;
import com.sar.user.livfastRewards.helper.PrefManager;
import com.sar.user.livfastRewards.helper.ProfileImage.GOTOConstants;
import com.sar.user.livfastRewards.helper.ProfileImage.ImageCropActivity;

import org.json.JSONException;

import java.text.ParseException;

public class add_product_activity extends BaseActivity {
    private static int SELECT_PICTURE = 1;
    private static String[] PERMISSIONS_LOCATION = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
    final int REQUEST_LOCATION = 2;
    EditText comment;
    TextView qrcode;
    Button button_add;
    ImageView view_previous, scanner_icon;
    String result;
    ProgressBar progressBar;
    Toolbar toolbar;
    ImageView back_button;
    ImageView productImage;
    TextView uploadImageTv;
    DealeProductModel dealeProductModel = null;
    PurchaseProductModelNew ProductModel = null;
    String user_id;
    View.OnClickListener uploadClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (hasLocationPermissionGranted())
                showAddProfilePicDialog1();
            else
                requestLocationPermission();
        }
    };
    View.OnClickListener backClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };
    RequestListener addProductListener = new RequestListener() {
        @Override
        public void onRequestStarted() {
        }

        @SuppressLint("LongLogTag")
        @Override
        public void onRequestCompleted(final Object responseObject) throws JSONException, ParseException {
            Log.d("response AddProductDetail", responseObject.toString());
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    button_add.setEnabled(true);
                    progressBar.setVisibility(View.GONE);
                }
            });
            try {
                ProductModel = JsonUtils.objectify(responseObject.toString(), PurchaseProductModelNew.class);
                assert ProductModel != null;
                //dealeProductModel = ProductModel.getModel_number();
            } catch (Exception e) {
            }
            if (responseObject.toString().equals("Something Went Wrong")) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(add_product_activity.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
                    }
                });
            } else if (responseObject.toString().equals("Serial Number is not in Scheme")) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(add_product_activity.this, "Serial Number is not in Scheme", Toast.LENGTH_SHORT).show();
                    }
                });

            } else if (responseObject.toString().equals("Invalid Barcode")) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(add_product_activity.this, "Invalid Barcode", Toast.LENGTH_SHORT).show();
                    }
                });
            } else if (ProductModel == null) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(add_product_activity.this, responseObject.toString(), Toast.LENGTH_SHORT).show();
                    }
                });
                return;
            } else {
                //Custome Analytics Fabric
                Answers.getInstance().logCustom(new CustomEvent("Purchase Secondary")
                        .putCustomAttribute("BAT_CODE", PrefManager.getInstance(add_product_activity.this).getBatCode())
                        .putCustomAttribute("USER_NAME", PrefManager.getInstance(add_product_activity.this).getDealershipName())
                        .putCustomAttribute("UNIQUE_NUMBER", qrcode.getText().toString().trim()));

                runOnUiThread(new Runnable() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);

                        final Dialog dialog = new Dialog(add_product_activity.this);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.popup_massage);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                        final Button addMore = dialog.findViewById(R.id.addmore);

                        addMore.setText("Add More Product");
                        final TextView close = dialog.findViewById(R.id.close);
                        final TextView msg = dialog.findViewById(R.id.bettarymsg);

                        msg.setText(ProductModel.getModel_number() + "\n" + " product has been added successfully");

                        addMore.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent1 = new Intent(add_product_activity.this, ScannerActivity.class);
                                intent1.putExtra("AddProduct", "NewProduct");
                                startActivity(intent1);
                                finish();
                            }
                        });
                        close.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(add_product_activity.this, ProductDetailActivity.class);
                                startActivity(intent);
                            }
                        });

                        if (add_product_activity.this != null)
                            add_product_activity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (!(add_product_activity.this).isFinishing()) {
                                        try {
                                            dialog.show();
                                        } catch (WindowManager.BadTokenException e) {
                                            Log.e("WindowManagerBad ", e.toString());
                                        } catch (Exception e) {
                                            Log.e("Exception ", e.toString());
                                        }
                                    }
                                }
                            });
                    }
                });

            }
        }

        @Override
        public void onRequestError(int errorCode, String message) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    button_add.setEnabled(true);
                    progressBar.setVisibility(View.GONE);
                }
            });

            handelError(errorCode, message);

        }
    };
    private String image_path;
    private ProductListModel productListModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product_activity);

        toolbar = findViewById(R.id.toolbar);
        back_button = toolbar.findViewById(R.id.back_button);
        back_button.setOnClickListener(backClick);
        button_add = findViewById(R.id.add_button);
        qrcode = findViewById(R.id.qrcode);
        comment = findViewById(R.id.comment);
        scanner_icon = findViewById(R.id.scanner_icon);
        view_previous = findViewById(R.id.view_previous);
        progressBar = findViewById(R.id.progressbar);
        productImage = findViewById(R.id.productImage);
        uploadImageTv = findViewById(R.id.uploadImageTv);
        result = getIntent().getStringExtra("result");
        if (result != null && !result.equals(""))
            result = result.toUpperCase();
        qrcode.setText(result);
        // qrcode.setSelection(qrcode.getText().length());

        user_id = PrefManager.getInstance(this).getÜserId();
        button_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (qrcode.getText().toString().trim().isEmpty()) {
                    Toast.makeText(add_product_activity.this, "Please Enter Unique No", Toast.LENGTH_SHORT).show();
                } else {
                    button_add.setEnabled(false);
                    AddProductModel addProductModel = new AddProductModel();
                    addProductModel.setUnique_code(qrcode.getText().toString().trim());
                    addProductModel.setSerial_number(result);
                    addProductModel.setComment(comment.getText().toString());
                    addProductModel.setUser_id(user_id);
                    Controller.AddProductDetail(add_product_activity.this, addProductModel, addProductListener);
                    progressBar.setVisibility(View.VISIBLE);
                }
            }

        });
        view_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(add_product_activity.this, ProductDetailActivity.class);
                intent.putExtra("product_tab", "product");
                intent.putExtra("tab", 1);
                startActivity(intent);
                finish();

            }
        });
        scanner_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(add_product_activity.this, ScannerActivity.class);
                intent.putExtra("AddProduct", "NewProduct");
                startActivity(intent);
                finish();
            }
        });

        uploadImageTv.setOnClickListener(uploadClick);

        productImage.setOnClickListener(uploadClick);
    }

    private Bitmap showCroppedImage(String imagePath) {
        if (imagePath != null) {
            return BitmapFactory.decodeFile(imagePath);

        }
        return null;
    }

    public boolean hasLocationPermissionGranted() {
        return ContextCompat.checkSelfPermission(add_product_activity.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(add_product_activity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    private void showAddProfilePicDialog1() {
        PicModeSelectDialogFragment dialogFragment = new PicModeSelectDialogFragment();
        dialogFragment.setiPicModeSelectListener(new PicModeSelectDialogFragment.IPicModeSelectListener() {
            @Override
            public void onPicModeSelected(String mode) {
                String action = mode.equalsIgnoreCase(GOTOConstants.PicModes.CAMERA) ? GOTOConstants.IntentExtras.ACTION_CAMERA : GOTOConstants.IntentExtras.ACTION_GALLERY;
                Intent intent = new Intent(add_product_activity.this, ImageCropActivity.class);
                intent.putExtra("ACTION", action);
                startActivityForResult(intent, SELECT_PICTURE);
            }
        });
        dialogFragment.show(getFragmentManager(), "picModeSelector");
    }

    public void requestLocationPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            ActivityCompat.requestPermissions(add_product_activity.this, PERMISSIONS_LOCATION,
                    REQUEST_LOCATION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_LOCATION:
                if (grantResults.length > 0) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        //Storage permission is enabled
                        showAddProfilePicDialog1();

                    } else if (ActivityCompat.shouldShowRequestPermissionRationale(add_product_activity.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                            && ActivityCompat.shouldShowRequestPermissionRationale(add_product_activity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            && ActivityCompat.shouldShowRequestPermissionRationale(add_product_activity.this, Manifest.permission.CAMERA)) {
                        //User has deny from permission dialog
                        final AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(add_product_activity.this, R.style.AppCompatAlertDialogStyle);
                        alertDialog1.setTitle("Reading Permission Denied");
                        alertDialog1.setMessage("Are you sure you want to deny this permission?");
                        alertDialog1.setPositiveButton("I'M SURE", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                requestLocationPermission();
                            }
                        });
                        alertDialog1.setNegativeButton("RETRY", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                requestLocationPermission();
                            }
                        });
                        alertDialog1.show();
                    } else {
                        // User has deny permission and checked never show permission dialog so you can redirect to Application settings page
                        AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(add_product_activity.this, R.style.AppCompatAlertDialogStyle);
                        alertDialog1.setMessage("It looks like you have turned off permission required for this feature. It can be enabled under Phone Settings > Apps > Partner > Permissions");
                        alertDialog1.setPositiveButton("GO TO SETTINGS", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent();
                                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package", getPackageName(), null);
                                intent.setData(uri);
                                startActivity(intent);
                            }
                        });
                        alertDialog1.show();
                    }
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SELECT_PICTURE) {
            if (resultCode == RESULT_OK) {
                image_path = data.getStringExtra(GOTOConstants.IntentExtras.IMAGE_PATH);
                progressBar.setVisibility(View.GONE);
                uploadImageTv.setVisibility(View.GONE);
                productImage.setVisibility(View.VISIBLE);
                productImage.setImageBitmap(showCroppedImage(image_path));
            } else if (resultCode == RESULT_CANCELED) {
                //TODO : Handle case
            } else {
                String errorMsg = data.getStringExtra(ImageCropActivity.ERROR_MSG);
                Toast.makeText(add_product_activity.this, errorMsg, Toast.LENGTH_LONG).show();
            }
        }
    }
}
