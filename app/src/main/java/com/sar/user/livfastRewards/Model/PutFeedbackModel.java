package com.sar.user.livfastRewards.Model;

/**
 * Created by Nav on 02-Aug-17.
 */

public class PutFeedbackModel {
    int rating;
    String comment, cta_status, ticketID;

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCta_status() {
        return cta_status;
    }

    public void setCta_status(String cta_status) {
        this.cta_status = cta_status;
    }

    public String getTicketID() {
        return ticketID;
    }

    public void setTicketID(String ticketID) {
        this.ticketID = ticketID;
    }
}
