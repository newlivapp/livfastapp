package com.sar.user.livfastRewards.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sar.user.livfastRewards.Model.ProductModel;
import com.sar.user.livfastRewards.R;

import java.util.List;

public class BatteryModelAdapter extends RecyclerView.Adapter<BatteryModelAdapter.MyviewHolder> {
    List<ProductModel> batteryList;
    Context context;

    public BatteryModelAdapter(Context context1, List<ProductModel> batteryList1) {
        context = context1;
        batteryList = batteryList1;
    }

    @Override
    public MyviewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.battery_model_detail, parent, false);
        MyviewHolder mvh = new MyviewHolder(v);
        return mvh;
    }

    @Override
    public void onBindViewHolder(MyviewHolder holder, int position) {
        holder.battery_name.setText(batteryList.get(position).getProduct().toUpperCase());
        holder.model_name.setText(batteryList.get(position).getBrand__name());
        holder.capacity.setText(batteryList.get(position).getCapacity());
        holder.warranty.setText(batteryList.get(position).getWarranty());
    }

    @Override
    public int getItemCount() {
        return batteryList.size();
    }

    public class MyviewHolder extends RecyclerView.ViewHolder {
        TextView battery_name, model_name, capacity, warranty;

        public MyviewHolder(View itemView) {
            super(itemView);
            battery_name = (TextView) itemView.findViewById(R.id.battery_name);
            model_name = (TextView) itemView.findViewById(R.id.model_name);
            capacity = (TextView) itemView.findViewById(R.id.capacity);
            warranty = (TextView) itemView.findViewById(R.id.warranty);
        }
    }
}
