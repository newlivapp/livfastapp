package com.sar.user.livfastRewards.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sar.user.livfastRewards.Interface.FeedbackListner;
import com.sar.user.livfastRewards.Interface.RequestListener;
import com.sar.user.livfastRewards.Model.GetServiceRequestModel;
import com.sar.user.livfastRewards.R;
import com.sar.user.livfastRewards.activity.Feedback_activity;
import com.sar.user.livfastRewards.adapter.ServiceRequestRVAdapter;
import com.sar.user.livfastRewards.helper.Controller;
import com.sar.user.livfastRewards.helper.MyUtils;

import org.json.JSONException;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Nav on 26-Jun-17.
 */

public class LogsServiceRequestFragment extends BaseFragment implements FeedbackListner {
    Context context;
    TextView loyalty_points_colom;
    RecyclerView serive_request_rv;
    TextView startDate_tv, endDate_tv;
    Calendar calendar = Calendar.getInstance();
    int final_end_year = calendar.get(Calendar.YEAR);
    int final_end_month = calendar.get(Calendar.MONTH);
    int final_end_day = calendar.get(Calendar.DAY_OF_MONTH);

    int final_start_day = calendar.get(Calendar.DAY_OF_MONTH) - 7;
    int final_start_month = calendar.get(Calendar.MONTH);
    int final_start_year = calendar.get(Calendar.YEAR);
    List<GetServiceRequestModel> serviceresponseList;
    ServiceRequestRVAdapter serviceRequestRVAdapter;
    SwipeRefreshLayout swipeRefreshLayout;
    RelativeLayout empty_rl;

    String str_start_date, str_end_date;
    RequestListener productListner = new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @SuppressLint("LongLogTag")
        @Override
        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
            Log.d("response GetServiceRequest", responseObject.toString());
            serviceresponseList.clear();
            Type collection = new TypeToken<List<GetServiceRequestModel>>() {
            }.getType();
            List<GetServiceRequestModel> srl = (List<GetServiceRequestModel>) new Gson().fromJson(responseObject.toString(), collection);
            serviceresponseList.addAll(srl);
            if ((Activity) getContext() != null) {
                ((Activity) getContext()).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (serviceresponseList.isEmpty()) {
                            empty_rl.setVisibility(View.VISIBLE);
                            serive_request_rv.setVisibility(View.GONE);
                        } else {
                            empty_rl.setVisibility(View.GONE);
                            serive_request_rv.setVisibility(View.VISIBLE);
                        }
                        swipeRefreshLayout.setRefreshing(false);
                        serviceRequestRVAdapter.notifyDataSetChanged();
                    }
                });
            }
        }

        @Override
        public void onRequestError(int errorCode, String message) {
            if ((Activity) getContext() != null) {
                ((Activity) getContext()).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefreshLayout.setRefreshing(false);
                    }
                });
            }

            handelError(errorCode, message);
        }
    };

    @SuppressLint("ValidFragment")
    public LogsServiceRequestFragment(Context context1) {
        context = context1;
    }

    public LogsServiceRequestFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.logs_service_request_fragmnet, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        serive_request_rv = (RecyclerView) getView().findViewById(R.id.service_request_rv);
        loyalty_points_colom = (TextView) getView().findViewById(R.id.loyalty_points_colom);
        startDate_tv = (TextView) getView().findViewById(R.id.startDate);
        endDate_tv = (TextView) getView().findViewById(R.id.endDate);
        swipeRefreshLayout = (SwipeRefreshLayout) getView().findViewById(R.id.swiperefresh);
        empty_rl = (RelativeLayout) getView().findViewById(R.id.empty_rl);
        final DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        final Date date = new Date();
        String todate = dateFormat.format(date);
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, -7);
        Date todate1 = cal.getTime();
        String fromdate = dateFormat.format(todate1);
        startDate_tv.setText(fromdate);
        endDate_tv.setText(todate);
        str_start_date = MyUtils.getYearlyFormat(startDate_tv.getText().toString());

        str_end_date = MyUtils.getYearlyFormat(endDate_tv.getText().toString());

        startDate_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final com.wdullaer.materialdatetimepicker.date.DatePickerDialog dpd = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance(new com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(com.wdullaer.materialdatetimepicker.date.DatePickerDialog view, final int year, final int monthOfYear, final int dayOfMonth) {

                        final String select_date = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;

                        ((Activity) getContext()).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

                                    Date date1 = formatter.parse(select_date);
                                    Date date2 = formatter.parse(MyUtils.getslashFormat(str_end_date));

                                    if (date1.compareTo(date2) <= 0) {
                                        final_start_day = dayOfMonth;
                                        final_start_month = monthOfYear + 1;
                                        final_start_year = year;
                                        startDate_tv.setText(MyUtils.getdaydashFormat(select_date));
                                        str_start_date = MyUtils.getYearlydashFormat(select_date);

                                        Log.d("ComparisonOfdate", "-----" + date1 + "is less Than---" + date2);

                                        Controller.GetServiceRequest(getContext(), MyUtils.getYearlydashFormat(select_date), str_end_date, productListner);
                                    } else if (date1.compareTo(date2) > 0) {
                                        Log.d("ComparisonOfdate", "-----" + date1 + "is greater than---" + date2);
                                        Toast.makeText(getContext(), "Start Date can not be greater than End Date", Toast.LENGTH_SHORT).show();
                                    }
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                    }
                }, final_start_year, final_start_month, final_start_day);
                dpd.setMaxDate(calendar);
                dpd.show(getActivity().getFragmentManager(), "DATE_PICKER_TAG");
            }
        });
        endDate_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final com.wdullaer.materialdatetimepicker.date.DatePickerDialog dpd = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance(new com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(com.wdullaer.materialdatetimepicker.date.DatePickerDialog view, final int year, final int monthOfYear, final int dayOfMonth) {

                        final String select_date = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;

                        ((Activity) getContext()).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

                                    Date date1 = formatter.parse(MyUtils.getslashFormat(str_start_date));
                                    Date date2 = formatter.parse(select_date);

                                    if (date1.compareTo(date2) <= 0) {
                                        Log.d("ComparisonOfdate", "-----" + date1 + "is less Than---" + date2);
                                        final_end_day = dayOfMonth;
                                        final_end_month = monthOfYear + 1;
                                        final_end_year = year;
                                        endDate_tv.setText(MyUtils.getdaydashFormat(select_date));
                                        str_end_date = MyUtils.getYearlydashFormat(select_date);
                                        // Controller.GetServiceRequest(getContext(),str_start_date,MyUtils.getYearlydashFormat(select_date),productListner);
                                    } else if (date1.compareTo(date2) > 0) {
                                        Log.d("ComparisonOfdate", "-----" + date1 + "is greater than---" + date2);
                                        Toast.makeText(getContext(), "End Date can not be Less than Start Date", Toast.LENGTH_SHORT).show();
                                    }
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                    }
                }, final_end_year, final_end_month, final_end_day);

                dpd.setMaxDate(calendar);
                dpd.show(getActivity().getFragmentManager(), "DATE_PICKER_TAG");
            }
        });


        serviceresponseList = new ArrayList<GetServiceRequestModel>();
        //  Controller.GetServiceRequest(getContext(),str_start_date,str_end_date,productListner);
        serviceRequestRVAdapter = new ServiceRequestRVAdapter(getContext(), serviceresponseList, this);
        serive_request_rv.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        serive_request_rv.setAdapter(serviceRequestRVAdapter);
        serive_request_rv.setHasFixedSize(true);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //  Controller.GetServiceRequest(getContext(),str_start_date,str_end_date,productListner);
            }
        });
    }

    @Override
    public void onFeedBackClick(String mobile, String status, String ticket, String serail, String cta_status, String name, int id) {
        Log.d("Feedback---", mobile + "----" + status + "---" + ticket + "----" + serail + "----" + cta_status + "-----" + name + "ID----" + id);
        Intent intent = new Intent(getContext(), Feedback_activity.class);
        intent.putExtra("mobile", mobile);
        intent.putExtra("status", status);
        intent.putExtra("ticket", ticket);
        intent.putExtra("serial", serail);
        intent.putExtra("cta_status", cta_status);
        intent.putExtra("name", name);
        intent.putExtra("id", id);
        getContext().startActivity(intent);
    }
}
