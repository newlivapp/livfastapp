package com.sar.user.livfastRewards.fragment;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.DownloadManager;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.horizontalnumberpicker.HorizontalNumberPicker;
import com.sar.user.livfastRewards.Database.DatabaseBackend;
import com.sar.user.livfastRewards.Interface.OnItemRemoveListner;
import com.sar.user.livfastRewards.Interface.RequestListener;
import com.sar.user.livfastRewards.Model.CalculateLoyaltyPointsModel;
import com.sar.user.livfastRewards.Model.ProductListForCalculateModel;
import com.sar.user.livfastRewards.Model.ProductLoyalityModel;
import com.sar.user.livfastRewards.Model.ResultCalculatedPointsModel;
import com.sar.user.livfastRewards.Model.TargetsModel;
import com.sar.user.livfastRewards.R;
import com.sar.user.livfastRewards.adapter.LoyaltyPointsProductAdapter;
import com.sar.user.livfastRewards.adapter.TargetRecyclerViewAdapter;
import com.sar.user.livfastRewards.helper.Controller;
import com.sar.user.livfastRewards.helper.JsonUtils;
import com.sar.user.livfastRewards.helper.MyUtils;

import org.json.JSONException;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.bumptech.glide.gifdecoder.GifHeaderParser.TAG;

//import com.tagbin.user.liveguardloyalty.Model.RedeemProductModel;

public class Target_fragmnet extends BaseFragment implements com.horizontalnumberpicker.HorizontalNumberPickerListener, OnItemRemoveListner {
    Spinner battery_current_spinner, battery_warranty_spinner;
    TextView purchase_date, clear_all_tv;
    RecyclerView totalProdct_rv;
    LoyaltyPointsProductAdapter loyaltyPointsProductAdapter;
    List<ProductLoyalityModel> addedProductList;
    RecyclerView target_rv;
    SwipeRefreshLayout swipeRefreshLayout;
    List<TargetsModel> targetsList;
    Button calculate_button;
    Button pdf_button, buttonpdf2, buttonpdf3, buttonpdf4, buttonpdf5;
    TargetRecyclerViewAdapter targetAdapter;
    ProgressBar progressBar;
    Context context;
    Calendar calendar = Calendar.getInstance();
    int year = calendar.get(Calendar.YEAR);
    int month = calendar.get(Calendar.MONTH);
    int day = calendar.get(Calendar.DAY_OF_MONTH);
    AnimatorSet animatorSet;
    RelativeLayout calculater_RL, offer_RL;
    TextView calculator_tv, offer_tv;
    List<ProductLoyalityModel> productLoyalityList;
    DatabaseBackend databaseBackend;
    List<String> battery_Ah_List;
    List<String> warrantyList;
    RelativeLayout pdfdownload;
    RelativeLayout empty_relative_layout;
    String selected_bettary_ah, selected_warranty;
    String str_start_date;
    CalculateLoyaltyPointsModel calculateLoyaltyPointsModel;
    List<ProductListForCalculateModel> productListForCalculateModelsList;
    LinearLayoutManager targetManager, calculatormanager;
    ArrayAdapter<String> battery_warranty_adapte;
    ScrollView scrollView;
    TextView pdfLink, pdflink1, pdflink2, pdflink3, pdflink5;
    DownloadManager downloadManager;
    Long refrence;
    View.OnClickListener calculateClick = new View.OnClickListener() {
        @RequiresApi(api = Build.VERSION_CODES.M)
        @Override
        public void onClick(View v) {
            if (addedProductList.size() == 0) {
                Toast.makeText(getActivity(), "Please select your battery", Toast.LENGTH_SHORT).show();
            } else {
                Log.d("AdapterArraySize", "" + addedProductList.size());
                productListForCalculateModelsList.clear();
                calculateLoyaltyPointsModel = new CalculateLoyaltyPointsModel();
                calculateLoyaltyPointsModel.setEnd_date(str_start_date);
                for (int i = 0; i < addedProductList.size(); i++) {
                    ProductListForCalculateModel productListForCalculateModel = new ProductListForCalculateModel();
                    productListForCalculateModel.setId(String.valueOf(addedProductList.get(i).getId()));
                    productListForCalculateModel.setProduct(String.valueOf(addedProductList.get(i).getProduct()));
                    productListForCalculateModel.setQuantity(String.valueOf(addedProductList.get(i).getQuantity()));
                    productListForCalculateModelsList.add(productListForCalculateModel);
                }
                calculateLoyaltyPointsModel.setProducts(productListForCalculateModelsList);
                progressBar.setVisibility(View.VISIBLE);
                Controller.CalculateLoyalty(getContext(), calculateLoyaltyPointsModel, new RequestListener() {
                    @Override
                    public void onRequestStarted() {
                    }

                    @SuppressLint("LongLogTag")
                    @Override
                    public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
                        Log.d("response CalculateLoyalty", responseObject.toString());
                        final ResultCalculatedPointsModel resultCalculatedPointsModel = JsonUtils.objectify(responseObject.toString(), ResultCalculatedPointsModel.class);
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressBar.setVisibility(View.GONE);
                                animatorSet = (AnimatorSet) AnimatorInflater.loadAnimator(getContext(), R.animator.flip);
                                animatorSet.setTarget(calculate_button);
                                calculate_button.setText("Points earned: " + resultCalculatedPointsModel.getTotal_point());
                                calculate_button.setBackgroundResource(R.drawable.green_rounded_button_background);
                                calculate_button.setTextColor(Color.WHITE);
                                calculate_button.setClickable(false);
                                animatorSet.start();
                            }
                        });
                    }

                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onRequestError(int errorCode, String message) {
                        if ((Activity) getContext() != null) {
                            ((Activity) getContext()).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    progressBar.setVisibility(View.GONE);
                                }
                            });
                        }

                        handelError(errorCode, message);
                    }
                });
            }
        }
    };
    View.OnClickListener purchaseClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            final com.wdullaer.materialdatetimepicker.date.DatePickerDialog dpd = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance(new com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener() {
                @TargetApi(Build.VERSION_CODES.M)
                @Override
                public void onDateSet(com.wdullaer.materialdatetimepicker.date.DatePickerDialog view, final int year, final int monthOfYear, final int dayOfMonth) {
                    final String select_date = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                    ((Activity) getContext()).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            purchase_date.setText(select_date);
                            str_start_date = MyUtils.getYearlyFormat(select_date);
                            refreshButton();
                            battery_current_spinner.setSelection(0);
                            battery_warranty_spinner.setSelection(0);
                        }
                    });
                }
            }, year, month, day);
//            dpd.setMaxDate(calendar);
            dpd.show(((Activity) context).getFragmentManager(), "DATE_PICKER_TAG");
        }
    };
    View.OnClickListener clear_all_click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (addedProductList.size() > 0) {
                final Dialog dialog = new Dialog(getContext());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.popup_logout);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                TextView main_heading = (TextView) dialog.findViewById(R.id.successmsg);
                TextView msg = (TextView) dialog.findViewById(R.id.bettarymsg);
                final Button addmore = (Button) dialog.findViewById(R.id.addmore);
                final TextView close = (TextView) dialog.findViewById(R.id.close);
                main_heading.setText("Clear All");
                msg.setText("Are you sure you want to clear all\nthe added product");
                addmore.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        refreshButton();
                        battery_current_spinner.setSelection(0);
                        battery_warranty_spinner.setSelection(0);
                        addedProductList.clear();
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loyaltyPointsProductAdapter.notifyDataSetChanged();
                            }
                        });
                        dialog.cancel();
                    }
                });
                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });
                if (getActivity() != null)
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (!(getActivity()).isFinishing()) {
                                try {
                                    dialog.show();
                                } catch (WindowManager.BadTokenException e) {
                                    Log.e("WindowManagerBad ", e.toString());
                                } catch (Exception e) {
                                    Log.e("Exception ", e.toString());
                                }
                            }
                        }
                    });
            }
        }
    };
    //    RequestListener loyalityListner=new RequestListener() {
//        @Override
//        public void onRequestStarted() {
//        }
//        @RequiresApi(api = Build.VERSION_CODES.M)
//        @Override
//        public void onRequestCompleted(final Object responseObject) throws JSONException, ParseException {
//            Log.d("response",responseObject.toString());
//            productLoyalityList.clear();
//            databaseBackend.deleteFilterData();
//            final Type collection=new TypeToken<List<ProductLoyalityModel>>(){}.getType();
//            List<ProductLoyalityModel> plp=(List<ProductLoyalityModel>) new Gson().fromJson(responseObject.toString(),collection);
//            for (int i=0;i<plp.size();i++){
//                ProductLoyalityModel productLoyalityModel=plp.get(i);
//                productLoyalityModel.setQuantity(1);
//                databaseBackend.createFilterData(productLoyalityModel);
//            }
//            productLoyalityList.addAll(plp);
//            warrantyList.add("Battery Warranty");
//            List<String> apiBattryAh=new ArrayList<>();
//            for (int i=0;i<productLoyalityList.size();i++) {
//                apiBattryAh.add(productLoyalityList.get(i).getCapacity());
//            }
//            Set<String> brandhash=new HashSet<>();
//            brandhash.addAll(apiBattryAh);
//            apiBattryAh.clear();
//            apiBattryAh.addAll(brandhash);
//            battery_Ah_List.addAll(apiBattryAh);
//            ((Activity)getContext()).runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    progressBar.setVisibility(View.GONE);
//                    calculate_button.setEnabled(true);
//                    final ArrayAdapter<String> battery_current_adapter=new ArrayAdapter<String>(getContext(),R.layout.spinner_items,battery_Ah_List);
//                    battery_current_adapter.setDropDownViewResource(R.layout.spinner_drop_down);
//                    battery_current_spinner.setAdapter(battery_current_adapter);
//                    battery_current_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                        @Override
//                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                            if (position == 0) {
//                                battery_current_spinner.setSelected(false);
//                                battery_warranty_spinner.setEnabled(false);
//                            } else {
//                                battery_warranty_spinner.setEnabled(true);
//                                selected_bettary_ah = battery_Ah_List.get(position);
//                                battery_warranty_spinner.setSelection(0);
//                                warrantyList.removeAll(warrantyList);
//                                warrantyList.add("Battery Warranty");
//                                for(int i = 0; i < databaseBackend.getFilterableBatteryWarranty(selected_bettary_ah).size();i++){
//                                    warrantyList.add(databaseBackend.getFilterableBatteryWarranty(selected_bettary_ah).get(i));
//                                }
//                            }
//                        }
//                        @Override
//                        public void onNothingSelected (AdapterView < ? > parent){
//                        }
//                    });
//                    battery_warranty_adapte = new ArrayAdapter<String>(getContext(), R.layout.spinner_items, warrantyList);
//                    battery_warranty_adapte.setDropDownViewResource(R.layout.spinner_drop_down);
//                    battery_warranty_spinner.setAdapter(battery_warranty_adapte);
//                    battery_warranty_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                        @Override
//                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                            if (position == 0) {
//                                battery_warranty_spinner.setSelected(false);
//
//                            } else {
//                                selected_warranty = warrantyList.get(position);
//                                refreshButton();
//                                int strproductid=databaseBackend.getFilterableProduct(selected_bettary_ah, selected_warranty).get(0).getId();
//                                Log.d("onselctId",""+strproductid);
//                                boolean flag=true;
//                                if (addedProductList.size()>0){
//
//                                    for (int i=0;i<addedProductList.size();i++) {
//                                        if (strproductid==addedProductList.get(i).getId()) {
//                                            flag=false;
//                                            int quantity = addedProductList.get(i).getQuantity();
//                                            addedProductList.get(i).setQuantity(quantity + 1);
//                                            break;
//                                        }else {
//                                            flag=true;
//                                        }
//                                    }
//                                    if (flag==true){
//                                        addedProductList.add(databaseBackend.getFilterableProduct(selected_bettary_ah, selected_warranty).get(0));
//                                        getActivity().runOnUiThread(new Runnable() {
//                                            @Override
//                                            public void run() {
//                                                loyaltyPointsProductAdapter.notifyDataSetChanged();
//                                            }
//                                        });
//                                    }
//                                    getActivity().runOnUiThread(new Runnable() {
//                                        @Override
//                                        public void run() {
//                                            loyaltyPointsProductAdapter.notifyDataSetChanged();
//                                            battery_current_spinner.setSelection(0);
//                                            battery_warranty_spinner.setSelection(0);
//                                        }
//                                    });
//                                }else {
//                                    Log.d("CheckAdapter","Adapter is null");
//                                    addedProductList.add(databaseBackend.getFilterableProduct(selected_bettary_ah, selected_warranty).get(0));
//                                    calculatormanager=new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
//                                    loyaltyPointsProductAdapter = new LoyaltyPointsProductAdapter(getContext(), addedProductList,Target_fragmnet.this,Target_fragmnet.this);
//                                    totalProdct_rv.setLayoutManager(calculatormanager);
//                                    totalProdct_rv.setAdapter(loyaltyPointsProductAdapter);
//                                    totalProdct_rv. setNestedScrollingEnabled(false);
//                                    totalProdct_rv.setHasFixedSize(true);
//                                    totalProdct_rv.setNestedScrollingEnabled(false);
//                                    ((Activity) getContext()).runOnUiThread(new Runnable() {
//                                        @Override
//                                        public void run() {
//                                            loyaltyPointsProductAdapter.notifyDataSetChanged();
//                                            battery_current_spinner.setSelection(0);
//                                            battery_warranty_spinner.setSelection(0);
//                                        }
//                                    });
//                                }
//                            }
//                        }
//                        @Override
//                        public void onNothingSelected(AdapterView<?> parent) {
//                        }
//                    });
//                    totalProdct_rv.setOnScrollChangeListener(new View.OnScrollChangeListener() {
//                        @Override
//                        public void onScrollChange(View view, int i, int i1, int i2, int i3) {
//                            try {
//                                int firstPos = calculatormanager.findFirstCompletelyVisibleItemPosition();
//                                if (firstPos > 0) {
//                                    swipeRefreshLayout.setEnabled(false);
//                                } else {
//                                    swipeRefreshLayout.setEnabled(true);
//                                    if(totalProdct_rv.getScrollState() == 1)
//                                        if(swipeRefreshLayout.isRefreshing())
//                                            totalProdct_rv.stopScroll();
//                                }
//                            }catch(Exception e) {
//                                Log.e(TAG, "Scroll Error : "+e.getLocalizedMessage());
//                            }
//                        }
//                    });
//                }
//            });
//        }
//        @RequiresApi(api = Build.VERSION_CODES.M)
//        @Override
//        public void onRequestError(int errorCode, String message) {
//            Log.d("response",message);
//            if (errorCode >= 400 && errorCode < 500) {
//                if (errorCode == 403){
//                    ((Activity)getContext()).runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            progressBar.setVisibility(View.GONE);
//                            Toast.makeText(getContext(), "UnAuthorised!", Toast.LENGTH_SHORT).show();
//                        }
//                    });
//                }
//                else if(errorCode==401) {
//                    ((Activity)getContext()).runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            progressBar.setVisibility(View.GONE);
////                            logout();
//                        }
//                    });
//                }
//                else {
//                    final ErrorResponseModel errorResponseModel = JsonUtils.objectify(message, ErrorResponseModel.class);
//                    ((Activity)getContext()).runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            progressBar.setVisibility(View.GONE);
//                            Toast.makeText(getContext(), errorResponseModel.getErr(), Toast.LENGTH_SHORT).show();
//                        }
//                    });
//                }
//            } else {
//                ((Activity)getContext()).runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        progressBar.setVisibility(View.GONE);
//                        Toast.makeText(getContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
//                    }
//                });
//            }
//        }
//    };
    RequestListener SchemeListner = new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @Override
        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
            Log.d("response getScheme", responseObject.toString());
            final String URl = responseObject.toString();
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                    pdf_button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            downloadManager = (DownloadManager) getActivity().getSystemService(Context.DOWNLOAD_SERVICE);
                            String s = URl.replaceAll(" ", "%20");
                            DownloadManager.Request request = new DownloadManager.Request(Uri.parse((s)));
                            String fileName = "LATEST OFFERS FOR IB+UPS";
                            request.setTitle(fileName);
                            request.setNotificationVisibility(1);
                            request.allowScanningByMediaScanner();
                            request.setMimeType("application/pdf");
                            request.setDestinationInExternalFilesDir(getContext(), Environment.DIRECTORY_DOWNLOADS, "LatestOffers.png");
                            request.setVisibleInDownloadsUi(true);
                            request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
                            //   request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                            refrence = downloadManager.enqueue(request);
                            Toast.makeText(getContext(), "DOWNLOADING", Toast.LENGTH_SHORT).show();

                        }
                    });
                }
            });

        }

        @Override
        public void onRequestError(int errorCode, String message) {

        }
    };
    RequestListener SchemeListner1 = new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @Override
        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
            Log.d("response getScheme1", responseObject.toString());
            final String URl = responseObject.toString();
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                    buttonpdf2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            downloadManager = (DownloadManager) getActivity().getSystemService(Context.DOWNLOAD_SERVICE);
                            String s = URl.replaceAll(" ", "%20");
                            DownloadManager.Request request = new DownloadManager.Request(Uri.parse((s)));
                            String fileName = "LATEST OFFERS FOR 4W";
                            request.setTitle(fileName);
                            request.setNotificationVisibility(1);
                            request.allowScanningByMediaScanner();
                            request.setMimeType("application/pdf");
                            request.setDestinationInExternalFilesDir(getContext(), Environment.DIRECTORY_DOWNLOADS, "LatestOffers.png");
                            request.setVisibleInDownloadsUi(true);
                            request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
                            //   request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                            refrence = downloadManager.enqueue(request);
                            Toast.makeText(getContext(), "DOWNLOADING", Toast.LENGTH_SHORT).show();

                        }
                    });
                }
            });
        }

        @Override
        public void onRequestError(int errorCode, String message) {

        }
    };
    RequestListener SchemeListner2 = new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @Override
        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
            Log.d("response getScheme2", responseObject.toString());
            final String URl = responseObject.toString();
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                    buttonpdf3.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            downloadManager = (DownloadManager) getActivity().getSystemService(Context.DOWNLOAD_SERVICE);
                            String s = URl.replaceAll(" ", "%20");
                            DownloadManager.Request request = new DownloadManager.Request(Uri.parse((s)));
                            String fileName = "LATEST OFFERS FOR E-RICK";
                            request.setTitle(fileName);
                            request.setNotificationVisibility(1);
                            request.allowScanningByMediaScanner();
                            request.setMimeType("application/pdf");
                            request.setDestinationInExternalFilesDir(getContext(), Environment.DIRECTORY_DOWNLOADS, "LatestOffers.png");
                            request.setVisibleInDownloadsUi(true);
                            request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
                            //   request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                            refrence = downloadManager.enqueue(request);
                            Toast.makeText(getContext(), "DOWNLOADING", Toast.LENGTH_SHORT).show();

                        }
                    });
                }
            });
        }

        @Override
        public void onRequestError(int errorCode, String message) {

        }
    };
    RequestListener SchemeListner3 = new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @Override
        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
            Log.d("response getScheme3", responseObject.toString());
            final String URl = responseObject.toString();
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                    buttonpdf4.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            downloadManager = (DownloadManager) getActivity().getSystemService(Context.DOWNLOAD_SERVICE);
                            String s = URl.replaceAll(" ", "%20");
                            DownloadManager.Request request = new DownloadManager.Request(Uri.parse((s)));
                            String fileName = "LATEST OFFERS FOR SECONDARY IB+UPS";
                            request.setTitle(fileName);
                            request.setNotificationVisibility(1);
                            request.allowScanningByMediaScanner();
                            request.setMimeType("application/pdf");
                            request.setDestinationInExternalFilesDir(getContext(), Environment.DIRECTORY_DOWNLOADS, "LatestOffers.png");
                            request.setVisibleInDownloadsUi(true);
                            request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
                            //   request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                            refrence = downloadManager.enqueue(request);
                            Toast.makeText(getContext(), "DOWNLOADING", Toast.LENGTH_SHORT).show();

                        }
                    });
                }
            });
        }

        @Override
        public void onRequestError(int errorCode, String message) {

        }
    };
    RequestListener SchemeListner5 = new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @Override
        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
            Log.d("response getScheme5", responseObject.toString());
            final String URl = responseObject.toString();
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                    buttonpdf5.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            downloadManager = (DownloadManager) getActivity().getSystemService(Context.DOWNLOAD_SERVICE);
                            String s = URl.replaceAll(" ", "%20");
                            DownloadManager.Request request = new DownloadManager.Request(Uri.parse((s)));
                            String fileName = "LATEST OFFERS FOR SECONDARY E-Rick";
                            request.setTitle(fileName);
                            request.setNotificationVisibility(1);
                            request.allowScanningByMediaScanner();
                            request.setMimeType("application/pdf");
                            request.setDestinationInExternalFilesDir(getContext(), Environment.DIRECTORY_DOWNLOADS, "LatestOffers.png");
                            request.setVisibleInDownloadsUi(true);
                            request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
                            //   request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                            refrence = downloadManager.enqueue(request);
                            Toast.makeText(getContext(), "DOWNLOADING", Toast.LENGTH_SHORT).show();

                        }
                    });
                }
            });
        }

        @Override
        public void onRequestError(int errorCode, String message) {

        }
    };
    private ScaleGestureDetector mScaleGestureDetector;
    private float mScaleFactor = 1.0f;

    @SuppressLint("ValidFragment")
    public Target_fragmnet(Context context1) {
        context = context1;
    }

    public Target_fragmnet() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.targets_fragment, container, false);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        battery_current_spinner = (Spinner) getView().findViewById(R.id.battery_current_spinner);
        battery_warranty_spinner = (Spinner) getView().findViewById(R.id.battery_warranty_spinner);
        purchase_date = (TextView) getView().findViewById(R.id.purchase_date);
        totalProdct_rv = (RecyclerView) getView().findViewById(R.id.product_list_rv);
        clear_all_tv = (TextView) getView().findViewById(R.id.clear_all_tv);
        calculate_button = (Button) getView().findViewById(R.id.calculate_button);
        empty_relative_layout = (RelativeLayout) getView().findViewById(R.id.empty_rl);
        calculater_RL = (RelativeLayout) getView().findViewById(R.id.calculater_RL);
        offer_RL = (RelativeLayout) getView().findViewById(R.id.offer_RL);
        pdfdownload = (RelativeLayout) getView().findViewById(R.id.pdfdownload);
        pdfLink = (TextView) getView().findViewById(R.id.pdflink);
//        pdflink1 = (TextView) getView().findViewById(R.id.pdflink1);
        pdflink2 = (TextView) getView().findViewById(R.id.pdflink2);
        pdflink3 = (TextView) getView().findViewById(R.id.pdflink3);
        pdflink5 = (TextView) getView().findViewById(R.id.pdflink5);
        swipeRefreshLayout = (SwipeRefreshLayout) getView().findViewById(R.id.swiperefresh);
        calculator_tv = (TextView) getView().findViewById(R.id.calculator_tv);
        offer_tv = (TextView) getView().findViewById(R.id.offer_tv);
        purchase_date.setOnClickListener(purchaseClick);
        scrollView = (ScrollView) getView().findViewById(R.id.scrollView);
        databaseBackend = DatabaseBackend.getInstance(getContext());
        calculate_button.setEnabled(false);
        target_rv = (RecyclerView) getView().findViewById(R.id.target_rv);
        progressBar = (ProgressBar) getView().findViewById(R.id.progressbar);
        pdf_button = (Button) getActivity().findViewById(R.id.buttonpdf);
        buttonpdf2 = (Button) getActivity().findViewById(R.id.buttonpdf2);
        buttonpdf3 = (Button) getActivity().findViewById(R.id.buttonpdf3);
        buttonpdf4 = (Button) getActivity().findViewById(R.id.buttonpdf4);
        buttonpdf5 = (Button) getActivity().findViewById(R.id.buttonpdf5);
        //change by mohit
        SpannableString content1 = new SpannableString("DOWNLOAD IB+UPS+4W TERTIARY SCHEME");
        content1.setSpan(new UnderlineSpan(), 0, content1.length(), 0);
        pdfLink.setText(content1);

//        SpannableString content2 = new SpannableString("DOWNLOAD 4W SECONDARY SCHEME");
//        content2.setSpan(new UnderlineSpan(), 0, content2.length(), 0);
//        pdflink1.setText(content2);

        SpannableString content3 = new SpannableString("DOWNLOAD E-RICK TERTIARY SCHEME");
        content3.setSpan(new UnderlineSpan(), 0, content3.length(), 0);
        pdflink2.setText(content3);

        SpannableString content4 = new SpannableString("DOWNLOAD IB+UPS+4W SECONDARY SCHEME");
        content4.setSpan(new UnderlineSpan(), 0, content4.length(), 0);
        pdflink3.setText(content4);

        SpannableString content5 = new SpannableString("DOWNLOAD E-RICK SECONDARY SCHEME");
        content5.setSpan(new UnderlineSpan(), 0, content5.length(), 0);
        pdflink5.setText(content5);

        targetsList = new ArrayList<TargetsModel>();
        productLoyalityList = new ArrayList<ProductLoyalityModel>();
        battery_Ah_List = new ArrayList<>();
        addedProductList = new ArrayList<>();
        progressBar.setVisibility(View.VISIBLE);
//        Controller.GetProductLoyality(getContext(),loyalityListner);
        // Controller.GetTargets(getContext(),targetsListner);
        Controller.getScheme(getContext(), SchemeListner);
        Controller.getScheme1(getContext(), SchemeListner1);
        Controller.getScheme2(getContext(), SchemeListner2);
        Controller.getScheme3(getContext(), SchemeListner3);
        Controller.getScheme5(getContext(), SchemeListner5);
        //change by mohit

        SpannableString content = new SpannableString("Clear all");
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        clear_all_tv.setText(content);
        productListForCalculateModelsList = new ArrayList<>();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                progressBar.setVisibility(View.GONE);
                pdf_button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Controller.getScheme(getContext(), SchemeListner);
                    }
                });
                buttonpdf2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Controller.getScheme1(getContext(), SchemeListner1);
                    }
                });
                buttonpdf3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Controller.getScheme2(getContext(), SchemeListner2);
                    }
                });
                buttonpdf4.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Controller.getScheme3(getContext(), SchemeListner3);
                    }
                });
                buttonpdf5.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Controller.getScheme5(getContext(), SchemeListner5);
                    }
                });
                // Controller.GetTargets(getContext(),targetsListner);
            }
        });
        targetAdapter = new TargetRecyclerViewAdapter(getContext(), targetsList);
        targetManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        target_rv.setLayoutManager(targetManager);
        target_rv.setAdapter(targetAdapter);
        target_rv.setNestedScrollingEnabled(false);
        target_rv.setHasFixedSize(true);
        final DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        final Date date = new Date();
        String todate = dateFormat.format(date);
        purchase_date.setText(todate);
        str_start_date = MyUtils.getYearlyFormat(purchase_date.getText().toString());
        warrantyList = new ArrayList<>();
        clear_all_tv.setOnClickListener(clear_all_click);
        calculate_button.setOnClickListener(calculateClick);
//        calculator_tv.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                calculater_RL.setVisibility(View.VISIBLE);
//                offer_RL.setVisibility(View.GONE);
//                pdfdownload.setVisibility(View.GONE);
//                //  empty_relative_layout.setVisibility(View.GONE);
//                calculator_tv.setBackgroundResource(R.drawable.left_red_rounded_box);
//                offer_tv.setBackgroundResource(R.drawable.righ_white_rounded_box);
//                calculator_tv.setTextColor(Color.WHITE);
//                offer_tv.setTextColor(Color.RED);
//            }
//        });
//        offer_tv.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                calculater_RL.setVisibility(View.GONE);
//                // offer_RL.setVisibility(View.VISIBLE);
//                pdfdownload.setVisibility(View.VISIBLE);
//                //  empty_relative_layout.setVisibility(View.VISIBLE);
//                offer_tv.setBackgroundResource(R.drawable.right_red_rounded_box);
//                calculator_tv.setBackgroundResource(R.drawable.left_white_rounded_box);
//                offer_tv.setTextColor(Color.WHITE);
//                calculator_tv.setTextColor(Color.RED);
//            }
//        });
//        scrollView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
//            @Override
//            public void onScrollChange(View view, int i, int i1, int i2, int i3) {
//                try {
//                    int firstPos = targetManager.findFirstCompletelyVisibleItemPosition();
//                    if (firstPos > 0) {
//                        swipeRefreshLayout.setEnabled(false);
//                    } else {
//                        swipeRefreshLayout.setEnabled(true);
//                        if (target_rv.getScrollState() == 1)
//                            if (swipeRefreshLayout.isRefreshing())
//                                target_rv.stopScroll();
//                    }
//                } catch (Exception e) {
//                    Log.e(TAG, "Scroll Error : " + e.getLocalizedMessage());
//                }
//            }
//        });
        battery_Ah_List.add("Battery Current");
        battery_warranty_spinner.setEnabled(false);
    }

    private void refreshButton() {
        calculate_button.setText("Calculate");
        calculate_button.setBackgroundResource(R.drawable.red_rounded_button_background);
        calculate_button.setTextColor(Color.WHITE);
        calculate_button.setClickable(true);
    }

    //    RequestListener targetsListner=new RequestListener() {
//        @Override
//        public void onRequestStarted() {
//        }
//        @Override
//        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
//            Log.d("response",responseObject.toString());
//            targetsList.clear();
//            Type collectionType = new TypeToken<List<TargetsModel>>() {}.getType();
//            List<TargetsModel> ca = (List<TargetsModel>) new Gson().fromJson( responseObject.toString(), collectionType);
//            targetsList.addAll(ca);
//            ((Activity)getContext()).runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    swipeRefreshLayout.setRefreshing(false);
//                    if (!targetsList.isEmpty()){
//                        empty_relative_layout.setVisibility(View.GONE);
//                    }
//                    progressBar.setVisibility(View.GONE);
//                    targetAdapter.notifyDataSetChanged();
//                }
//            });
//        }
//        @RequiresApi(api = Build.VERSION_CODES.M)
//        @Override
//        public void onRequestError(int errorCode, String message)  {
//            Log.d("response",message);
//            if (errorCode >= 400 && errorCode < 500) {
//                if (errorCode == 403){
//                    ((Activity)getContext()).runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            swipeRefreshLayout.setRefreshing(false);
//                            Toast.makeText(getContext(), "UnAuthorised!", Toast.LENGTH_SHORT).show();
//                        }
//                    });
//                }
//                else if(errorCode==401) {
//                    ((Activity)getContext()).runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            swipeRefreshLayout.setRefreshing(false);
////                            logout();
//                        }
//                    });
//                }
//                else {
//                    final ErrorResponseModel errorResponseModel = JsonUtils.objectify(message, ErrorResponseModel.class);
//                    ((Activity)getContext()).runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            swipeRefreshLayout.setRefreshing(false);
//                            Toast.makeText(getContext(), errorResponseModel.getErr(), Toast.LENGTH_SHORT).show();
//                        }
//                    });
//                }
//            } else {
//                ((Activity)getContext()).runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        swipeRefreshLayout.setRefreshing(false);
//                        Toast.makeText(getContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
//                    }
//                });
//            }
//        }
//    };
    @Override
    public void onHorizontalNumberPickerChanged(HorizontalNumberPicker horizontalNumberPicker, int id, int value) {
        refreshButton();
        for (int i = 0; i < addedProductList.size(); i++) {
            if (id == addedProductList.get(i).getId()) {
                addedProductList.get(i).setQuantity(value);
                break;
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshButton();
        battery_current_spinner.setSelection(0);
        battery_warranty_spinner.setSelection(0);
    }

    @Override
    public void onItemRemove(boolean result) {
        if (result = true) {
            refreshButton();
        }
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                loyaltyPointsProductAdapter.notifyDataSetChanged();
            }
        });
    }
}
