package com.sar.user.livfastRewards.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sar.user.livfastRewards.Interface.RequestListener;
import com.sar.user.livfastRewards.Model.ProductWarrantyDetailModel;
import com.sar.user.livfastRewards.R;
import com.sar.user.livfastRewards.adapter.WarrantyRecyclerViewAdapter;
import com.sar.user.livfastRewards.helper.Controller;
import com.sar.user.livfastRewards.helper.MyUtils;
import com.sar.user.livfastRewards.helper.PrefManager;

import org.json.JSONException;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Nav on 26-Jun-17.
 */

public class LogsWarrantyFragment extends BaseFragment {
    Context context;
    RecyclerView warranty_rv;
    WarrantyRecyclerViewAdapter warrantyAdapter;
    List<ProductWarrantyDetailModel> warrantylist;
    TextView startDate_tv, endDate_tv;
    Calendar calendar = Calendar.getInstance();
    int final_end_year = calendar.get(Calendar.YEAR);
    int final_end_month = calendar.get(Calendar.MONTH);
    int final_end_day = calendar.get(Calendar.DAY_OF_MONTH);

    int final_start_day = calendar.get(Calendar.DAY_OF_MONTH) - 7;
    int final_start_month = calendar.get(Calendar.MONTH);
    int final_start_year = calendar.get(Calendar.YEAR);
    SwipeRefreshLayout swipeRefreshLayout;
    RelativeLayout empty_rl;
    String str_start_date;
    String str_end_date;
    RequestListener warrantyListner = new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @Override
        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
            Log.d("response GetWarranty", responseObject.toString());
            warrantylist.clear();
            Type collectionType = new TypeToken<List<ProductWarrantyDetailModel>>() {
            }.getType();
            List<ProductWarrantyDetailModel> ca = (List<ProductWarrantyDetailModel>) new Gson().fromJson(responseObject.toString(), collectionType);
            warrantylist.addAll(ca);
            if ((Activity) getContext() != null) {
                ((Activity) getContext()).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (warrantylist.isEmpty()) {
                            empty_rl.setVisibility(View.VISIBLE);
                            warranty_rv.setVisibility(View.GONE);
                        } else {
                            empty_rl.setVisibility(View.GONE);
                            warranty_rv.setVisibility(View.VISIBLE);
                        }
                        warrantyAdapter.notifyDataSetChanged();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                });
            }
        }

        @Override
        public void onRequestError(int errorCode, String message) {

            if ((Activity) getContext() != null) {
                ((Activity) getContext()).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefreshLayout.setRefreshing(false);
                    }
                });
            }

            handelError(errorCode, message);
        }
    };

    @SuppressLint({"ValidFragmemt", "ValidFragment"})
    public LogsWarrantyFragment(Context context1) {
        context = context1;
    }

    public LogsWarrantyFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.logs_warranty_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        warranty_rv = (RecyclerView) getView().findViewById(R.id.warranty_rv);
        startDate_tv = (TextView) getView().findViewById(R.id.startDate);
        endDate_tv = (TextView) getView().findViewById(R.id.endDate);
        swipeRefreshLayout = (SwipeRefreshLayout) getView().findViewById(R.id.swiperefresh);
        empty_rl = (RelativeLayout) getView().findViewById(R.id.empty_rl);
        final DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        final Date date = new Date();
        String todate = dateFormat.format(date);
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, -7);
        Date todate1 = cal.getTime();
        String fromdate = dateFormat.format(todate1);
        startDate_tv.setText(fromdate);
        endDate_tv.setText(todate);
        str_start_date = MyUtils.getYearlyFormat(startDate_tv.getText().toString());

        str_end_date = MyUtils.getYearlyFormat(endDate_tv.getText().toString());

        startDate_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final com.wdullaer.materialdatetimepicker.date.DatePickerDialog dpd = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance(new com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(com.wdullaer.materialdatetimepicker.date.DatePickerDialog view, final int year, final int monthOfYear, final int dayOfMonth) {

                        final String select_date = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;

                        ((Activity) getContext()).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

                                    Date date1 = formatter.parse(select_date);
                                    Date date2 = formatter.parse(MyUtils.getslashFormat(str_end_date));

                                    if (date1.compareTo(date2) <= 0) {
                                        final_start_day = dayOfMonth;
                                        final_start_month = monthOfYear + 1;
                                        final_start_year = year;
                                        startDate_tv.setText(MyUtils.getdaydashFormat(select_date));
                                        str_start_date = MyUtils.getYearlydashFormat(select_date);

                                        Log.d("ComparisonOfdate", "-----" + date1 + "is less Than---" + date2);

                                        Controller.GetWarranty(getContext(), MyUtils.getYearlydashFormat(select_date), str_end_date, PrefManager.getInstance(getContext()).getÜserId(), warrantyListner);
                                    } else if (date1.compareTo(date2) > 0) {
                                        Log.d("ComparisonOfdate", "-----" + date1 + "is greater than---" + date2);
                                        Toast.makeText(getContext(), "Start Date can not be greater than End Date", Toast.LENGTH_SHORT).show();
                                    }

                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                    }
                }, final_start_year, final_start_month, final_start_day);
                dpd.setMaxDate(calendar);
                dpd.show(getActivity().getFragmentManager(), "DATE_PICKER_TAG");
            }
        });
        endDate_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final com.wdullaer.materialdatetimepicker.date.DatePickerDialog dpd = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance(new com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(com.wdullaer.materialdatetimepicker.date.DatePickerDialog view, final int year, final int monthOfYear, final int dayOfMonth) {

                        final String select_date = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;

                        ((Activity) getContext()).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

                                    Date date1 = formatter.parse(MyUtils.getslashFormat(str_start_date));
                                    Date date2 = formatter.parse(select_date);

                                    if (date1.compareTo(date2) <= 0) {
                                        Log.d("ComparisonOfdate", "-----" + date1 + "is less Than---" + date2);
                                        final_end_day = dayOfMonth;
                                        final_end_month = monthOfYear + 1;
                                        final_end_year = year;
                                        endDate_tv.setText(MyUtils.getdaydashFormat(select_date));
                                        str_end_date = MyUtils.getYearlydashFormat(select_date);
                                        Controller.GetWarranty(getContext(), str_start_date, MyUtils.getYearlydashFormat(select_date), PrefManager.getInstance(getContext()).getÜserId(), warrantyListner);
                                    } else if (date1.compareTo(date2) > 0) {
                                        Log.d("ComparisonOfdate", "-----" + date1 + "is greater than---" + date2);
                                        Toast.makeText(getContext(), "End Date can not be Less than Start Date", Toast.LENGTH_SHORT).show();
                                    }

                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                    }
                }, final_end_year, final_end_month, final_end_day);

                dpd.setMaxDate(calendar);
                dpd.show(getActivity().getFragmentManager(), "DATE_PICKER_TAG");
            }
        });
        warrantylist = new ArrayList<ProductWarrantyDetailModel>();
        Controller.GetWarranty(getContext(), str_start_date, str_end_date, PrefManager.getInstance(getContext()).getÜserId(), warrantyListner);
        warrantyAdapter = new WarrantyRecyclerViewAdapter(getContext(), warrantylist);
        warranty_rv.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        warranty_rv.setAdapter(warrantyAdapter);
        warranty_rv.setHasFixedSize(true);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Controller.GetWarranty(getContext(), str_start_date, str_end_date, PrefManager.getInstance(getContext()).getÜserId(), warrantyListner);
            }
        });
    }
}
