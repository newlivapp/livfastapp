package com.sar.user.livfastRewards.Model;

import android.content.ContentValues;
import android.database.Cursor;

import com.sar.user.livfastRewards.Database.AbstractEntry;

public class ProductListModel extends AbstractEntry {

    public static final String TABLE_NAME = "Added_Product_Table";
    public static final String UNIQUE_CODE = "unique_code";
    public static final String COMMENT = "comment";
    public static final String PRODUCT_IMAGE_PATH = "product_image_path";

    String unique_code, comment, product_image_path;

    public ProductListModel(String unique_code, String comment, String product_image_path) {
        this.unique_code = unique_code;
        this.comment = comment;
        this.product_image_path = product_image_path;
    }

    public static ProductListModel fromCursor(Cursor cursor) {
        return new ProductListModel(cursor.getString(cursor.getColumnIndex(UNIQUE_CODE)),
                cursor.getString(cursor.getColumnIndex(COMMENT)),
                cursor.getString(cursor.getColumnIndex(PRODUCT_IMAGE_PATH)));
    }

    public String getUnique_code() {
        return unique_code;
    }

    public void setUnique_code(String unique_code) {
        this.unique_code = unique_code;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getProduct_image_path() {
        return product_image_path;
    }

    public void setProduct_image_path(String product_image_path) {
        this.product_image_path = product_image_path;
    }

    @Override
    public ContentValues getContentValues() {
        ContentValues values = new ContentValues();
        values.put(UNIQUE_CODE, getUnique_code());
        values.put(COMMENT, getComment());
        values.put(PRODUCT_IMAGE_PATH, getProduct_image_path());
        return values;
    }
}
