package com.sar.user.livfastRewards.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.sar.user.livfastRewards.Interface.RequestListener;
import com.sar.user.livfastRewards.Model.LoginModel;
import com.sar.user.livfastRewards.Model.LoginResponseModel;
import com.sar.user.livfastRewards.R;
import com.sar.user.livfastRewards.helper.Controller;
import com.sar.user.livfastRewards.helper.JsonUtils;
import com.sar.user.livfastRewards.helper.PrefManager;

import org.json.JSONException;

import java.text.ParseException;

public class BatCodeAvtivity extends BaseActivity {
    TextView bat_code_msg, main_heading;
    Button confirm_button;
    EditText batCode;
    Context mContext;
    ProgressBar progressBar;
    PrefManager pref;

    private boolean isFirstTimeUser = false;
    private String forAction = "";
    RequestListener firstTimeListner = new RequestListener() {
        @Override
        public void onRequestStarted() {
        }

        @SuppressLint("LongLogTag")
        @Override
        public void onRequestCompleted(final Object responseObject) throws JSONException, ParseException {
            Log.d("response firstTimeListner", responseObject.toString());
            LoginResponseModel loginResponceModel = JsonUtils.objectify(responseObject.toString(), LoginResponseModel.class);

            if (loginResponceModel == null || loginResponceModel.getData() == null || loginResponceModel.getData().getUsername().isEmpty()) {
                runOnUiThread(new Thread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(mContext, responseObject.toString(), Toast.LENGTH_SHORT).show();
                    }
                }));

                return;
            }

            if (responseObject.toString().equals("User Does Not Exist With Us !")) {
                runOnUiThread(new Thread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(mContext, "User Does Not Exist ! Please Do First Time Registration", Toast.LENGTH_SHORT).show();
                    }
                }));

            } else {

                pref.saveUserInfo(loginResponceModel);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                        Intent intent = new Intent(mContext, Change_password_activity.class);
                        intent.putExtra("for_action", forAction);
                        startActivity(intent);
                    }
                });

            }
        }

        @Override
        public void onRequestError(int errorCode, String message) {
            handelError(errorCode, message);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                }
            });
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bat_code_avtivity);

        mContext = this;

        initControl();
        pref = PrefManager.getInstance(mContext);

        if (getIntent().getStringExtra("for_action") != null && getIntent().getExtras().containsKey("for_action")) {
            forAction = getIntent().getStringExtra("for_action");

            if (forAction.equalsIgnoreCase("user_first_time"))
                isFirstTimeUser = true;
        } else {
            finish();
        }

        if (isFirstTimeUser) {
            bat_code_msg.setText("Enter your BAT Code to get started");
            main_heading.setText("Welcome Partner");
        } else {
            bat_code_msg.setText("Enter the BAT Code associated with" + "\n" + "your account & we'll send you an OTP on" + "\n" + "your registered no. to reset your" + "\n" + "password.");
            main_heading.setText("Forgot Password");
        }

        confirm_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String strBatCode = batCode.getText().toString().trim().toUpperCase();

                if (strBatCode.equals("")) {
                    batCode.setError("Enter Batt Code");
                } else {
                    LoginModel loginModel = new LoginModel();
                    loginModel.setBat_code(strBatCode);

                    if (isFirstTimeUser) {
                        loginModel.setJob_Type("Register");
                    } else {
                        loginModel.setJob_Type("Forgot");
                    }

                    progressBar.setVisibility(View.VISIBLE);
                    Controller.manualLogin(mContext, loginModel, firstTimeListner);
                }
            }
        });
    }

    private void initControl() {
        bat_code_msg = (TextView) findViewById(R.id.bat_code_msg);
        confirm_button = (Button) findViewById(R.id.confirm_button);
        main_heading = (TextView) findViewById(R.id.main_heading);
        progressBar = (ProgressBar) findViewById(R.id.progressbar);
        batCode = (EditText) findViewById(R.id.bat_code);
    }


}
