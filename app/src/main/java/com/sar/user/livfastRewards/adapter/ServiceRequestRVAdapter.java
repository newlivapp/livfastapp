package com.sar.user.livfastRewards.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sar.user.livfastRewards.Interface.FeedbackListner;
import com.sar.user.livfastRewards.Model.GetServiceRequestModel;
import com.sar.user.livfastRewards.R;
import com.sar.user.livfastRewards.helper.MyUtils;

import java.util.List;

/**
 * Created by Nav on 01-Aug-17.
 */

public class ServiceRequestRVAdapter extends RecyclerView.Adapter<ServiceRequestRVAdapter.MyViewHolder> {
    List<GetServiceRequestModel> serviceList;
    Context context;
    int id;
    FeedbackListner feedbackListner;

    public ServiceRequestRVAdapter(Context context1, List<GetServiceRequestModel> serviceList1, FeedbackListner feedbackListner1) {
        context = context1;
        serviceList = serviceList1;
        this.feedbackListner = feedbackListner1;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.service_request_rv_item, parent, false);
        MyViewHolder mvh = new MyViewHolder(v);
        return mvh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.date.setText(MyUtils.getValidDateFormat(serviceList.get(position).getCreated_timestamp()));
        holder.ticket_no.setText(serviceList.get(position).getTicket_no());

        holder.status.setText(serviceList.get(position).getTicket_state());
        if (serviceList.get(position).getTicket_state().equals("closed")) {
            holder.status.setClickable(true);
        } else {
            holder.status.setClickable(false);
        }

        if (serviceList.get(position).getTicket_state().equals("open")) {
            holder.status.setTextColor(Color.parseColor("#6FCF97"));
        } else if (serviceList.get(position).getTicket_state().equals("open & late")) {
            holder.status.setTextColor(Color.parseColor("#F2994A"));
        }
        if (serviceList.get(position).getTicket_state().equals("closed")) {
            SpannableString content = new SpannableString("closed");
            content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
            holder.status.setText(content);
            holder.status.setTextColor(Color.parseColor("#106836"));
            holder.status.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    feedbackListner.onFeedBackClick(serviceList.get(position).getCustomer_mobile(),
                            serviceList.get(position).getBattery_status(),
                            serviceList.get(position).getTicket_no(),
                            serviceList.get(position).getSerial(),
                            serviceList.get(position).getCta_status(),
                            serviceList.get(position).getCustomer_name(),
                            serviceList.get(position).getId());
                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return serviceList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView date, ticket_no, status;

        public MyViewHolder(View itemView) {
            super(itemView);
            date = (TextView) itemView.findViewById(R.id.date);
            ticket_no = (TextView) itemView.findViewById(R.id.ticket_no);
            status = (TextView) itemView.findViewById(R.id.status);
        }
    }
}
