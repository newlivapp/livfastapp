package com.sar.user.livfastRewards.Model;

/**
 * Created by Nav on 08-Aug-17.
 */

public class RedeemSlabModel {

    String user_id;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    int id, loyalty_point, battery_scheme_id;
    boolean is_active, isSelected;
    String created_timestamp, updated_timestamp, scheme_type, redeem_desc;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLoyalty_point() {
        return loyalty_point;
    }

    public void setLoyalty_point(int loyalty_point) {
        this.loyalty_point = loyalty_point;
    }

    public boolean is_active() {
        return is_active;
    }

    public String getCreated_timestamp() {
        return created_timestamp;
    }

    public void setCreated_timestamp(String created_timestamp) {
        this.created_timestamp = created_timestamp;
    }

    public String getUpdated_timestamp() {
        return updated_timestamp;
    }

    public void setUpdated_timestamp(String updated_timestamp) {
        this.updated_timestamp = updated_timestamp;
    }

    public int getBattery_scheme_id() {
        return battery_scheme_id;
    }

    public void setBattery_scheme_id(int battery_scheme_id) {
        this.battery_scheme_id = battery_scheme_id;
    }

    public boolean isIs_active() {
        return is_active;
    }

    public void setIs_active(boolean is_active) {
        this.is_active = is_active;
    }

    public String getScheme_type() {
        return scheme_type;
    }

    public void setScheme_type(String scheme_type) {
        this.scheme_type = scheme_type;
    }

    public String getRedeem_desc() {
        return redeem_desc;
    }

    public void setRedeem_desc(String redeem_desc) {
        this.redeem_desc = redeem_desc;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
