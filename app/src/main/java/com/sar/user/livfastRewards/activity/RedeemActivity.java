package com.sar.user.livfastRewards.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sar.user.livfastRewards.Interface.RequestListener;
import com.sar.user.livfastRewards.Model.FinalPointsRedeemModel;
import com.sar.user.livfastRewards.Model.RedeemPointsAPIModel;
import com.sar.user.livfastRewards.Model.RedeemSlabModel;
import com.sar.user.livfastRewards.Model.SelectRedeemModel;
import com.sar.user.livfastRewards.R;
import com.sar.user.livfastRewards.adapter.CouponAdapter;
import com.sar.user.livfastRewards.helper.Controller;
import com.sar.user.livfastRewards.helper.JsonUtils;
import com.sar.user.livfastRewards.helper.PrefManager;
import com.sar.user.livfastRewards.listener.RecyclerItemClickListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class RedeemActivity extends BaseActivity {
    final int SCREEN_IB_UPS = 1;
    final int SCREEN_FOUR_W = 2;
    final int SCREEN_E_RICK = 3;
    final int SCREEN_E_RICK_PR = 4;
    List<RedeemSlabModel> choice_list = new ArrayList<>();
    TextView view_redeem_history, total_purchase_points, tvTitle;
    Toolbar toolbar;
    ImageView back_button;
    ProgressBar progressBar;
    Button redeem_Button, btnCouponWallet;
    int loyalty_points;
    RelativeLayout empty_rl, redeem_slab_rl;
    Double points_redeem = 0.0;
    int scheme_id = 0;
    CouponAdapter couponAdapter;
    RecyclerView recyclerView;
    LinearLayoutManager linearLayoutManager;
    Dialog dialog;
    int SCREEN = 0;
    RedeemPointsAPIModel responseRedeemPointsModel;
    View.OnClickListener historyDetail = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent inten = new Intent(RedeemActivity.this, RedeemHistoryActivity.class);
            inten.putExtra("screen", SCREEN);
            startActivity(inten);
        }
    };
    RequestListener GetRedeemPointsRequestListener = new RequestListener() {
        @Override
        public void onRequestStarted() {
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
            if (SCREEN == SCREEN_IB_UPS)
                Log.d("responce GetRedeemSlab", responseObject.toString());
            else if (SCREEN == SCREEN_FOUR_W)
                Log.d("responce Get4wSlab", responseObject.toString());
            else if (SCREEN == SCREEN_E_RICK)
                Log.d("responce GeterickSlab", responseObject.toString());
            else if (SCREEN == SCREEN_E_RICK_PR)
                Log.d("responce GeterickSlabPR", responseObject.toString());

            choice_list.clear();
            Type collection = new TypeToken<List<RedeemSlabModel>>() {
            }.getType();
            final List<RedeemSlabModel> rsm = (List<RedeemSlabModel>) new Gson().fromJson(responseObject.toString(), collection);
            choice_list.addAll(rsm);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                    if (choice_list.isEmpty()) {
                        empty_rl.setVisibility(View.VISIBLE);
                        redeem_slab_rl.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.GONE);
                    } else {
                        empty_rl.setVisibility(View.GONE);
                        redeem_slab_rl.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.VISIBLE);
                        if (couponAdapter == null)
                            setupRecyclerView();
                        else
                            couponAdapter.notifyDataSetChanged();
                    }
                }
            });
        }

        @Override
        public void onRequestError(int errorCode, String message) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                }
            });
            handelError(errorCode, message);
        }
    };
    RequestListener APIRedeemPointsRequestListener = new RequestListener() {
        @Override
        public void onRequestStarted() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.VISIBLE);
                }
            });

        }

        @SuppressLint("LongLogTag")
        @Override
        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
            Log.d("response apiRedeemPoints", responseObject.toString());
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    redeem_Button.setEnabled(true);
                }
            });

            //{"staus":"Failed","msg":"User doesnt have any points "}
            if (responseObject == null || responseObject.toString().isEmpty()) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                    }
                });
                showToast("Somethings is wrong please try again.");
                return;
            }

            try {
                JSONObject jsonObject = new JSONObject(responseObject.toString());
                if (jsonObject.has("status")) {
                    String status = jsonObject.getString("status");
                    String msg = jsonObject.getString("msg");
                    if (status == null || status.isEmpty() || status.equalsIgnoreCase("Failed")) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressBar.setVisibility(View.GONE);
                            }
                        });
                        if (msg == null || msg.isEmpty())
                            msg = "Somethings is wrong please try again.";

                        showToast(msg);
                        return;
                    }
                }


                responseRedeemPointsModel = JsonUtils.objectify(responseObject.toString(), RedeemPointsAPIModel.class);

                if (responseRedeemPointsModel == null || responseRedeemPointsModel.getId().isEmpty()) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(RedeemActivity.this, "Somethings is wrong, Please try again later.", Toast.LENGTH_SHORT).show();
                            progressBar.setVisibility(View.GONE);
                        }
                    });
                    return;
                }


                //Show Popup for OTP
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                        dialog = new Dialog(RedeemActivity.this);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.otp_massage_popup);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                        final TextView msgText = (TextView) dialog.findViewById(R.id.msg_text);
                        final ImageView dismiss = (ImageView) dialog.findViewById(R.id.dissmiss);
                        final TextView phone_no_msg = (TextView) dialog.findViewById(R.id.phone_no_massage);
                        final EditText otp_text = (EditText) dialog.findViewById(R.id.otp_text);
                        final TextView resendOtp_tv = (TextView) dialog.findViewById(R.id.resend_otp_tv);
                        SpannableString content = new SpannableString("RE-Send OTP");
                        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
                        resendOtp_tv.setText(content);
                        resendOtp_tv.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                FinalPointsRedeemModel finalPointsRedeemModel = new FinalPointsRedeemModel();
                                finalPointsRedeemModel.setId(PrefManager.getInstance(RedeemActivity.this).getÜserId());
                                finalPointsRedeemModel.setResend_otp(1);
                                progressBar.setVisibility(View.VISIBLE);
                                Controller.TotalPointsRedeem(RedeemActivity.this, PrefManager.getInstance(RedeemActivity.this).getÜserId(), finalPointsRedeemModel, new RequestListener() {
                                    @Override
                                    public void onRequestStarted() {

                                    }

                                    @SuppressLint("LongLogTag")
                                    @Override
                                    public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
                                        Log.d("response TotalPointsRedeem", responseObject.toString());
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(RedeemActivity.this, "OTP sent successfully", Toast.LENGTH_SHORT).show();
                                                progressBar.setVisibility(View.GONE);
                                            }
                                        });
                                    }

                                    @Override
                                    public void onRequestError(int errorCode, String message) {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                progressBar.setVisibility(View.GONE);
                                            }
                                        });
                                        handelError(errorCode, message);
                                    }
                                });
                            }
                        });


                        phone_no_msg.setText("Enter OTP send to +91" + PrefManager.getInstance(RedeemActivity.this).getPhoneNo());

                        msgText.setVisibility(View.GONE);
                        final Button verify = (Button) dialog.findViewById(R.id.verify_button);
                        verify.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                FinalPointsRedeemModel finalPointsRedeemModel = new FinalPointsRedeemModel();
                                finalPointsRedeemModel.setUser_id(PrefManager.getInstance(RedeemActivity.this).getÜserId());
                                finalPointsRedeemModel.setUsername(PrefManager.getInstance(RedeemActivity.this).getBatCode());
                                finalPointsRedeemModel.setId(responseRedeemPointsModel.getId());
                                finalPointsRedeemModel.setOtp(otp_text.getText().toString());
                                finalPointsRedeemModel.setLoyalty_point(String.valueOf(loyalty_points));

                                Controller.APIRedeemConfirmationPoints(RedeemActivity.this, finalPointsRedeemModel, new RequestListener() {
                                    @Override
                                    public void onRequestStarted() {

                                    }

                                    @SuppressLint("LongLogTag")
                                    @Override
                                    public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
                                        Log.d("response APIRedeemConfirmationPoints", responseObject.toString());
                                        handleOTPConfirmResop(responseObject.toString());
                                    }

                                    @Override
                                    public void onRequestError(int errorCode, String message) {
                                        handleOTPConfirmResop(message);
                                    }
                                });
                            }
                        });
                        dismiss.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                dialog.cancel();
                            }
                        });
                        if (RedeemActivity.this != null)
                            RedeemActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (!(RedeemActivity.this).isFinishing()) {
                                        try {
                                            dialog.show();
                                            dialog.setCancelable(false);
                                        } catch (WindowManager.BadTokenException e) {
                                            Log.e("WindowManagerBad ", e.toString());
                                        } catch (Exception e) {
                                            Log.e("Exception ", e.toString());
                                        }
                                    }
                                }
                            });
                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onRequestError(int errorCode, String message) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                    redeem_Button.setEnabled(true);
                }
            });
            handelError(errorCode, message);
        }
    };
    View.OnClickListener redeemClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            redeem_Button.setEnabled(false);

            progressBar.setVisibility(View.VISIBLE);
            //Validation selected point is not grater than user point
            if (loyalty_points == 0) {
                loyalty_points = 0;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                        redeem_Button.setEnabled(true);
                        Toast.makeText(RedeemActivity.this, "Select at least any one redeem slab.", Toast.LENGTH_SHORT).show();
                    }
                });
                return;
            }

            PrefManager prefManager = PrefManager.getInstance(RedeemActivity.this);

            //Call Api to send OTP for final confirmation
            SelectRedeemModel selectRedeemModel = new SelectRedeemModel();
            selectRedeemModel.setUser_id(prefManager.getÜserId());
            selectRedeemModel.setUsername(prefManager.getBatCode());
            selectRedeemModel.setMobile_number(prefManager.getPhoneNo());
            selectRedeemModel.setLoyalty_point(String.valueOf(loyalty_points));
            selectRedeemModel.setScheme_id(String.valueOf(scheme_id));
            if (SCREEN == SCREEN_IB_UPS)
                selectRedeemModel.setCategory("IB(Tertiary)");
            else if (SCREEN == SCREEN_FOUR_W)
                selectRedeemModel.setCategory("4W(Tertiary)");
            else if (SCREEN == SCREEN_E_RICK)
                selectRedeemModel.setCategory("ERICK(Tertiary)");
            progressBar.setVisibility(View.VISIBLE);
            Controller.apiRedeemPoints(RedeemActivity.this, selectRedeemModel, APIRedeemPointsRequestListener);
        }
    };
    View.OnClickListener backClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_redeem);

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("screen"))
            SCREEN = getIntent().getExtras().getInt("screen");

        initControl();
        setupRecyclerView();


        //set title based on screen
        if (SCREEN == SCREEN_IB_UPS) {
            btnCouponWallet.setVisibility(View.VISIBLE);
            tvTitle.setText("IB+UPS+4W Redeem");
        } else if (SCREEN == SCREEN_FOUR_W) {
            btnCouponWallet.setVisibility(View.GONE);
            tvTitle.setText("4W Redeem");
            view_redeem_history.setVisibility(View.GONE);
        } else if (SCREEN == SCREEN_E_RICK) {
            btnCouponWallet.setVisibility(View.GONE);
            tvTitle.setText("E-RICK Redeem");
        } else if (SCREEN == SCREEN_E_RICK_PR) {
            view_redeem_history.setVisibility(View.GONE);
            btnCouponWallet.setVisibility(View.GONE);
            tvTitle.setText("E-RICK Redeem");
        } else { //if there is no screen found than close this
            finish();
        }

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("points")) {
            String strPoints = getIntent().getStringExtra("points");
            if (strPoints != null && !strPoints.isEmpty()) {
                points_redeem = Double.parseDouble(strPoints);
            }
        }

        total_purchase_points.setText("" + points_redeem);

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        // do whatever
                        if (position >= 0 && position < choice_list.size()) {
                        } else {
                            return;
                        }
                        RedeemSlabModel redeemSlabModel = choice_list.get(position);
                        if (redeemSlabModel == null)
                            return;

                        loyalty_points = redeemSlabModel.getLoyalty_point();

                        if (loyalty_points == 0 || loyalty_points > points_redeem) {
                            loyalty_points = 0;
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(RedeemActivity.this, "You don't have enough points for redeem.", Toast.LENGTH_SHORT).show();
                                }
                            });
                            return;
                        }

                        for (RedeemSlabModel re : choice_list)
                            re.setSelected(false);

                        scheme_id = redeemSlabModel.getId();
                        redeemSlabModel.setSelected(true);
                        couponAdapter.notifyDataSetChanged();
                    }
                })
        );

        if (SCREEN == SCREEN_IB_UPS)
            Controller.GetRedeemSlab(RedeemActivity.this, GetRedeemPointsRequestListener);
        else if (SCREEN == SCREEN_FOUR_W)
            Controller.Get4wSlab(RedeemActivity.this, GetRedeemPointsRequestListener);
        else if (SCREEN == SCREEN_E_RICK)
            Controller.GeterickSlab(RedeemActivity.this, GetRedeemPointsRequestListener);
        else if (SCREEN == SCREEN_E_RICK_PR)
            Controller.GeterickSlabPR(RedeemActivity.this, GetRedeemPointsRequestListener);
    }

    private void initControl() {
        toolbar = findViewById(R.id.toolbar);
        back_button = toolbar.findViewById(R.id.back_button);
        tvTitle = toolbar.findViewById(R.id.toolbar_title);
        back_button.setOnClickListener(backClick);
        total_purchase_points = findViewById(R.id.total_purchase_points);
        redeem_Button = findViewById(R.id.redeem_button);
        //redeem_Button.setOnClickListener(redeemClick);
        empty_rl = findViewById(R.id.empty_rl);
        redeem_slab_rl = findViewById(R.id.loyalty_points_slab);
        view_redeem_history = findViewById(R.id.view_redeem_history);
        SpannableString content = new SpannableString("View Redeem History");
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        view_redeem_history.setText(content);
        view_redeem_history.setOnClickListener(historyDetail);
        progressBar = findViewById(R.id.progressbar);

        recyclerView = findViewById(R.id.recyclerView);
        btnCouponWallet = findViewById(R.id.btnCouponWallet);
        btnCouponWallet.setVisibility(View.GONE);
        btnCouponWallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RedeemActivity.this, ScratchListActivity.class);
                startActivity(intent);
            }
        });
    }

    private void setupRecyclerView() {
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        couponAdapter = new CouponAdapter(this, choice_list);
        recyclerView.setAdapter(couponAdapter);
        recyclerView.invalidate();
    }

    private void handleOTPConfirmResop(String jsonStr) {
        if (jsonStr == null || jsonStr.isEmpty()) {
            showToast("Somethings is wrong please try again.");
            return;
        }

        try {
            JSONObject jsonObject = new JSONObject(jsonStr);
            String status = jsonObject.getString("status");
            String msg = jsonObject.getString("msg");
            if (status == null || status.isEmpty() || status.equalsIgnoreCase("Failed")) {
                if (msg == null || msg.isEmpty())
                    msg = "Somethings is wrong please try again.";

                showToast(msg);
                return;
            } else {
                if (msg == null || msg.isEmpty())
                    msg = "OTP Confirm successfully.";

                showToast(msg);
            }
            if (dialog != null)
                dialog.dismiss();
            //Success here you can navigate to home screen
            finish();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void showToast(final String massage) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(RedeemActivity.this, massage, Toast.LENGTH_SHORT).show();
            }
        });
    }
    //9910278353
}
