package com.sar.user.livfastRewards.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.sar.user.livfastRewards.R;

public class Service_request_activity extends AppCompatActivity {
    EditText customer_name, mobile_no;
    TextView city_tv;
    Button next;
    Toolbar toolbar;
    ImageView back_button;
    String strCutomerName, strContactNo;
    int city_id;
    String cityName;
    View.OnClickListener cityclick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(Service_request_activity.this, SearchCityActivity.class);
            intent.putExtra("name", customer_name.getText().toString());
            intent.putExtra("mobileno", mobile_no.getText().toString());
            startActivity(intent);
            finish();
        }
    };
    View.OnClickListener nextClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            strCutomerName = customer_name.getText().toString();
            strContactNo = mobile_no.getText().toString();
            try {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            } catch (Exception e) {
            }
            Log.d("CustomerName", strCutomerName);
            if (strCutomerName.equals("")) {
                final Snackbar snackbar;
                snackbar = Snackbar.make(customer_name, "Please enter Cutomer name", Snackbar.LENGTH_SHORT);
                View snackBarView = snackbar.getView();
                snackBarView.layout(20, 20, 20, 20);
                snackBarView.setBackgroundColor(Color.RED);
                TextView textView = (TextView) snackBarView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(Color.WHITE);
                snackBarView.setMinimumHeight(103);
                textView.setGravity(Gravity.CENTER);
                snackbar.setActionTextColor(Color.WHITE);
                snackbar.show();
            } else if (strContactNo.length() != 10) {
                final Snackbar snackbar;
                snackbar = Snackbar.make(mobile_no, "Please Check Contact No.", Snackbar.LENGTH_SHORT);
                View snackBarView = snackbar.getView();
                snackBarView.layout(20, 20, 20, 20);
                snackBarView.setBackgroundColor(Color.RED);
                TextView textView = (TextView) snackBarView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(Color.WHITE);
                snackBarView.setMinimumHeight(103);
                textView.setGravity(Gravity.CENTER);
                snackbar.setActionTextColor(Color.WHITE);
                snackbar.show();
            } else if (cityName.equals("")) {
                final Snackbar snackbar;
                snackbar = Snackbar.make(city_tv, "Please Select a City", Snackbar.LENGTH_SHORT);
                View snackBarView = snackbar.getView();
                snackBarView.layout(20, 20, 20, 20);
                snackBarView.setBackgroundColor(Color.RED);
                TextView textView = (TextView) snackBarView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(Color.WHITE);
                snackBarView.setMinimumHeight(103);
                textView.setGravity(Gravity.CENTER);
                snackbar.setActionTextColor(Color.WHITE);
                snackbar.show();
            } else {
                Intent intent = new Intent(Service_request_activity.this, ScannerActivity.class);
                intent.putExtra("service", "service_request");
                intent.putExtra("customer_name", strCutomerName);
                intent.putExtra("customer_no", strContactNo);
                Log.d("OnNext_City_ID", "" + city_id);
                intent.putExtra("city_id", city_id);
                startActivity(intent);
                finish();
            }
        }
    };
    View.OnClickListener backClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(Service_request_activity.this, ProductDetailActivity.class);
            startActivity(intent);
            finish();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_request_activity);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        back_button = (ImageView) toolbar.findViewById(R.id.back_button);
        back_button.setOnClickListener(backClick);
        customer_name = (EditText) findViewById(R.id.customer_name);
        city_tv = (TextView) findViewById(R.id.city_tv);
        mobile_no = (EditText) findViewById(R.id.mobile_no);
        next = (Button) findViewById(R.id.next);
        next.setOnClickListener(nextClick);
        city_tv.setOnClickListener(cityclick);
        if (getIntent().getStringExtra("cityname") == null) {
            cityName = "";
            city_tv.setText("City");

        } else {
            cityName = getIntent().getStringExtra("cityname");
            city_tv.setText(cityName);
            city_id = getIntent().getIntExtra("cityId", 0);
            customer_name.setText(getIntent().getStringExtra("name"));
            mobile_no.setText(getIntent().getStringExtra("mobileno"));
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(Service_request_activity.this, ProductDetailActivity.class);
        startActivity(intent);
        finish();
    }
}