package com.sar.user.livfastRewards.helper;

import android.app.Dialog;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by user on 04-04-2017.
 */

public class MyUtils {
    private static Dialog mDialog;

    public static String getValidDateFormat(String date) {
        try {
            DateFormat f1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            Date d = f1.parse(date);
            DateFormat f2 = new SimpleDateFormat("dd-MM-yyyy");
            return f2.format(d).toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getYearlyFormat(String date) {
        try {
            DateFormat f1 = new SimpleDateFormat("dd-MM-yyyy");
            Date d = f1.parse(date);
            DateFormat f2 = new SimpleDateFormat("yyyy-MM-dd");
            return f2.format(d).toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getslashFormat(String date) {
        try {
            DateFormat f1 = new SimpleDateFormat("yyy-MM-dd");
            Date d = f1.parse(date);
            DateFormat f2 = new SimpleDateFormat("dd/MM/yyyy");
            return f2.format(d).toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getdaydashFormat(String date) {
        try {
            DateFormat f1 = new SimpleDateFormat("dd/MM/yyyy");
            Date d = f1.parse(date);
            DateFormat f2 = new SimpleDateFormat("dd-MM-yyyy");
            return f2.format(d).toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getYearlydashFormat(String date) {
        try {
            DateFormat f1 = new SimpleDateFormat("dd/MM/yyyy");
            Date d = f1.parse(date);
            DateFormat f2 = new SimpleDateFormat("yyyy-MM-dd");
            return f2.format(d).toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void removeCustomeProgressDialog() {
        try {
            if (mDialog != null) {
                if (mDialog.isShowing()) {
                    mDialog.dismiss();
                }
            }
            mDialog = null;
        } catch (IllegalArgumentException ie) {
            ie.printStackTrace();

        } catch (RuntimeException re) {
            re.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
