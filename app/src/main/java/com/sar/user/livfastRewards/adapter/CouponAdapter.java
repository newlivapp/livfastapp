package com.sar.user.livfastRewards.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sar.user.livfastRewards.Model.RedeemSlabModel;
import com.sar.user.livfastRewards.R;

import java.util.List;

/**
 * Created by user on 02-04-2017.
 */

public class CouponAdapter extends RecyclerView.Adapter<CouponAdapter.MyViewHolder> {
    Context context;
    List<RedeemSlabModel> productList;

    public CouponAdapter(Context context1, List<RedeemSlabModel> productList1) {
        context = context1;
        productList = productList1;
    }

    @Override
    public CouponAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.row_coupon_item, parent, false);
        MyViewHolder mvh = new MyViewHolder(v);
        return mvh;
    }

    @Override
    public void onBindViewHolder(final CouponAdapter.MyViewHolder holder, int position) {
        holder.tvLoyalityPoints.setText(String.valueOf(productList.get(position).getLoyalty_point()));
        holder.tvDescription.setText(productList.get(position).getRedeem_desc());
        if (productList.get(position).isSelected()) {
            holder.ivSelector.setVisibility(View.VISIBLE);
        } else {
            holder.ivSelector.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvLoyalityPoints, tvDescription;
        ImageView ivSelector;

        public MyViewHolder(View itemView) {
            super(itemView);
            ivSelector = itemView.findViewById(R.id.ivSelector);
            tvLoyalityPoints = itemView.findViewById(R.id.tvLoyalityPoints);
            tvDescription = itemView.findViewById(R.id.tvDescription);
        }
    }
}
