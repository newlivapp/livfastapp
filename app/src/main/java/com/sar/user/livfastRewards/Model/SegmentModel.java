package com.sar.user.livfastRewards.Model;

/**
 * Created by user on 10-04-2017.
 */

public class SegmentModel {
    int id;
    String car_segment;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCar_segment() {
        return car_segment;
    }

    public void setCar_segment(String car_segment) {
        this.car_segment = car_segment;
    }
}
