package com.sar.user.livfastRewards.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.sar.user.livfastRewards.R;

;


public abstract class CouponWinDialog extends Dialog {

    public CouponWinDialog(Context context, String message) {
        super(context, R.style.MyCustomDialog);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setContentView(R.layout.popup_win_coupon);

        TextView tvCouponMessage = (TextView) findViewById(R.id.tvCouponMessage);
        tvCouponMessage.setText(message);

        TextView tvClose = (TextView) findViewById(R.id.tvClose);
        tvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCloseClick();
                dismiss();
            }
        });

        Button btnCouponWallet = findViewById(R.id.btnCouponWallet);
        btnCouponWallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onWalletCouponClick();
                dismiss();
            }
        });
    }


    public abstract void onCloseClick();

    public abstract void onWalletCouponClick();

}
