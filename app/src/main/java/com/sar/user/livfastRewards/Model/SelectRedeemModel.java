package com.sar.user.livfastRewards.Model;

/**
 * Created by Nav on 08-Aug-17.
 */

public class SelectRedeemModel {
    private String user_id, loyalty_point, mobile_number, category, username, scheme_id;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getLoyalty_point() {
        return loyalty_point;
    }

    public void setLoyalty_point(String loyalty_point) {
        this.loyalty_point = loyalty_point;
    }

    public String getMobile_number() {
        return mobile_number;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getScheme_id() {
        return scheme_id;
    }

    public void setScheme_id(String scheme_id) {
        this.scheme_id = scheme_id;
    }
}
