package com.sar.user.livfastRewards.Model;

public class PInCodeModel {
    String jobType;

    String pincode;
    String warranty_num;

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }


    public String getWarranty_num() {
        return warranty_num;
    }

    public void setWarranty_num(String warranty_num) {
        this.warranty_num = warranty_num;
    }


    public String getJobType() {
        return jobType;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

}
