package com.sar.user.livfastRewards.Model;

/**
 * Created by user on 03-04-2017.
 */

public class ProductWarrantyDetailModel {
    int id;
    private double customer_phone;
    private String customer_name, dealer_product, vehicle_number, created_timestamp, small_image, large_image, warranty, sell_date, car_segment, vehicle_manufacturer, vehicle_model, unique_code, loyalty_points;
    ;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getCustomer_phone() {
        return customer_phone;
    }

    public void setCustomer_phone(double customer_phone) {
        this.customer_phone = customer_phone;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getDealer_product() {
        return dealer_product;
    }

    public void setDealer_product(String dealer_product) {
        this.dealer_product = dealer_product;
    }

    public String getVehicle_number() {
        return vehicle_number;
    }

    public void setVehicle_number(String vehicle_number) {
        this.vehicle_number = vehicle_number;
    }

    public String getVehicle_model() {
        return vehicle_model;
    }

    public void setVehicle_model(String vehicle_model) {
        this.vehicle_model = vehicle_model;
    }

    public String getCar_segment() {
        return car_segment;
    }

    public void setCar_segment(String car_segment) {
        this.car_segment = car_segment;
    }

    public String getVehicle_manufacturer() {
        return vehicle_manufacturer;
    }

    public void setVehicle_manufacturer(String vehicle_manufacturer) {
        this.vehicle_manufacturer = vehicle_manufacturer;
    }

    public String getUnique_code() {
        return unique_code;
    }

    public void setUnique_code(String unique_code) {
        this.unique_code = unique_code;
    }

    public String getCreated_timestamp() {
        return created_timestamp;
    }

    public void setCreated_timestamp(String created_timestamp) {
        this.created_timestamp = created_timestamp;
    }

    public String getSmall_image() {
        return small_image;
    }

    public void setSmall_image(String small_image) {
        this.small_image = small_image;
    }

    public String getLarge_image() {
        return large_image;
    }

    public void setLarge_image(String large_image) {
        this.large_image = large_image;
    }

    public String getWarranty() {
        return warranty;
    }

    public void setWarranty(String warranty) {
        this.warranty = warranty;
    }

    public String getSell_date() {
        return sell_date;
    }

    public void setSell_date(String sell_date) {
        this.sell_date = sell_date;
    }

    public String getLoyalty_points() {
        return loyalty_points;
    }

    public void setLoyalty_points(String loyalty_points) {
        this.loyalty_points = loyalty_points;
    }
}
