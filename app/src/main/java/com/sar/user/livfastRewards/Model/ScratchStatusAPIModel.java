package com.sar.user.livfastRewards.Model;

public class ScratchStatusAPIModel {
    private String status, id;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
