package com.sar.user.livfastRewards.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sar.user.livfastRewards.Interface.RequestListener;
import com.sar.user.livfastRewards.Model.RedeemSlabModel;
import com.sar.user.livfastRewards.Model.WarrantyLoyalityModel;
import com.sar.user.livfastRewards.R;
import com.sar.user.livfastRewards.adapter.CouponAdapter;
import com.sar.user.livfastRewards.helper.Controller;
import com.sar.user.livfastRewards.helper.PrefManager;

import org.json.JSONException;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class Activity_sec_ibups_redeem extends BaseActivity {
    List<RedeemSlabModel> choice_list = new ArrayList<RedeemSlabModel>();
    List<RedeemSlabModel> choice_list_pan = new ArrayList<RedeemSlabModel>();
    TextView view_redeem_history,heading1_dh;
    TextView total_purchase_points;
    TextView scheme;
    EditText enter_manualy;
    Toolbar toolbar;
    ImageView back_button;
    ProgressBar progressBar;
    Button redeem_Button;
    int loyalty_points;
    RadioGroup redeem_choice_rg;
    RadioButton radioButton;
    InputMethodManager imm;
    RelativeLayout empty_rl, redeem_slab_rl;
    String points_redeem;

    RecyclerView recyclerView;
    CouponAdapter couponAdapter;
    RelativeLayout redeem_policy_RL_dh;

    PrefManager pref;

    RequestListener requestListener = new RequestListener() {
        @Override
        public void onRequestStarted() {
        }

        @SuppressLint("LongLogTag")
        @Override
        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
            Log.d("responce GetSecRedeemSlab", responseObject.toString());


            if(pref.getUserViewFlag()){
                choice_list.clear();
                Type collection = new TypeToken<List<RedeemSlabModel>>() {
                }.getType();
                final List<RedeemSlabModel> rsm = (List<RedeemSlabModel>) new Gson().fromJson(responseObject.toString(), collection);
                choice_list.addAll(rsm);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);

                        if (choice_list.isEmpty()) {
                            empty_rl.setVisibility(View.VISIBLE);
                            redeem_slab_rl.setVisibility(View.GONE);
                            redeem_policy_RL_dh.setVisibility(View.GONE);
                        } else {
                            empty_rl.setVisibility(View.GONE);

                            if(pref.getUserViewFlag())
                            {
                                redeem_slab_rl.setVisibility(View.GONE);
                                redeem_policy_RL_dh.setVisibility(View.VISIBLE);
                                if (couponAdapter == null)
                                    setupRecyclerView();
                                else
                                    couponAdapter.notifyDataSetChanged();
                            }else {
                                redeem_slab_rl.setVisibility(View.VISIBLE);
                                redeem_policy_RL_dh.setVisibility(View.GONE);
                                for (int i = 0; i < choice_list.size(); i++) {
                                    radioButton = new RadioButton(Activity_sec_ibups_redeem.this);
                                    radioButton.setId(i);
                                    radioButton.setText("" + choice_list.get(i).getLoyalty_point());
                                    radioButton.setTextColor(Color.parseColor("#4F4F4F"));
                                    redeem_choice_rg.addView(radioButton);
                                }
                            }
                            /*if (choice_list.get(0).is_manual() == true) {
                                //enter_manually_tv.setVisibility(View.VISIBLE);
                                enter_manualy.setVisibility(View.GONE);
                                scheme.setVisibility(View.VISIBLE);
                            } else {
                                // enter_manually_tv.setVisibility(View.GONE);
                                enter_manualy.setVisibility(View.GONE);
                            }*/
                        }
                    }
                });
            }else {
                choice_list_pan.clear();
                Type collection = new TypeToken <List <RedeemSlabModel>>() {
                }.getType();
                final List <RedeemSlabModel> rsm = (List <RedeemSlabModel>) new Gson().fromJson(responseObject.toString(), collection);
                choice_list_pan.addAll(rsm);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);

                        if (choice_list_pan.isEmpty()) {
                            empty_rl.setVisibility(View.VISIBLE);
                            redeem_slab_rl.setVisibility(View.GONE);
                            redeem_policy_RL_dh.setVisibility(View.GONE);
                        } else {
                            empty_rl.setVisibility(View.GONE);

                            if (pref.getUserViewFlag()) {
                                redeem_slab_rl.setVisibility(View.GONE);
                                redeem_policy_RL_dh.setVisibility(View.VISIBLE);
                                if (couponAdapter == null)
                                    setupRecyclerViewpan();
                                else
                                    couponAdapter.notifyDataSetChanged();
                            } else {
                                redeem_slab_rl.setVisibility(View.GONE);
                                redeem_policy_RL_dh.setVisibility(View.VISIBLE);
                                for (int i = 0; i < choice_list_pan.size(); i++) {
                                    radioButton = new RadioButton(Activity_sec_ibups_redeem.this);
                                    radioButton.setId(i);
                                    radioButton.setText("" + choice_list_pan.get(i).getLoyalty_point());
                                    radioButton.setTextColor(Color.parseColor("#4F4F4F"));
                                    redeem_choice_rg.addView(radioButton);
                                }
                            }
                            /*if (choice_list.get(0).is_manual() == true) {
                                //enter_manually_tv.setVisibility(View.VISIBLE);
                                enter_manualy.setVisibility(View.GONE);
                                scheme.setVisibility(View.VISIBLE);
                            } else {
                                // enter_manually_tv.setVisibility(View.GONE);
                                enter_manualy.setVisibility(View.GONE);
                            }*/
                        }
                    }
                });
            }
        }

        @Override
        public void onRequestError(int errorCode, String message) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                }
            });

            handelError(errorCode, message);
        }
    };

    //    RequestListener loyalityListner =new RequestListener() {
//        @Override
//        public void onRequestStarted() {
//
//        }
//
//        @Override
//        public void onRequestCompleted(final Object responseObject) throws JSONException, ParseException {
//            Log.d("response",responseObject.toString());
//            runOnUiThread(new Runnable() {
//                @SuppressLint("SetTextI18n")
//                @Override
//                public void run() {
//                 //   int loyality= pref.getLoginModel().getData().getTotal_loyalty()+Integer.parseInt(String.valueOf(responseObject));
//                    int loyality= Integer.parseInt(String.valueOf(responseObject));
//                    total_purchase_points.setText(""+loyality);
//                }
//            });
//
//        }
//
//        @Override
//        public void onRequestError(int errorCode, String message) {
//
//        }
//    };
    View.OnClickListener historyDetail = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent inten = new Intent(Activity_sec_ibups_redeem.this, RedeemHistoryActivity.class);
            startActivity(inten);
        }
    };
    //    View.OnClickListener redeemClick=new View.OnClickListener() {
//        @Override
//        public void onClick(View v) {
//
//            if(loyalty_points==0&&enter_manualy.getText().toString().equals("")){
//                Toast.makeText(RedeemActivity.this,"Please select your Redeem points",Toast.LENGTH_SHORT).show();
//            }else if (!enter_manualy.getText().toString().equals("")&&Integer.parseInt(enter_manualy.getText().toString()) > prefManager.getLoginModel().getData().getTotal_loyalty()) {
//                Toast.makeText(RedeemActivity.this, "You do not have enough loyalty points to redeem", Toast.LENGTH_SHORT).show();
//
//            }else {
//                SelectRedeemModel selectRedeemModel = new SelectRedeemModel();
//                selectRedeemModel.setRedeem_scheme(redeemSchemeId);
//                if (enter_manualy.getText().toString().equals("")) {
//                    selectRedeemModel.setLoyalty_point(loyalty_points);
//                } else {
//                    selectRedeemModel.setLoyalty_point(Integer.parseInt(enter_manualy.getText().toString()));
//                }
//
//
//                progressBar.setVisibility(View.VISIBLE);
//                Controller.SelectRedeem(RedeemActivity.this, selectRedeemModel, new RequestListener() {
//                    @Override
//                    public void onRequestStarted() {
//
//                    }
//
//                    @Override
//                    public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
//                        Log.d("response", responseObject.toString());
//                        RedeemResponseModel getRedeemHistoryModel = JsonUtils.objectify(responseObject.toString(), RedeemResponseModel.class);
//                        assert getRedeemHistoryModel != null;
//                        user_id = getRedeemHistoryModel.getId();
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                progressBar.setVisibility(View.GONE);
//                                final Dialog dialog = new Dialog(RedeemActivity.this);
//                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                                dialog.setContentView(R.layout.otp_massage_popup);
//                                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//                                final TextView msgText = (TextView) dialog.findViewById(R.id.msg_text);
//                                final ImageView dismiss = (ImageView) dialog.findViewById(R.id.dissmiss);
//                                final TextView phone_no_msg = (TextView) dialog.findViewById(R.id.phone_no_massage);
//                                final EditText otp_text = (EditText) dialog.findViewById(R.id.otp_text);
//                                final TextView resendOtp_tv = (TextView) dialog.findViewById(R.id.resend_otp_tv);
//                                SpannableString content = new SpannableString("RE-Send OTP");
//                                content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
//                                resendOtp_tv.setText(content);
//                                resendOtp_tv.setOnClickListener(new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View view) {
//                                        FinalPointsRedeemModel finalPointsRedeemModel = new FinalPointsRedeemModel();
//                                        finalPointsRedeemModel.setId(user_id);
//                                        finalPointsRedeemModel.setResend_otp(1);
//                                        progressBar.setVisibility(View.VISIBLE);
//                                        Controller.TotalPointsRedeem(RedeemActivity.this, user_id, finalPointsRedeemModel, new RequestListener() {
//                                            @Override
//                                            public void onRequestStarted() {
//
//                                            }
//
//                                            @Override
//                                            public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
//                                                Log.d("response", responseObject.toString());
//                                                runOnUiThread(new Runnable() {
//                                                    @Override
//                                                    public void run() {
//                                                        Toast.makeText(RedeemActivity.this, "OTP sent successfully", Toast.LENGTH_SHORT).show();
//                                                        progressBar.setVisibility(View.GONE);
//                                                    }
//                                                });
//                                            }
//
//                                            @Override
//                                            public void onRequestError(int errorCode, String message) {
//                                                Log.d("response", message);
//                                                if (errorCode >= 400 && errorCode < 500) {
//                                                    if (errorCode == 403) {
//                                                        runOnUiThread(new Runnable() {
//                                                            @Override
//                                                            public void run() {
//                                                                progressBar.setVisibility(View.GONE);
//                                                                Toast.makeText(RedeemActivity.this, "UnAuthorised!", Toast.LENGTH_SHORT).show();
//                                                            }
//                                                        });
//                                                    } else if (errorCode == 401) {
//
//                                                        runOnUiThread(new Runnable() {
//                                                            @Override
//                                                            public void run() {
//                                                                progressBar.setVisibility(View.GONE);
//                                                                //                                logout();
//                                                            }
//                                                        });
//                                                    } else {
//                                                        final ErrorResponseModel errorResponseModel = JsonUtils.objectify(message, ErrorResponseModel.class);
//                                                        runOnUiThread(new Runnable() {
//                                                            @Override
//                                                            public void run() {
//                                                                progressBar.setVisibility(View.GONE);
//                                                                Toast.makeText(RedeemActivity.this, errorResponseModel.getErr(), Toast.LENGTH_SHORT).show();
//                                                            }
//                                                        });
//                                                    }
//                                                } else {
//                                                    runOnUiThread(new Runnable() {
//                                                        @Override
//                                                        public void run() {
//                                                            progressBar.setVisibility(View.GONE);
//                                                            Toast.makeText(RedeemActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
//                                                        }
//                                                    });
//                                                }
//                                            }
//                                        });
//                                    }
//                                });
//
//                                if (prefManager.getLoginModel() != null) {
//                                    phone_no_msg.setText("Enter OTP send to +91" + prefManager.getLoginModel().getData().getPhone());
//                                }
//                                msgText.setVisibility(View.GONE);
//                                final Button verify = (Button) dialog.findViewById(R.id.verify_button);
//                                verify.setOnClickListener(new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View v) {
//                                        FinalPointsRedeemModel finalPointsRedeemModel = new FinalPointsRedeemModel();
//                                        finalPointsRedeemModel.setOtp(otp_text.getText().toString());
//                                        Controller.TotalPointsRedeem(RedeemActivity.this, user_id, finalPointsRedeemModel, new RequestListener() {
//                                            @Override
//                                            public void onRequestStarted() {
//
//                                            }
//
//                                            @Override
//                                            public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
//                                                Log.d("response", responseObject.toString());
//                                                ResponseRedeemPointsModel responseRedeemPointsModel = JsonUtils.objectify(responseObject.toString(), ResponseRedeemPointsModel.class);
//                                                assert responseRedeemPointsModel != null;
//                                                int deductLoyaltyPoints = responseRedeemPointsModel.getLoyalty_point();
//                                                LoginResponseModel obj1 = prefManager.getLoginModel();
//                                                int previousLoyaltyPoints = obj1.getData().getTotal_loyalty();
//                                                int finalLoyaltyPoints = previousLoyaltyPoints + deductLoyaltyPoints;
//                                                obj1.getData().setTotal_loyalty(finalLoyaltyPoints);
//                                                prefManager.saveLoginModel(obj1);
//                                                runOnUiThread(new Runnable() {
//                                                    @Override
//                                                    public void run() {
//                                                        Toast.makeText(RedeemActivity.this, "Redeem request successful", Toast.LENGTH_SHORT).show();
//                                                        finish();
//                                                    }
//                                                });
//
//
//                                            }
//
//                                            @Override
//                                            public void onRequestError(int errorCode, String message) {
//                                                Log.d("response", message);
//                                                if (errorCode >= 400 && errorCode < 500) {
//                                                    if (errorCode == 403) {
//                                                        runOnUiThread(new Runnable() {
//                                                            @Override
//                                                            public void run() {
//                                                                progressBar.setVisibility(View.GONE);
//                                                                Toast.makeText(RedeemActivity.this, "UnAuthorised!", Toast.LENGTH_SHORT).show();
//                                                            }
//                                                        });
//                                                    } else if (errorCode == 401) {
//
//                                                        runOnUiThread(new Runnable() {
//                                                            @Override
//                                                            public void run() {
//                                                                progressBar.setVisibility(View.GONE);
//                                                                //  logout();
//                                                            }
//                                                        });
//                                                    } else {
//                                                        final ErrorResponseModel errorResponseModel = JsonUtils.objectify(message, ErrorResponseModel.class);
//                                                        runOnUiThread(new Runnable() {
//                                                            @Override
//                                                            public void run() {
//                                                                progressBar.setVisibility(View.GONE);
//                                                                Toast.makeText(RedeemActivity.this, errorResponseModel.getErr(), Toast.LENGTH_SHORT).show();
//                                                            }
//                                                        });
//                                                    }
//                                                } else {
//                                                    runOnUiThread(new Runnable() {
//                                                        @Override
//                                                        public void run() {
//                                                            progressBar.setVisibility(View.GONE);
//                                                            Toast.makeText(RedeemActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
//                                                        }
//                                                    });
//                                                }
//                                            }
//                                        });
//                                    }
//                                });
//                                dismiss.setOnClickListener(new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View v) {
//                                        dialog.dismiss();
//                                    }
//                                });
//                                dialog.show();
//                                dialog.setCancelable(false);
//                            }
//                        });
//                    }
//
//                    @Override
//                    public void onRequestError(int errorCode, String message) {
//                        Log.d("response", message);
//                        if (errorCode >= 400 && errorCode < 500) {
//                            if (errorCode == 403) {
//                                runOnUiThread(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        progressBar.setVisibility(View.GONE);
//                                        Toast.makeText(RedeemActivity.this, "UnAuthorised!", Toast.LENGTH_SHORT).show();
//                                    }
//                                });
//                            } else if (errorCode == 401) {
//
//                                runOnUiThread(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        progressBar.setVisibility(View.GONE);
////                                logout();
//                                    }
//                                });
//                            } else {
//                                final ErrorResponseModel errorResponseModel = JsonUtils.objectify(message, ErrorResponseModel.class);
//                                runOnUiThread(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        progressBar.setVisibility(View.GONE);
//                                        Toast.makeText(RedeemActivity.this, errorResponseModel.getErr(), Toast.LENGTH_SHORT).show();
//                                    }
//                                });
//                            }
//                        } else {
//                            runOnUiThread(new Runnable() {
//                                @Override
//                                public void run() {
//                                    progressBar.setVisibility(View.GONE);
//                                    Toast.makeText(RedeemActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
//                                }
//                            });
//                        }
//
//                    }
//                });
//            }
//        }
//    };
    View.OnClickListener backClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    public static void hideSoftKeyboard(Activity activity, View view) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sec_ibups_redeem);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        back_button = (ImageView) toolbar.findViewById(R.id.back_button);
        recyclerView = findViewById(R.id.recyclerView);
        heading1_dh = findViewById(R.id.heading1_dh);
        redeem_policy_RL_dh = findViewById(R.id.redeem_policy_RL_dh);
        enter_manualy = (EditText) findViewById(R.id.manually_points);
        total_purchase_points = (TextView) findViewById(R.id.total_purchase_points);
        redeem_Button = (Button) findViewById(R.id.redeem_button);
        redeem_choice_rg = (RadioGroup) findViewById(R.id.redeem_choice_rb);
        scheme = (TextView) findViewById(R.id.choice_list);
        // enter_manually_tv= (TextView) findViewById(R.id.enter_manually_tv);
        empty_rl = (RelativeLayout) findViewById(R.id.empty_rl);
        redeem_slab_rl = (RelativeLayout) findViewById(R.id.loyalty_points_slab);
        redeem_choice_rg.clearCheck();
        back_button.setOnClickListener(backClick);
        view_redeem_history = (TextView) findViewById(R.id.view_redeem_history);
        progressBar = (ProgressBar) findViewById(R.id.progressbar);
        SpannableString content = new SpannableString("View Redeem History");
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        view_redeem_history.setText(content);
        view_redeem_history.setOnClickListener(historyDetail);
        //  redeem_Button.setOnClickListener(redeemClick);
        progressBar.setVisibility(View.VISIBLE);
        //Intent intent=getIntent();
        points_redeem = getIntent().getStringExtra("points4");
        total_purchase_points.setText("" + points_redeem);

        WarrantyLoyalityModel warrantyLoyalityModel = new WarrantyLoyalityModel();
        String user_id = PrefManager.getInstance(this).getÜserId();
        warrantyLoyalityModel.setUser_id(user_id);
//        Controller.getWarrantyLoyality(RedeemActivity.this, warrantyLoyalityModel, loyalityListner);

        pref = PrefManager.getInstance(this);

        if(pref.getUserViewFlag()){
            setupRecyclerView();
        }else {
            setupRecyclerViewpan();
//            imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//            redeem_choice_rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//
//                @Override
//                public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
//                    enter_manualy.setText(null);
//
//                    enter_manualy.setEnabled(false);
//                    int checkId = group.getCheckedRadioButtonId();
//                    radioButton = (RadioButton) findViewById(checkId);
//                    if (checkedId == -1) {
//                        //  enter_manually_tv.setVisibility(View.GONE);
//                        enter_manualy.setEnabled(true);
//                        imm.showSoftInput(enter_manualy, 0);
//                    } else {
//                        loyalty_points = Integer.parseInt(radioButton.getText().toString());
//                    }
//                }
//            });
        }
//        enter_manually_tv.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                enter_manually_tv.setVisibility(View.GONE);
//                enter_manualy.requestFocus();
//                imm.showSoftInput(enter_manualy,0);
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        redeem_choice_rg.clearCheck();
//                    }
//                });
//            }
//        });

        RedeemSlabModel redeemSlabModel = new RedeemSlabModel();
        redeemSlabModel.setUser_id(user_id);
        Controller.GetSecRedeemSlab(Activity_sec_ibups_redeem.this, redeemSlabModel,requestListener);
    }

    private void setupRecyclerView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        couponAdapter = new CouponAdapter(this, choice_list);
        recyclerView.setAdapter(couponAdapter);
        recyclerView.invalidate();
    }

    private void setupRecyclerViewpan() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        couponAdapter = new CouponAdapter(this, choice_list_pan);
        recyclerView.setAdapter(couponAdapter);
        recyclerView.invalidate();
    }
}
