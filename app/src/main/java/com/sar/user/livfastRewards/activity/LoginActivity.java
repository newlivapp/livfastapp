package com.sar.user.livfastRewards.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.sar.user.livfastRewards.Interface.RequestListener;
import com.sar.user.livfastRewards.Model.LoginModel;
import com.sar.user.livfastRewards.Model.LoginResponseModel;
import com.sar.user.livfastRewards.R;
import com.sar.user.livfastRewards.helper.Controller;
import com.sar.user.livfastRewards.helper.JsonUtils;
import com.sar.user.livfastRewards.helper.PrefManager;

public class LoginActivity extends BaseActivity implements View.OnClickListener {
    Button login_button;
    TextView forgot_password;
    EditText login, password;
    ProgressBar progressBar;
    RelativeLayout first_time_rl;
    TextView click_here;

    Context mContext;
    RequestListener loginlistener = new RequestListener() {
        @Override
        public void onRequestStarted() {
        }

        @Override
        public void onRequestCompleted(final Object responseObject) {
            Log.d("response manualLogin", responseObject.toString());
            final LoginResponseModel responsemodel = JsonUtils.objectify(responseObject.toString(), LoginResponseModel.class);
            final PrefManager pref = PrefManager.getInstance(mContext);

            if (responsemodel == null || responsemodel.getData() == null || responsemodel.getData().getUser_id().isEmpty()) {
                runOnUiThread(new Thread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(mContext, responseObject.toString(), Toast.LENGTH_SHORT).show();
                    }
                }));

                return;
            }
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                    pref.saveUserInfo(responsemodel);
                    if (responsemodel.getToken() != null && responsemodel.getData().getVerification_status().equals("Verified")) {
                        Answers.getInstance().logCustom(new CustomEvent("Login")
                                .putCustomAttribute("BAT_CODE", pref.getBatCode())
                                .putCustomAttribute("USER_NAME", pref.getDealershipName()));
                        pref.setIsLogin(true);
                        Intent intent1 = new Intent(mContext, ProductDetailActivity.class);
                        startActivity(intent1);
                        finish();
                    } else {
                        Intent intent = new Intent(mContext, Change_password_activity.class);
                        intent.putExtra("for_action", "user_first_time");
                        startActivity(intent);
                    }
                }
            });
        }

        @Override
        public void onRequestError(int errorCode, String message) {
            handelError(errorCode, message);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);

                }
            });
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mContext = this;
        initControl();

        SpannableString content = new SpannableString("Click here");
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        click_here.setText(content);

        //Click event
        first_time_rl.setOnClickListener(this);
        login_button.setOnClickListener(this);
        forgot_password.setOnClickListener(this);
    }

    private void initControl() {
        login_button = findViewById(R.id.loginbutton);
        forgot_password = findViewById(R.id.tvForgotPassword);
        login = findViewById(R.id.username);
        password = findViewById(R.id.password);
        first_time_rl = findViewById(R.id.first_time_rl);
        click_here = findViewById(R.id.clickhere_tv);
        progressBar = findViewById(R.id.progressbar);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    public void onClick(View v) {
        if (v == first_time_rl) {
            Intent intent = new Intent(LoginActivity.this, BatCodeAvtivity.class);
            intent.putExtra("for_action", "user_first_time");
            startActivity(intent);
        } else if (v == forgot_password) {
            Intent intent1 = new Intent(LoginActivity.this, BatCodeAvtivity.class);
            intent1.putExtra("for_action", "user_forgot_password");
            startActivity(intent1);
            finish();
        } else if (v == login_button) {
            if (!validateInputData())
                return;

            LoginModel loginModel = new LoginModel();
                loginModel.setJob_Type("Login");
            loginModel.setBat_code(login.getText().toString().trim().toUpperCase());
            loginModel.setPassword(password.getText().toString().trim());

            progressBar.setVisibility(View.VISIBLE);
            //call API
            Controller.manualLogin(LoginActivity.this, loginModel, loginlistener);
        }
    }

    private boolean validateInputData() {
        if (login.getText().toString().trim().isEmpty()) {
            login.setError("Please enter batcode.");
            login.requestFocus();
            return false;
        } else if (password.getText().toString().trim().isEmpty()) {
            password.setError("Please enter Password.");
            password.requestFocus();
            return false;
        }

        return true;
    }
}
