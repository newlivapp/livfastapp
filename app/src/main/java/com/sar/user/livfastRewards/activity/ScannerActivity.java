package com.sar.user.livfastRewards.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.zxing.Result;
import com.sar.user.livfastRewards.Model.SerialToUniqueModel;
import com.sar.user.livfastRewards.R;

import java.util.regex.Pattern;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class ScannerActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {
    private static String[] PERMISSIONS_LOCATION = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
    final int REQUEST_LOCATION = 2;
    Toolbar toolbar;
    ImageView skip;
    String service_cutomer_name, service_cutomer_no;
    int city_id;
    String strCityId;
    String serialno;
    String serial_no;
    ViewGroup contentFrame;
    private ZXingScannerView mScannerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scanner_activity);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        skip = findViewById(R.id.skip);
        contentFrame = findViewById(R.id.content_frame);
        mScannerView = new ZXingScannerView(this);   // Programmatically initialize the scanner view
        contentFrame.addView(mScannerView);

        if (hasLocationPermissionGranted()) {
            mScannerView.startCamera();
        } else {
            requestLocationPermission();
        }
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            service_cutomer_name = bundle.getString("customer_name");
            service_cutomer_no = bundle.getString("customer_no");
            city_id = bundle.getInt("city_id");
            Log.d("ScannerCityID", "" + city_id);
        }

        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getIntent().getStringExtra("AddProduct") != null && getIntent().getStringExtra("AddProduct").equals("NewProduct")) {
                    Log.d("Warning", "-------- go to add product");
                    Intent intent = new Intent(ScannerActivity.this, add_product_activity.class);
                    startActivity(intent);
                    finish();
                } else if (getIntent().getStringExtra("AddWarranty") != null && getIntent().getStringExtra("AddWarranty").equals("NewWarranty")) {
                    Log.d("Warning", "-------- go to add warranty");
                    Intent intent1 = new Intent(ScannerActivity.this, AddWarantyActivity.class);
                    startActivity(intent1);
                    finish();
                } else if (getIntent().getStringExtra("service") != null && getIntent().getStringExtra("service").equals("service_request")) {
                    Intent intent = new Intent(ScannerActivity.this, Add_service_request_activity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

    public boolean hasLocationPermissionGranted() {
        return ContextCompat.checkSelfPermission(ScannerActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(ScannerActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(ScannerActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
    }

    public void requestLocationPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            ActivityCompat.requestPermissions(ScannerActivity.this, PERMISSIONS_LOCATION,
                    REQUEST_LOCATION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_LOCATION:
                if (grantResults.length > 0) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        //Storage permission is enabled
                        mScannerView.startCamera();

                    } else if (ActivityCompat.shouldShowRequestPermissionRationale(ScannerActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) && ActivityCompat.shouldShowRequestPermissionRationale(ScannerActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) && ActivityCompat.shouldShowRequestPermissionRationale(ScannerActivity.this, Manifest.permission.CAMERA)) {
                        //User has deny from permission dialog
                        final AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(ScannerActivity.this, R.style.AppCompatAlertDialogStyle);
                        alertDialog1.setTitle("Reading Permission Denied");
                        alertDialog1.setMessage("Are you sure you want to deny this permission?");
                        alertDialog1.setPositiveButton("I'M SURE", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                requestLocationPermission();
                            }
                        });
                        alertDialog1.setNegativeButton("RETRY", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                requestLocationPermission();
                            }
                        });
                        alertDialog1.show();
                    } else {
                        // User has deny permission and checked never show permission dialog so you can redirect to Application settings page
                        AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(ScannerActivity.this, R.style.AppCompatAlertDialogStyle);
                        alertDialog1.setMessage("It looks like you have turned off permission required for this feature. It can be enabled under Phone Settings > Apps > Partner > Permissions");
                        alertDialog1.setPositiveButton("GO TO SETTINGS", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent();
                                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package", getPackageName(), null);
                                intent.setData(uri);
                                startActivity(intent);
                            }
                        });
                        alertDialog1.show();
                    }
                }
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();          // Start camera on resume
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Stop camera on pause
    }

    @Override
    public void handleResult(Result rawResult) {
        Log.v("scanner", rawResult.getText()); // Prints scan results
        Log.v("scanner", rawResult.getBarcodeFormat().toString()); // Prints the scan format (qrCode, pdf417 etc.)
        serial_no = rawResult.getText();

        if (Pattern.matches("^[A-Z][A-Z]\\d[A-Z]{0,5}\\d{0,5}[A-Z]\\d$", serial_no)) {
            Toast.makeText(ScannerActivity.this, "Valid Barcode", Toast.LENGTH_SHORT).show();
            serialno = serial_no;
        } else if (Pattern.matches("^[A-Z][A-Z]\\d[A-Z]{0,5}\\d{0,5}[A-Z]{0,2}$", serial_no)) {
            Toast.makeText(ScannerActivity.this, "Valid Barcode", Toast.LENGTH_SHORT).show();
            serialno = serial_no;
            //new regex
        } else if (Pattern.matches("^[A-Z][A-Z]\\d{0,2}[A-Z]{0,4}\\d{0,5}[A-Z]\\d$", serial_no)) {
            Toast.makeText(ScannerActivity.this, "Valid Barcode", Toast.LENGTH_SHORT).show();
            serialno = serial_no;
        }
        //end if regex
        else if (Pattern.matches("^[A-Z][A-Z]\\d\\d[A-Z]{0,4}\\d{0,5}[A-Z]{0,2}$", serial_no)) {
            Toast.makeText(ScannerActivity.this, "Valid Barcode", Toast.LENGTH_SHORT).show();
            serialno = serial_no;
        } else if (Pattern.matches("^[A-Z]\\d{0,2}[A-Z]{0,6}\\d{0,5}[A-Z0-9]{0,2}[A-Z0-9]$", serial_no)) {
            Toast.makeText(ScannerActivity.this, "Valid Barcode", Toast.LENGTH_SHORT).show();
            serialno = serial_no;
        } else if (Pattern.matches("^\\d{0,18}$", serial_no)) {
            Toast.makeText(ScannerActivity.this, "Invalid Barcode", Toast.LENGTH_SHORT).show();
            return;
        } else if (Pattern.matches("^[A-Z][0-9]'%[A-Z],%,[0-9][\\/][0-9]![A-Z]{0,2}[0-9]", serial_no)) {
            Toast.makeText(ScannerActivity.this, "Invalid Barcode", Toast.LENGTH_SHORT).show();
            return;
        }
        if (getIntent().getStringExtra("service") != null && getIntent().getStringExtra("service").equals("service_request")) {
            Intent intent = new Intent(ScannerActivity.this, Service_request_activity.class);
            intent.putExtra("serial_no", serialno);
            intent.putExtra("customer_name", service_cutomer_name);
            intent.putExtra("customer_no", service_cutomer_no);
            intent.putExtra("city_id", city_id);
            Log.d("ResultCityId", "" + city_id);
            startActivity(intent);
            finish();
        }
        SerialToUniqueModel serialToUniqueModel = new SerialToUniqueModel();
        serialToUniqueModel.setSerial_number(serial_no);
        // Controller.SerialToUnique(ScannerActivity.this, serialToUniqueModel, serialListner);
        if (getIntent().getStringExtra("AddProduct") != null && getIntent().getStringExtra("AddProduct").equals("NewProduct")) {
            Log.d("Warning", "-------- go to add product");
            Intent intent = new Intent(ScannerActivity.this, add_product_activity.class);
            intent.putExtra("result", serial_no);
            startActivity(intent);
            finish();
        } else if (getIntent().getStringExtra("AddWarranty") != null && getIntent().getStringExtra("AddWarranty").equals("NewWarranty")) {
            Log.d("Warning", "-------- go to add warranty");
            Intent intent1 = new Intent(ScannerActivity.this, AddWarantyActivity.class);
            intent1.putExtra("result", serial_no);
            startActivity(intent1);
            finish();
        }
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mScannerView.resumeCameraPreview(ScannerActivity.this);
            }
        }, 2000);
    }
//    RequestListener serialListner = new RequestListener() {
//        @Override
//        public void onRequestStarted() {
//        }
//        @Override
//        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
//            Log.d("response", responseObject.toString());
//            //   final GetSerialNoModel serialNoModel = JsonUtils.objectify(responseObject.toString(), GetSerialNoModel.class);
//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    if (getIntent().getStringExtra("AddProduct") != null && getIntent().getStringExtra("AddProduct").equals("NewProduct")) {
//                        Log.d("Warning", "-------- go to add product");
//                        Intent intent = new Intent(ScannerActivity.this, add_product_activity.class);
//                        intent.putExtra("result",serial_no);
//                        startActivity(intent);
//                        finish();
//                    } else if (getIntent().getStringExtra("AddWarranty") != null && getIntent().getStringExtra("AddWarranty").equals("NewWarranty")) {
//                        Log.d("Warning", "-------- go to add warranty");
//                        Intent intent1 = new Intent(ScannerActivity.this, AddWarantyActivity.class);
//                        intent1.putExtra("result", serial_no);
//                        startActivity(intent1);
//                        finish();
//                    }
//                }
//            });
//        }
//        @Override
//        public void onRequestError(int errorCode, String message) {
//
//        }
//    };
}
