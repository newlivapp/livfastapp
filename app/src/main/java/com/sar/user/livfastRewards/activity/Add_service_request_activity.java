package com.sar.user.livfastRewards.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.sar.user.livfastRewards.Interface.RequestListener;
import com.sar.user.livfastRewards.Model.AddServiceRequestModel;
import com.sar.user.livfastRewards.Model.ServiceRequestResponceModel;
import com.sar.user.livfastRewards.R;
import com.sar.user.livfastRewards.helper.Controller;
import com.sar.user.livfastRewards.helper.JsonUtils;

import org.json.JSONException;

import java.text.ParseException;

public class Add_service_request_activity extends BaseActivity {
    Button add_button;
    Toolbar toolbar;
    ImageView back_button;
    EditText serial_no, comment;
    String result, customer_name, customer_no;
    int city_id;
    ProgressBar progressBar;
    ImageView view_previous;
    String strCityId;
    View.OnClickListener previousClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(Add_service_request_activity.this, ProductDetailActivity.class);
            intent.putExtra("service_tab", "service");
            intent.putExtra("tab", 1);
            startActivity(intent);
            finish();
        }
    };
    RequestListener serviceListner = new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @SuppressLint("LongLogTag")
        @Override
        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
            Log.d("responce AddserviceRequest", responseObject.toString());
            final ServiceRequestResponceModel serviceModel = JsonUtils.objectify(responseObject.toString(), ServiceRequestResponceModel.class);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                    final Dialog dialog = new Dialog(Add_service_request_activity.this);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.popup_massage);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    final Button addmore = (Button) dialog.findViewById(R.id.addmore);
                    final TextView close = (TextView) dialog.findViewById(R.id.close);
                    final TextView msg = (TextView) dialog.findViewById(R.id.bettarymsg);
                    addmore.setText("Add more service request");

                    msg.setText("Service request raised for " + serviceModel.getBattery_name() + " battery");
                    addmore.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent1 = new Intent(Add_service_request_activity.this, Service_request_activity.class);
                            intent1.putExtra("service", "service_request");
                            startActivity(intent1);
                            finish();
                        }
                    });
                    close.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(Add_service_request_activity.this, ProductDetailActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    });
                    if (Add_service_request_activity.this != null)
                        Add_service_request_activity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (!(Add_service_request_activity.this).isFinishing()) {
                                    try {
                                        dialog.show();
                                    } catch (WindowManager.BadTokenException e) {
                                        Log.e("WindowManagerBad ", e.toString());
                                    } catch (Exception e) {
                                        Log.e("Exception ", e.toString());
                                    }
                                }
                            }
                        });
                }
            });
        }

        @Override
        public void onRequestError(int errorCode, String message) {
            Log.d("response", message);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                }
            });

            handelError(errorCode, message);
        }
    };
    View.OnClickListener addClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AddServiceRequestModel addServiceRequestModel = new AddServiceRequestModel();
            addServiceRequestModel.setUnique_code("");
            addServiceRequestModel.setSerial(serial_no.getText().toString());
            addServiceRequestModel.setComment(comment.getText().toString());
            addServiceRequestModel.setCustomer_name(customer_name);
            addServiceRequestModel.setCity(1);
            addServiceRequestModel.setCustomer_mobile(customer_no);
            Controller.AddserviceRequest(Add_service_request_activity.this, addServiceRequestModel, serviceListner);
            progressBar.setVisibility(View.VISIBLE);


        }
    };
    View.OnClickListener backClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_service_request_activity);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        serial_no = (EditText) findViewById(R.id.qrcode);
        comment = (EditText) findViewById(R.id.comment);
        progressBar = (ProgressBar) findViewById(R.id.progressbar);
        back_button = (ImageView) toolbar.findViewById(R.id.back_button);
        view_previous = (ImageView) findViewById(R.id.view_previous);
        back_button.setOnClickListener(backClick);
        add_button = (Button) findViewById(R.id.add_button);
        add_button.setOnClickListener(addClick);
        result = getIntent().getStringExtra("serial_no");
        serial_no.setText(result);
        view_previous.setOnClickListener(previousClick);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            customer_name = getIntent().getStringExtra("customer_name");
            customer_no = getIntent().getStringExtra("customer_no");
            city_id = bundle.getInt("city_id");
            Log.d("ActivityCityId", "" + city_id);
        }

    }
}
