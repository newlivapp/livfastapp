package com.sar.user.livfastRewards.dialog;

import android.content.Context;
import android.support.v7.app.AlertDialog;

public abstract class BaseAlertDialog extends AlertDialog.Builder {

    public BaseAlertDialog(Context context) {
        super(context);

    }
}
