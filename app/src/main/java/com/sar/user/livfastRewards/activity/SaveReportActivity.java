package com.sar.user.livfastRewards.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sar.user.livfastRewards.Interface.RequestListener;
import com.sar.user.livfastRewards.Model.ProductWarrantyDetailModel;
import com.sar.user.livfastRewards.R;
import com.sar.user.livfastRewards.helper.Controller;
import com.sar.user.livfastRewards.helper.MyUtils;
import com.sar.user.livfastRewards.helper.PrefManager;

import org.json.JSONException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class SaveReportActivity extends BaseActivity {
    private static String[] PERMISSIONS_READ_WRITE = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    Button button_save;
    ProgressBar progressBar;
    CircleImageView profile_pic;
    ImageView back_button;
    Toolbar toolbar;
    TextView startDate_tv, endDate_tv;
    Calendar calendar = Calendar.getInstance();
    int final_end_year = calendar.get(Calendar.YEAR);
    int final_end_month = calendar.get(Calendar.MONTH);
    int final_end_day = calendar.get(Calendar.DAY_OF_MONTH);

    int final_start_day = calendar.get(Calendar.DAY_OF_MONTH) - 7;
    int final_start_month = calendar.get(Calendar.MONTH);
    int final_start_year = calendar.get(Calendar.YEAR);
    String str_start_date, str_end_date;
    List<ProductWarrantyDetailModel> purchaselist = new ArrayList<>();
    View.OnClickListener backClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };
    RequestListener productListner = new RequestListener() {
        @Override
        public void onRequestStarted() {

        }

        @Override
        public void onRequestCompleted(Object responseObject) throws JSONException, ParseException {
            Log.d("response GetWarranty", responseObject.toString());
            purchaselist.clear();
            Type collectionType = new TypeToken<List<ProductWarrantyDetailModel>>() {
            }.getType();
            List<ProductWarrantyDetailModel> ca = (List<ProductWarrantyDetailModel>) new Gson().fromJson(responseObject.toString(), collectionType);

            purchaselist.addAll(ca);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                    if (purchaselist.isEmpty()) {
                        Toast.makeText(SaveReportActivity.this, "Data not available", Toast.LENGTH_LONG).show();
                    } else {
                        if (hasLocationPermissionGranted())
                            createPDF();
                        else
                            requestReadWritePermission();
                    }
                }
            });

        }

        @Override
        public void onRequestError(int errorCode, String message) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                }
            });
            handelError(errorCode, message);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_report);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        back_button = (ImageView) toolbar.findViewById(R.id.back_button);
        button_save = (Button) findViewById(R.id.save_button);
        progressBar = (ProgressBar) findViewById(R.id.progressbar);
        profile_pic = (CircleImageView) findViewById(R.id.ivProfilePicmain);

        startDate_tv = findViewById(R.id.startDate);
        endDate_tv = findViewById(R.id.endDate);

        startDate_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final com.wdullaer.materialdatetimepicker.date.DatePickerDialog dpd = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance(new com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(com.wdullaer.materialdatetimepicker.date.DatePickerDialog view, final int year, final int monthOfYear, final int dayOfMonth) {

                        final String select_date = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

                                    final_start_day = dayOfMonth;
                                    final_start_month = monthOfYear + 1;
                                    final_start_year = year;
                                    startDate_tv.setText(MyUtils.getdaydashFormat(select_date));
                                    str_start_date = MyUtils.getYearlydashFormat(select_date);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                    }
                }, final_start_year, final_start_month, final_start_day);
                dpd.setMaxDate(calendar);
                dpd.show(getFragmentManager(), "DATE_PICKER_TAG");
            }
        });
        endDate_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (str_start_date == null || str_start_date.length() == 0) {
                    Toast.makeText(SaveReportActivity.this, "Select start date.", Toast.LENGTH_LONG).show();
                    return;
                }

                final com.wdullaer.materialdatetimepicker.date.DatePickerDialog dpd = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance(new com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(com.wdullaer.materialdatetimepicker.date.DatePickerDialog view, final int year, final int monthOfYear, final int dayOfMonth) {

                        final String select_date = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {

                                    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

                                    Date date1 = formatter.parse(MyUtils.getslashFormat(str_start_date));
                                    Date date2 = formatter.parse(select_date);

                                    if (date1.compareTo(date2) <= 0) {
                                        Log.d("ComparisonOfdate", "-----" + date1 + "is less Than---" + date2);
                                        final_end_day = dayOfMonth;
                                        final_end_month = monthOfYear + 1;
                                        final_end_year = year;
                                        endDate_tv.setText(MyUtils.getdaydashFormat(select_date));
                                        str_end_date = MyUtils.getYearlydashFormat(select_date);

                                    } else if (date1.compareTo(date2) > 0) {
                                        Log.d("ComparisonOfdate", "-----" + date1 + "is greater than---" + date2);
                                        Toast.makeText(SaveReportActivity.this, "End Date can not be Less than Start Date", Toast.LENGTH_SHORT).show();
                                    }

                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                    }
                }, final_end_year, final_end_month, final_end_day);

                dpd.setMaxDate(calendar);
                dpd.show(getFragmentManager(), "DATE_PICKER_TAG");
            }
        });

        button_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (str_start_date == null || str_start_date.length() == 0) {
                    Toast.makeText(SaveReportActivity.this, "Please select start date.", Toast.LENGTH_LONG).show();
                    return;
                }

                if (str_end_date == null || str_end_date.length() == 0) {
                    Toast.makeText(SaveReportActivity.this, "Please select end date.", Toast.LENGTH_LONG).show();
                    return;
                }
                progressBar.setVisibility(View.VISIBLE);

                Controller.GetWarranty(SaveReportActivity.this, str_start_date, str_end_date, PrefManager.getInstance(SaveReportActivity.this).getÜserId(), productListner);
            }
        });
        back_button.setOnClickListener(backClick);
    }

    public boolean hasLocationPermissionGranted() {
        return ContextCompat.checkSelfPermission(SaveReportActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(SaveReportActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    private void createPDF() {
        // create a new document
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {

            int NUMBER_OF_CELL = 3;
            int LINE_HEIGHT = 20;
            int HEADER_HEIGHT = 80;
            int PAGE_WIDTH = 600;
            int PAGE_HEIGHT = 900;


            PdfDocument document = new PdfDocument();
            // crate a page description

            int totalPages = (int) Math.ceil((double) purchaselist.size() / 25);

            for (int pageNumber = 0; pageNumber < totalPages; pageNumber++) {
                int currentPosition = 0;
                PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(PAGE_WIDTH, PAGE_HEIGHT, (pageNumber + 1)).create();

                // start a page
                PdfDocument.Page page = document.startPage(pageInfo);
                Canvas canvas = page.getCanvas();
                Paint paint = new Paint();
                paint.setColor(Color.RED);

                //Page Header
                Paint paintHeader = new Paint();
                paintHeader.setColor(Color.RED);
                paintHeader.setTextSize(30f);
                currentPosition += HEADER_HEIGHT;
                canvas.drawText("Secondary Product Reports", 100, 50, paintHeader);
                canvas.drawLine(0, HEADER_HEIGHT, PAGE_WIDTH, HEADER_HEIGHT, paint);

                //Table Header
                currentPosition += LINE_HEIGHT;
                paint.setColor(Color.BLACK);
                Paint paintTableHeader = new Paint();
                paintTableHeader.setColor(Color.BLACK);
                paintTableHeader.setTextSize(20f);
                //canvas.drawText("No.", 10, currentPosition, paint);
                canvas.drawText("Date Of Scan", 10, currentPosition, paintTableHeader);
                canvas.drawText("Loyalty Point", 200, currentPosition, paintTableHeader);
                canvas.drawText("Product SAPCODE", 350, currentPosition, paintTableHeader);
                currentPosition += 10;
                canvas.drawLine(0, currentPosition, PAGE_WIDTH, currentPosition, paint);

                int startIndex = (pageNumber * 25);
                int endIndex = startIndex + 25;
                if (purchaselist.size() < endIndex) {
                    endIndex = purchaselist.size();
                }

                //Add data to the Table
                for (int i = startIndex; i < endIndex; i++) {
                    ProductWarrantyDetailModel p = purchaselist.get(i);

                    currentPosition += LINE_HEIGHT;
                    //canvas.drawText(String.valueOf(i + 1), 10, currentPosition, paint);
                    canvas.drawText(p.getCreated_timestamp(), 10, currentPosition, paint);
                    canvas.drawText(p.getLoyalty_points(), 200, currentPosition, paint);
                    canvas.drawText(p.getDealer_product(), 350, currentPosition, paint);
                    currentPosition += 10;
                    canvas.drawLine(0, currentPosition, PAGE_WIDTH, currentPosition, paint);
                }

                //canvas.drawt
                // finish the page
                document.finishPage(page);
            }

            // write the document content
            String directory_path = Environment.getExternalStorageDirectory().getPath() + "/Liv-Faster/";
            File file = new File(directory_path);
            if (!file.exists()) {
                file.mkdirs();
            }
            String targetPdf = directory_path + "reports.pdf";
            File filePath = new File(targetPdf);
            try {
                document.writeTo(new FileOutputStream(filePath));
                Toast.makeText(this, "PDF download successfully.", Toast.LENGTH_LONG).show();

            } catch (IOException e) {
                Log.e("main", "error " + e.toString());
                Toast.makeText(this, "Something wrong: " + e.toString(), Toast.LENGTH_LONG).show();
            }
            // close the document
            document.close();
        } else {
            Toast.makeText(this, "Not supported for this android version", Toast.LENGTH_LONG).show();
        }
    }

    public void requestReadWritePermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            ActivityCompat.requestPermissions(SaveReportActivity.this, PERMISSIONS_READ_WRITE,
                    1002);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1002:
                if (grantResults.length > 0) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        //Storage permission is enabled
                        createPDF();

                    } else if (ActivityCompat.shouldShowRequestPermissionRationale(SaveReportActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) && ActivityCompat.shouldShowRequestPermissionRationale(SaveReportActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) && ActivityCompat.shouldShowRequestPermissionRationale(SaveReportActivity.this, Manifest.permission.CAMERA)) {
                        //User has deny from permission dialog
                        final AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(SaveReportActivity.this, R.style.AppCompatAlertDialogStyle);
                        alertDialog1.setTitle("Reading Permission Denied");
                        alertDialog1.setMessage("Are you sure you want to deny this permission?");
                        alertDialog1.setPositiveButton("I'M SURE", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                //not create pdf
                            }
                        });
                        alertDialog1.setNegativeButton("RETRY", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                requestReadWritePermission();
                            }
                        });
                        alertDialog1.show();
                    } else {
                        // User has deny permission and checked never show permission dialog so you can redirect to Application settings page
                        AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(SaveReportActivity.this, R.style.AppCompatAlertDialogStyle);
                        alertDialog1.setMessage("It looks like you have turned off permission required for this feature. It can be enabled under Phone Settings > Apps > Partner > Permissions");
                        alertDialog1.setPositiveButton("GO TO SETTINGS", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent();
                                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package", getPackageName(), null);
                                intent.setData(uri);
                                startActivity(intent);
                            }
                        });
                        alertDialog1.show();
                    }
                }

                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
